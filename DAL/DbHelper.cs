﻿using Astaguru.Models;
using Dapper;
using Repository.Lib;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace DAL
{
    public class DbHelper
    {

        private string _ConnStr;
        public string ConnStr
        {
            get { return _ConnStr; }
            set { _ConnStr = value; }
        }
        private Log log = new Log();

        public DbHelper(string CnStr)
        {
            ConnStr = CnStr;
        }



        public async Task<int> GetuserlimitAsync(int userid)
        {
            var value = (dynamic)null;
            try
            {
                SqlParameter p_mode = new SqlParameter("@Mode", "Getuserlimit");
                SqlParameter p_userid = new SqlParameter("@userid", userid);

                value = await SqlHelper.ExecuteScalarAsync(ConnStr, CommandType.StoredProcedure, "CRUDBid", p_mode, p_userid);
                if (value == null)
                {
                    return 0;
                }
                else
                {
                    return (int)value;
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return 0;
            }
        }
        public async Task<Auction> GetCurrentAuctionDetailAsync(int productid)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConnStr))
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetCurrentAuctionDetail");
                    para.Add("@productid", productid);
                    var val = await con.QueryAsync<Auction>("CRUDCurrentAuction", para, null, 0, CommandType.StoredProcedure);
                    con.Close();
                    return val.SingleOrDefault();
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return null;
            }
        }

        public async Task<int> CheckArtistAssginedbidUserlistAsync(Auction AUC)
        {
            try
            {
                SqlParameter p_mode = new SqlParameter("@Mode", "CheckArtistAssginedbidUserlist");
                SqlParameter p_userid = new SqlParameter("@userid", AUC.userid);
                SqlParameter p_productid = new SqlParameter("@artistid", AUC.productid);

                var value = await SqlHelper.ExecuteScalarAsync(ConnStr, CommandType.StoredProcedure, "CRUDBid", p_mode, p_userid, p_productid);
                return (int)value;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return 0;
            }
        }

        public async Task<int> AddArtisttoBidUserListAsync(Auction AUC)
        {
            try
            {
                SqlParameter p_mode = new SqlParameter("@Mode", "AddArtisttoBidUserList");
                SqlParameter p_userid = new SqlParameter("@userid", AUC.userid);
                SqlParameter p_productid = new SqlParameter("@artistid", AUC.productid);

                var value = await SqlHelper.ExecuteScalarAsync(ConnStr, CommandType.StoredProcedure, "CRUDBid", p_mode, p_userid, p_productid);

                return (int)value;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return 0;
            }
        }

        public async Task<int> UpdateBidClosingTimeAsync(int productid)
        {
            try
            {
                SqlParameter p_mode = new SqlParameter("@Mode", "UpdateBidClosingTime");
                SqlParameter p_productid = new SqlParameter("@productid", productid);

                var value = await SqlHelper.ExecuteScalarAsync(ConnStr, CommandType.StoredProcedure, "CRUDBid", p_mode, p_productid);

                return (int)value;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

                return 0;
            }
        }

        //04_01_2020
        public async Task<List<Auction>> getBidRecordListAsync(Auction AUC)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConnStr))
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "getBidRecordList");
                    para.Add("@productid", AUC.productid);
                    var val = await con.QueryAsync<Auction>("CRUDBid", para, null, 0, CommandType.StoredProcedure);
                    con.Close();
                    return val.ToList();

                    //var para = new DynamicParameters();
                    //para.Add("@Mode", "getBidRecordList");
                    //para.Add("@productid", AUC.productid);
                    //var val = con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).ToList();
                    //con.Close();
                    //return val;
                }

                //List<Auction> actions = new List<Auction>();

                //SqlParameter p_mode = new SqlParameter("@Mode", "getBidRecordList");
                //SqlParameter p_productid = new SqlParameter("@productid", AUC.productid);

                //DataSet ds = await SqlHelper.ExecuteDatasetAsync(ConnStr, CommandType.StoredProcedure, "CRUDBid", p_mode, p_productid);

                //var jsonobjectlist = JsonConvert.SerializeObject(ds);
                //return JsonConvert.DeserializeObject<List<Auction>>(jsonobjectlist);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return null;
            }
        }

        public async Task<int> updateBidAsync(Auction AUC)
        {
            try
            {
                SqlParameter p_mode = new SqlParameter("@Mode", "updateBid");
                SqlParameter p_currentbid = new SqlParameter("@currentbid", AUC.currentbid);
                SqlParameter p_recentbid = new SqlParameter("@recentbid", AUC.recentbid);

                var value = await SqlHelper.ExecuteScalarAsync(ConnStr, CommandType.StoredProcedure, "CRUDBid", p_mode, p_currentbid, p_recentbid);

                return (int)value;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return 0;
            }
        }

        public async Task updateRecentBidAsync(Auction AUC)
        {
            //var value = (dynamic)null;

            try
            {
                var para = new DynamicParameters();
                SqlParameter p_mode = new SqlParameter("@Mode", "updateRecentBid");
                SqlParameter p_recentbid = new SqlParameter("@recentbid", AUC.recentbid);
                SqlParameter p_Bidrecordid = new SqlParameter("@Bidrecordid", AUC.Bidrecordid);

                await SqlHelper.ExecuteScalarAsync(ConnStr, CommandType.StoredProcedure, "CRUDBid", p_mode, p_recentbid, p_Bidrecordid);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
        }

        public async Task<int> InsertBidRecordAsync(Auction AUC)
        {
            int value = 0;
            Auction objproductdetail = await GetCurrentAuctionDetailAsync(AUC.productid);

            objproductdetail.nextValidBidRs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(objproductdetail.pricers, 1).ToString());

            if (AUC.nextValidBidRs > objproductdetail.pricers)
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(ConnStr))
                    {
                        var para = new DynamicParameters();
                        para.Add("@Mode", "InsertBidRecord");
                        para.Add("@Firstname", AUC.firstname);
                        para.Add("@Lastname", AUC.lastname);
                        para.Add("@Thumbnail", AUC.thumbnail);
                        para.Add("@Bidpricers", AUC.nextValidBidRs);
                        para.Add("@Bidpriceus", AUC.nextValidBidUs);
                        para.Add("@Reference", AUC.reference);
                        para.Add("@anoname", AUC.nickname);
                        para.Add("@validbidpricers", AUC.pricers);
                        para.Add("@validbidpriceus", AUC.priceus);
                        para.Add("@currentbid", AUC.currentbid);
                        para.Add("@recentbid", AUC.recentbid);
                        para.Add("@UserId", AUC.userid);
                        para.Add("@Username", AUC.username);
                        para.Add("@productid", AUC.productid);
                        para.Add("@Auctionid", AUC.Online);
                        para.Add("@proxy", AUC.proxy);

                        para.Add("@browserName", AUC.browserName);
                        para.Add("@latitude", AUC.latitude);
                        para.Add("@longitude", AUC.longitude);
                        para.Add("@ipAddress", AUC.ipAddress);
                        para.Add("@userLocation", AUC.userLocation);
                        para.Add("@fullAddress", AUC.fullAddress);

                        value = await con.ExecuteScalarAsync<int>("CRUDBid", para, null, 0, CommandType.StoredProcedure);
                        con.Close();

                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
            }

            return value;
        }

        //get leading user Bidpricers
        public async Task<Auction> GetcurrentleadinguserAsync(int productid)
        {
            try
            {
                Auction objAuction = new Auction();

                SqlParameter p_mode = new SqlParameter("@Mode", "Getcurrentleadinguser");
                SqlParameter p_productid = new SqlParameter("@productid", productid);

                DataSet ds = await SqlHelper.ExecuteDatasetAsync(ConnStr, CommandType.StoredProcedure, "CRUDBid", p_mode, p_productid);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    objAuction.userid = int.Parse(ds.Tables[0].Rows[0]["UserId"].ToString());
                    objAuction.amountlimt = int.Parse(ds.Tables[0].Rows[0]["amountlimt"].ToString());
                    objAuction.Bidpricers = int.Parse(ds.Tables[0].Rows[0]["bidpricers"].ToString());
                }

                return objAuction;

                //28_jan
                //using (SqlConnection con = new SqlConnection(ConnStr))
                //{
                //    var para = new DynamicParameters();
                //    para.Add("@Mode", "Getcurrentleadinguser");
                //    para.Add("@productid", productid);
                //    var val = await con.QueryAsync<Auction>("CRUDBid", para, null, 0, CommandType.StoredProcedure);
                //    con.Close();
                //    return val.SingleOrDefault();

                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return null;
            }
        }

        public async Task<int> UpdateBidLimitAsync(int userid, int amountlimt)
        {
            try
            {
                SqlParameter p_mode = new SqlParameter("@Mode", "UpdateBidLimit");
                SqlParameter p_userid = new SqlParameter("@userid", userid);
                SqlParameter p_amountlimt = new SqlParameter("@amountlimt", amountlimt);

                var value = await SqlHelper.ExecuteScalarAsync(ConnStr, CommandType.StoredProcedure, "CRUDUSER", p_mode, p_userid, p_amountlimt);
                return (int)value;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return 0;
            }
        }

        //Update leading user
        public async Task IsleadingAsync(int productid, int userid, int bidpricers, int islead)
        {
            try
            {
                SqlParameter p_mode = new SqlParameter("@Mode", "Isleading");
                SqlParameter p_userid = new SqlParameter("@userid", userid);
                SqlParameter p_productid = new SqlParameter("@productid", productid);
                SqlParameter p_bidpricers = new SqlParameter("@bidpricers", bidpricers);
                SqlParameter p_islead = new SqlParameter("@productid", islead);

                DataSet ds = await SqlHelper.ExecuteDatasetAsync(ConnStr, CommandType.StoredProcedure, "CRUDUSER", p_mode, p_userid, p_productid, p_bidpricers, p_islead);

                //var jsonobjectlist = JsonConvert.SerializeObject(ds);
                //return JsonConvert.DeserializeObject(jsonobjectlist);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
        }

        //created on 29_06_2020
        //get proxyuser data
        public async Task<Auction> GetcurrentpriceuserAfterproxycheckAsync(int productid, int userid)
        {
            try
            {
                Auction objAuction = new Auction();

                SqlParameter p_mode = new SqlParameter("@Mode", "GetcurrentpriceuserAfterproxycheck");
                SqlParameter p_userid = new SqlParameter("@userid", userid);
                SqlParameter p_productid = new SqlParameter("@productid", productid);

                DataSet ds = await SqlHelper.ExecuteDatasetAsync(ConnStr, CommandType.StoredProcedure, "CRUDBid", p_mode, p_userid, p_productid);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    objAuction.ProxyAmt = int.Parse(ds.Tables[0].Rows[0]["ProxyAmt"].ToString());
                    objAuction.Bidpricers = int.Parse(ds.Tables[0].Rows[0]["Bidpricers"].ToString());
                    objAuction.userid = int.Parse(ds.Tables[0].Rows[0]["userid"].ToString());
                    objAuction.amountlimt = int.Parse(ds.Tables[0].Rows[0]["amountlimt"].ToString());
                    objAuction.islessproxy = int.Parse(ds.Tables[0].Rows[0]["islessproxy"].ToString());
                }

                return objAuction;
                //using (SqlConnection con = new SqlConnection(ConnStr))
                //{
                //    var para = new DynamicParameters();
                //    para.Add("@Mode", "GetcurrentpriceuserAfterproxycheck");
                //    para.Add("@productid", productid);
                //    para.Add("@userid", userid);
                //    var val = await con.QueryAsync<Auction>("CRUDBid", para, null, 0, CommandType.StoredProcedure);
                //    con.Close();
                //    return val.SingleOrDefault();

                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return null;
            }
        }

        //created on 6_2_2020
        //set flag for proxy user
        public async Task UpdateislesssproxyAsync(int userid, int productid, int islessproxy)
        {
            try
            {
                SqlParameter p_mode = new SqlParameter("@Mode", "Updateislesssproxy");
                SqlParameter p_userid = new SqlParameter("@userid", userid);
                SqlParameter p_productid = new SqlParameter("@productid", productid);
                SqlParameter p_islessproxy = new SqlParameter("@islessproxy", islessproxy);

                DataSet ds = await SqlHelper.ExecuteDatasetAsync(ConnStr, CommandType.StoredProcedure, "CRUDUSER", p_mode, p_userid, p_productid, p_islessproxy);

                //var jsonobjectlist = JsonConvert.SerializeObject(ds);
                //return JsonConvert.DeserializeObject(jsonobjectlist);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
        }

        // create 29_06_2020
        public async Task IsleadingproxyAsync(int productid, int userid, int ProxyAmt, int islead)                        //Update leading user
        {
            try
            {
                SqlParameter p_mode = new SqlParameter("@Mode", "Isleadingproxy");
                SqlParameter p_userid = new SqlParameter("@userid", userid);
                SqlParameter p_productid = new SqlParameter("@productid", productid);
                SqlParameter p_islead = new SqlParameter("@islead", islead);
                SqlParameter p_ProxyAmt = new SqlParameter("@ProxyAmt", ProxyAmt);

                await SqlHelper.ExecuteNonQueryAsync(ConnStr, CommandType.StoredProcedure, "CRUDUSER", p_mode, p_userid, p_productid, p_islead, p_ProxyAmt);

                //var jsonobjectlist = JsonConvert.SerializeObject(ds);
                //return JsonConvert.DeserializeObject(jsonobjectlist);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
        }

        //created on 29_06_2020
        //get outbid user Bidpricers
        public async Task<Auction> GetcurrentpriceuserAsync(int productid, int userid)
        {
            try
            {
                Auction objAuction = new Auction();

                SqlParameter p_mode = new SqlParameter("@Mode", "Getcurrentpriceuser");
                SqlParameter p_productid = new SqlParameter("@productid", productid);
                SqlParameter p_userid = new SqlParameter("@userid", userid);

                DataSet ds = await SqlHelper.ExecuteDatasetAsync(ConnStr, CommandType.StoredProcedure, "CRUDBid", p_mode, p_productid, p_userid);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    objAuction.userid = int.Parse(ds.Tables[0].Rows[0]["UserId"].ToString());
                    objAuction.amountlimt = int.Parse(ds.Tables[0].Rows[0]["amountlimt"].ToString());
                    objAuction.Bidpricers = int.Parse(ds.Tables[0].Rows[0]["bidpricers"].ToString());
                }

                return objAuction;

                //28_jan
                //Auction obj = new Auction();
                //using (SqlConnection con = new SqlConnection(ConnStr))
                //{
                //    var para = new DynamicParameters();
                //    para.Add("@Mode", "Getcurrentpriceuser");
                //    para.Add("@productid", productid);
                //    para.Add("@userid", userid);
                //    var val = await con.QueryAsync<Auction>("CRUDBid", para, null, 0, CommandType.StoredProcedure);
                //    con.Close();
                //    return val.SingleOrDefault();

                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return null;
            }
        }

        // create 29_06_2020
        //get outbid user
        public async Task<int> getoutbidamountAsync(int productid, int userid)
        {
            try
            {
                SqlParameter p_mode = new SqlParameter("@Mode", "getoutbidamount");
                SqlParameter p_userid = new SqlParameter("@userid", userid);
                SqlParameter p_productid = new SqlParameter("@productid", productid);

                var value = await SqlHelper.ExecuteScalarAsync(ConnStr, CommandType.StoredProcedure, "CRUDBid", p_mode, p_userid, p_productid);
                if (value == null)
                {
                    return 0;
                }

                return (int)value;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return 0;
            }
        }

        public async Task<int> UpdateAcutionPriceAsync(Auction AUC)
        {
            try
            {
                SqlParameter p_mode = new SqlParameter("@Mode", "UpdateAcutionPrice");
                SqlParameter p_pricers = new SqlParameter("@pricers", AUC.nextValidBidRs);
                SqlParameter p_priceus = new SqlParameter("@priceus", AUC.nextValidBidUs);
                SqlParameter p_productid = new SqlParameter("@productid", AUC.productid);

                var value = await SqlHelper.ExecuteScalarAsync(ConnStr, CommandType.StoredProcedure, "CRUDBid", p_mode, p_pricers, p_priceus, p_productid);

                return (int)value;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

                return 0;
            }
        }

        //get leading user proxy
        public async Task<Auction> GetCurrentLeadingProxyuserAsync(int productid)
        {
            try
            {
                //28_jan
                //using (SqlConnection con = new SqlConnection(ConnStr))
                //{
                //    var para = new DynamicParameters();
                //    para.Add("@Mode", "GetCurrentLeadingProxyuser");
                //    para.Add("@productid", productid);
                //    var val = await con.QueryAsync<Auction>("CRUDBid", para, null, 0, CommandType.StoredProcedure);
                //    con.Close();
                //    return val.SingleOrDefault();

                //}

                Auction objAuction = new Auction();

                SqlParameter p_mode = new SqlParameter("@Mode", "GetCurrentLeadingProxyuser");
                SqlParameter p_productid = new SqlParameter("@productid", productid);

                DataSet ds = await SqlHelper.ExecuteDatasetAsync(ConnStr, CommandType.StoredProcedure, "CRUDBid", p_mode, p_productid);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    objAuction.userid = int.Parse(ds.Tables[0].Rows[0]["UserId"].ToString());
                    objAuction.amountlimt = int.Parse(ds.Tables[0].Rows[0]["amountlimt"].ToString());
                    objAuction.ProxyAmt = int.Parse(ds.Tables[0].Rows[0]["ProxyAmt"].ToString());
                }

                return objAuction;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return null;
            }
        }

        public async Task<int> checkforproxyuserAsync(int productid, int userid)
        {
            try
            {
                SqlParameter p_mode = new SqlParameter("@Mode", "checkforproxyuser");
                SqlParameter p_userid = new SqlParameter("@userid", userid);
                SqlParameter p_productid = new SqlParameter("@productid", productid);

                var value = await SqlHelper.ExecuteScalarAsync(ConnStr, CommandType.StoredProcedure, "CRUDBid", p_mode, p_userid, p_productid);

                return (int)value;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return 0;
            }
        }

        public async Task<int> getoutbidproxyamountAsync(int productid, int userid)                      //get outbid user
        {
            try
            {
                SqlParameter p_mode = new SqlParameter("@Mode", "getoutbidproxyamount");
                SqlParameter p_userid = new SqlParameter("@userid", userid);
                SqlParameter p_productid = new SqlParameter("@productid", productid);

                var value = await SqlHelper.ExecuteScalarAsync(ConnStr, CommandType.StoredProcedure, "CRUDBid", p_mode, p_userid, p_productid);

                return (int)value;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return 0;
            }
        }

        public async Task<Auction> GetcurrentoutbiduserAsync(int productid, int userid)                               //get outbid user Bidpricers
        {
            try
            {
                //using (SqlConnection con = new SqlConnection(ConnStr))
                //{
                //    var para = new DynamicParameters();
                //    para.Add("@Mode", "Getcurrentoutbiduser");
                //    para.Add("@userid", userid);
                //    para.Add("@productid", productid);
                //    var val = await con.QueryAsync<Auction>("CRUDBid", para, null, 0, CommandType.StoredProcedure);
                //    con.Close();
                //    return val.SingleOrDefault();

                //}
                Auction objAuction = new Auction();

                SqlParameter p_mode = new SqlParameter("@Mode", "Getcurrentoutbiduser");
                SqlParameter p_userid = new SqlParameter("@userid", userid);
                SqlParameter p_productid = new SqlParameter("@productid", productid);

                DataSet ds = await SqlHelper.ExecuteDatasetAsync(ConnStr, CommandType.StoredProcedure, "CRUDBid", p_mode, p_userid, p_productid);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    objAuction.Bidpricers = int.Parse(ds.Tables[0].Rows[0]["Bidpricers"].ToString());
                    objAuction.userid = int.Parse(ds.Tables[0].Rows[0]["userid"].ToString());
                    objAuction.amountlimt = int.Parse(ds.Tables[0].Rows[0]["amountlimt"].ToString());
                    objAuction.email = ds.Tables[0].Rows[0]["email"].ToString();
                    objAuction.username = ds.Tables[0].Rows[0]["username"].ToString();
                    objAuction.mobile = ds.Tables[0].Rows[0]["mobile"].ToString();
                    objAuction.name = ds.Tables[0].Rows[0]["name"].ToString();
                    objAuction.lastname = ds.Tables[0].Rows[0]["Lastname"].ToString();
                }
                return objAuction;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return null;
            }
        }

        //Created on 29_1_2021 AddProxybidRecords Medthods

        public async Task<List<Auction>> GetProxyInfoAsync(Auction AUC)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConnStr))
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetProxyInfo");
                    para.Add("@Auctionid", AUC.Online);
                    para.Add("@productid", AUC.productid);
                    //para.Add("@price", AUC.nextValidBidRs);
                    para.Add("@price", AUC.pricers);
                    para.Add("@UserId", AUC.userid);
                    var val = await con.QueryAsync<Auction>("CRUDBid", para, null, 0, CommandType.StoredProcedure);
                    con.Close();
                    return val.ToList();
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return null;
            }
        }

        public async Task<List<Auction>> GetAcutionDataAsync(Auction AUC)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConnStr))
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetAcutionData");
                    para.Add("@Auctionid", AUC.Online);
                    para.Add("@productid", AUC.productid);
                    var val = await con.QueryAsync<Auction>("CRUDBid", para, null, 0, CommandType.StoredProcedure);
                    con.Close();
                    return val.ToList();
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

                return null;
            }
        }

        public async Task UpdateRecentCurrentBidAsync(Auction AUC)
        {
            var value = (dynamic)null;

            try
            {
                SqlParameter p_mode = new SqlParameter("@Mode", "UpdateRecentCurrentBid");
                SqlParameter p_currentbid = new SqlParameter("@currentbid", AUC.currentbid);
                SqlParameter p_recentbid = new SqlParameter("@recentbid", AUC.recentbid);
                SqlParameter p_Bidrecordid = new SqlParameter("@Bidrecordid", AUC.Bidrecordid);

                value = await SqlHelper.ExecuteScalarAsync(ConnStr, CommandType.StoredProcedure, "CRUDBid", p_mode, p_currentbid, p_recentbid, p_Bidrecordid);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                //return 0;
            }
            //return (int)value;
        }

        public async Task<List<Auction>> getBidUserListAsync(Auction AUC)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConnStr))
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "getBidUserList");
                    para.Add("@productid", AUC.productid);
                    para.Add("@userid", AUC.userid);
                    var val = await con.QueryAsync<Auction>("CRUDBid", para, null, 0, CommandType.StoredProcedure);
                    con.Close();
                    return val.ToList();
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return null;
            }
        }

        public async Task<List<Auction>> GetproxyUserAsync(Auction AUC)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConnStr))
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetproxyUser");
                    para.Add("@Auctionid", AUC.Online);
                    para.Add("@productid", AUC.productid);
                    para.Add("@price", AUC.nextValidBidRs);
                    var val = await con.QueryAsync<Auction>("CRUDBid", para, null, 0, CommandType.StoredProcedure);
                    con.Close();
                    return val.ToList();
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return null;
            }
        }

        public async Task<List<Auction>> GetsameproxyAsync(Auction AUC)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConnStr))
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "Getsameproxy");
                    para.Add("@Auctionid", AUC.Online);
                    para.Add("@productid", AUC.productid);
                    para.Add("@price", AUC.nextValidBidRs);
                    var val = await con.QueryAsync<Auction>("CRUDBid", para, null, 0, CommandType.StoredProcedure);
                    con.Close();
                    return val.ToList();
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return null;
            }
        }

        public async Task<List<Auction>> GetUpdatedProxyuserAsync(Auction AUC)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConnStr))
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetUpdatedProxyuser");
                    para.Add("@Auctionid", AUC.Online);
                    para.Add("@productid", AUC.productid);
                    para.Add("@price", AUC.curprice);
                    para.Add("@nextvalid", AUC.nextValidBidRs);
                    var val = await con.QueryAsync<Auction>("CRUDBid", para, null, 0, CommandType.StoredProcedure);
                    con.Close();
                    return val.ToList();
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return null;
            }
        }

        public async Task<List<Auction>> GetUpdatedProxyInfoAsync(Auction AUC)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConnStr))
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetUpdatedProxyInfo");
                    para.Add("@Auctionid", AUC.Online);
                    para.Add("@productid", AUC.productid);
                    para.Add("@price", AUC.curprice);
                    var val = await con.QueryAsync<Auction>("CRUDBid", para, null, 0, CommandType.StoredProcedure);
                    con.Close();
                    return val.ToList();
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

                return null;
            }
        }

        public async Task<int> InsertProxyBidRecordAsync(Auction AUC)
        {
            var value = (dynamic)null;
            var checkduplicatebid = 0;

            try
            {
                using (SqlConnection con = new SqlConnection(ConnStr))
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "checkduplicateProxyAmountUser");
                    para.Add("@productid", AUC.productid);
                    para.Add("@ProxyAmt", AUC.ProxyAmt);
                    para.Add("@userid", AUC.userid);
                    checkduplicatebid = await con.ExecuteScalarAsync<int>("CRUDBid", para, null, 0, CommandType.StoredProcedure);
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            if (checkduplicatebid == 0)
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(ConnStr))
                    {
                        var para = new DynamicParameters();
                        para.Add("@Mode", "InsertProxyBidRecord");
                        para.Add("@UserId", AUC.userid);
                        para.Add("@productid", AUC.productid);
                        para.Add("@ProxyAmt", AUC.ProxyAmt);
                        para.Add("@ProxyAmtus", AUC.ProxyAmtus);
                        para.Add("@Createdby", AUC.username);
                        para.Add("@Auctionid", AUC.Online);
                        para.Add("@BidBy", 0);

                        // 22_12_2020
                        para.Add("@latitude", AUC.latitude);
                        para.Add("@longitude", AUC.longitude);
                        para.Add("@ipAddress", AUC.ipAddress);
                        para.Add("@userLocation", AUC.userLocation);
                        para.Add("@fullAddress", AUC.fullAddress);
                        //

                        value = await con.ExecuteScalarAsync<int>("CRUDBid", para, null, 0, CommandType.StoredProcedure);
                        con.Close();
                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                }
            }

            return value;
        }

        public async Task<Auction> GetproxyinfosameAsync(int AuctionId)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConnStr))
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "Getproxyinfosame");
                    para.Add("@AuctionId", AuctionId);
                    var val = await con.QueryAsync<Auction>("CRUDBid", para, null, 0, CommandType.StoredProcedure);
                    con.Close();
                    return val.SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return null;
            }
        }
    }



}
