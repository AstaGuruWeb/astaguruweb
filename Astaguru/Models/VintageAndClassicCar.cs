﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Astaguru.Models
{
    public class VintageAndClassicCar
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Email is required.")]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Email is not valid.")]
        public string Email { get;set;}

        [Required(ErrorMessage = "Automaker is required.")]
        public string Automaker { get; set; }

        [Required(ErrorMessage = "Model No. is required.")]
        public string Model { get; set; }
        public string Engine { get; set; }
        public string Chassis { get; set; }

        [Required(ErrorMessage = "Validation Registration No. is required.")]
        public string Registration { get; set; }
        public string Aboutus { get; set; }

        [DataType(DataType.PhoneNumber, ErrorMessage = "Mobile No.")]
        public string Request { get; set; }

        [Required(ErrorMessage = "Image is required.")]
        public string Image { get; set; }

        public string Image2 { get; set; }

        public string Image3 { get; set; }

    }
}