﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru.Models
{
    public class GetInTouch
    {
        public string name { get; set; }
        public string Email { get; set; }

        public string Phone { get; set; }

        public string Category { get; set; }

        public string Message { get; set; }


    }
}