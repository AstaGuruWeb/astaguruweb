﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Astaguru.Models
{
    public class ContactUsForm
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name required.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Email required.")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Enter Valid Email Address.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Phone required.")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Queries required.")]
        public string Queries { get; set; }
    }
}