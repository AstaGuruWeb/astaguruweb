﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Astaguru.Models
{
    public class Marketing
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter your name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter your Email")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Please enter your Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter your Country")]
        public string Country { get; set; }

        [Required(ErrorMessage = "Please enter your City")]
        public string City { get; set; }

        public string Interested_In { get; set; }

        public string About_Us { get; set; }

        public string Mobile { get; set; }

    }
}