﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru.Models.AdminModel
{
    public class AdminUser
    {
        public int id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Emailid { get; set; }
        public string Username { get; set; }
        public string Gender { get; set; }
        public string UserPassword { get; set; }
        public string UserImage { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public int Is_active { get; set; }

        public string submit { get; set; }
        public IEnumerable<AdminUser> GetUserlist { get; set; }
    }
}