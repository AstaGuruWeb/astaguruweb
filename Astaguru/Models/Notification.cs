﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Astaguru.Models
{
    public class Notification
    {
        public int Sr { get; set; }
        public int notificationid { get; set; }
        public int userid { get; set; }
        public string device_id { get; set; }

        public string device_type { get; set; }

     
        public string subject { get; set; }

        public string shortdescription { get; set; }

        
        public string description { get; set; }

        public string username { get; set; }

        public List<Notification> GridList { get; set; }

        public IEnumerable<SelectListItem> devicelist { get; set; }
    }



}