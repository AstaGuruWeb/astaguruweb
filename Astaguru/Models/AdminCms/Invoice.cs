﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Astaguru.Models.AdminCms
{
    public class Invoice
    {
        public int productid { get; set; }

        public int id { get; set; }

        public string title { get; set; }
        public string name { get; set; }
        public string auctiontitle { get; set; }

        //[Required(ErrorMessage ="Please enter First Name")]
        public string firstname { get; set; }

        //[Required(ErrorMessage = "Please enter Last Name")]
        public string lastname { get; set; }

        public string thumbnail { get; set; }

        public string image { get; set; }


        public string Date { get; set; }

        public int mediumid { get; set; }
        public string medium { get; set; }

        public string productsize { get; set; }


        //[DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        //[Required(ErrorMessage = "Please enter Bid close Time")]
        //public DateTime Bidclosingtime { get; set; }

        //[Required(ErrorMessage = "Please enter your Bidclosingtime)")]
        public string Bidclosingtime { get; set; }

        public int price { get; set; }

        [Column("Bidpricers")]

        //[Required(ErrorMessage = "Please enter your price(RS)")]
        public int pricers { get; set; }
        public int priceus { get; set; }

        public int curprice { get; set; }

        public int curpriceUs { get; set; }


        public string collectors { get; set; }

        public string estamiate { get; set; }

        public int userid { get; set; }

        public string username { get; set; }

        public int Auctionid { get; set; }
        public string reference { get; set; }

        public string anoname { get; set; }


        public int ProxyAmt { get; set; }

        public int ProxyAmtus { get; set; }

        public int recentbid { get; set; }
        public int currentbid { get; set; }


        // pricelow
        public int nextValidBidRs { get; set; }
        public int nextValidBidUs { get; set; }


        public string nickname { get; set; }

        public int proxy { get; set; }

        public string proxy1 { get; set; }

        public int Bidrecordid { get; set; }

        public int validbidpricers { get; set; }
        public int validbidpriceus { get; set; }

        public int Bidpricers { get; set; }
        public int Bidpriceus { get; set; }

        //[Required(ErrorMessage ="Please enter Profile Description")]
        public string Profile { get; set; }

        public string Auctionname { get; set; }

        public string auctiondate { get; set; }

        public string daterec { get; set; }


        public int totalrs { get; set; }
        public int totalus { get; set; }


        public int pricelow { get; set; }
        public int pricehigh { get; set; }

        public string description { get; set; }

        public int auctionType { get; set; }

        public int artistid { get; set; }

        public string Picture { get; set; }

        public int categoryid { get; set; }

        //[Required(ErrorMessage ="Please enter Category")]
        public string category { get; set; }
        public string HumanFigure { get; set; }


        // Filter

        public List<string> chkArtist { get; set; }

        public List<string> chkMedium { get; set; }

        public List<string> chkDept { get; set; }

        public string view1 { get; set; }
        public string view2 { get; set; }
        public string view3 { get; set; }
        public string view4 { get; set; }

        public int VAT { get; set; }

        public string ProductImage { get; set; }


        public int currency { get; set; }

        public string auctionBanner { get; set; }
        public string Prdescription { get; set; }

        public string myBidClosingTime { get; set; }


        public string hh { get; set; }
        public string mm { get; set; }
        public string ss { get; set; }


        public int timeRemains { get; set; }

        public int MyUserID { get; set; }


        public string Subquery { get; set; }

        public int page { get; set; }

        public string pageName { get; set; } // For get the current page name

        public int Online { get; set; }


        [Required(ErrorMessage ="Invoice No. is Required")]
        public string Invoice_No { get; set; }

        [Required(ErrorMessage = "Invoice Date is Required")]
        public string Invoice_Date { get; set; }

        public string E_way_bill_No { get; set; }

        [Required(ErrorMessage = "GSTIN No. is Required")]
        public string GSTIN { get; set; }

        [Required(ErrorMessage = "Billing Name is Required")]
        public string BillingName { get; set; }

        [Required(ErrorMessage = "Billing Address is Required")]
        public string BillingAddress { get; set; }

        [Required(ErrorMessage = "Delivery Address is Required")]
        public string DeliveryAddress { get; set; }

        public string ArtistName { get; set; }


        [Required(ErrorMessage = "HSN code is Required")]
        [Range(1, int.MaxValue, ErrorMessage = "HSN code is Required")]
        public int HSN_code { get; set; }


        public int FreightHSN_Code { get; set; }

        public int Freight_IGST { get; set; }

        public int Freight_CGST { get; set; }

        public int Freight_SGST { get; set; }

        public int Freight_Amount { get; set; }

        public string userCountry { get; set; }

        public string PrVat { get; set; }

        public int isInternational { get; set; }

        public int isInternationalGST { get; set; }

        public int astaguruPrice { get; set; }

        public int usedGoodPercentage { get; set; }

        public string state { get; set; }


        public IEnumerable<SelectListItem> AuctionNameList { get; set; }


        public int referenceid { get; set; }

        //public IEnumerable<SelectListItem> AuctionNameList { get; set; }

        public List<Invoice> GridList { get; set; }

        public string mobile { get; set; }

        public string email { get; set; }

        public string fullAddress { get; set; }

        public string artist { get; set;}
    }
}