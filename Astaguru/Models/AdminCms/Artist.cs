﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Astaguru.Models.AdminCms
{
    public class Artist
    {
        public string Sr { get; set; }

        public int artistid { get; set; }

        [Required(ErrorMessage = "Please enter First Name")]
        public string firstname { get; set; }

        [Required(ErrorMessage = "Please enter Last Name")]
        public string lastname { get; set; }

        [Required(ErrorMessage = "Please enter Profile Description")]
        public string Profile { get; set; }

        [Required(ErrorMessage = "Picture Required")]
        public string Picture { get; set; }

        public string imgPreview { get; set; }

        public List<Artist> GridList { get; set; }
    }
}