﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Astaguru.Models.AdminCms
{
    public class Style
    {
        public string Sr { get; set; }
        public int styleid { get; set; }

        [Required(ErrorMessage = "Please Enter Style")]
        public string style { get; set; }

        public List<Style> GridList { get; set; }

    }
}