﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Astaguru.Models.AdminCms
{
    public class AuctionList
    {
        public string Sr { get; set; }

        public int Auctionid { get; set; }

        [Required(ErrorMessage = "Please Enter Auction Name")]
        public string Auctionname { get; set; }

        public string auctiondate { get; set; }

        public string Date { get; set; }

        public decimal DollarRate { get; set; }

        [Required(ErrorMessage = "Image Required")]
        public string image { get; set; }

        public string status { get; set; }

        public int auctionType { get; set; }

        [Required(ErrorMessage = "Total Sale Value Rs Required")]
        public string totalSaleValueRs { get; set; }

        [Required(ErrorMessage = "Total Sale Value Us Required")]
        public string totalSaleValueUs { get; set; }

        [Required(ErrorMessage = "Auction Banner Required")]
        public string auctionBanner { get; set; }

        [Required(ErrorMessage = "Auction WebImage Required")]
        public string auctionWebImage { get; set; }

        //[Required(ErrorMessage = "Recent Auction Banner Required")]
        public string recentAuctionBanner { get; set; }

        public string imgPreview { get; set; }

        public string imgPreview1 { get; set; }
        public string imgPreview2 { get; set; }
        public string imgPreview3 { get; set; }


        public List<AuctionList> GridList { get; set; }

        public bool Notification { get; set; }

        // notification before product add in upcoming or before Upcoming Start
        public bool RemindNotification { get; set; }


    }
}