﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Astaguru.Models.AdminCms
{
    public class Faq
    {
        public string Sr { get; set; }
        public int Id { get; set; }

        [Required(ErrorMessage = "Category is Required ")]
        public string Category { get; set; }

        [Required(ErrorMessage = "Title is Required ")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Description is Required ")]
        public string Description { get; set; }

       public List<Faq> GridList { get; set; }
    }
}