﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Astaguru.Models
{
    public class Shipping
    {
        public int Id { get; set; }

        public string PaymentType { get; set; }

        public string PaymentStatus { get; set; }

        public string PaymentDetails { get; set; }

        public string ShippingServiceProvider { get; set; }

        public string InhouseServiceStatus { get; set; }

        public int Productid { get; set; }

        public string thumbnail { get; set; }

        public string title { get; set; }

        public int Auctionid { get; set; }

        public string Auctionname { get; set; }

        public string auctiondate { get; set; }

        public int auctionType { get; set; }

        public string reference { get; set; }

        public string BillingName { get; set; }

        public string DeliveryAddress { get; set; }

        public string Country { get; set; }

        public int artistid { get; set; }

        public string ArtistName { get; set; }

        public string AWBNumber{get;set;}

        public string DeliveryStatus { get; set; }

        public string ExpectedDeliveryDate { get; set; }

        public string StatusDate { get; set; }



        public string Mobile { get; set; }

        public string Email { get; set; }


        public IEnumerable<SelectListItem> AuctionNameList { get; set; }


        public string Sr { get; set; }

        public List<Shipping> GridList { get; set; }

    }
}