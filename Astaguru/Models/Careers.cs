﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Astaguru.Models
{
    public class Careers
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Enter First Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Enter Email Address")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Enter Valid Email Address.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Enter Job Title")]
        public string JobTitle { get; set; }

        [Required(ErrorMessage = "Enter Messages")]
        public string Message { get; set; }

        [Required(ErrorMessage = "Enter Hear About Us")]
        public string aboutus { get; set; }

        [Required(ErrorMessage = "Please Upload Resume")]
        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.doc|.docx|.pdf)$", ErrorMessage = "Only Document files allowed.")]
        public string Resume { get; set; }

        //public string ApplyDate { get; set; }

        //public string Sr { get; set; }

        public List<Careers> ApplyForJobList { get; set; }
    }
}