﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Astaguru.Models
{

        public class Promotion
        {
        public int Id { get; set; }

        public string Category { get; set; }
        public string Name { get; set; }

        public string Mobile_Number { get; set; }

        public string Email { get; set; }

        public string Aboutus { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }

        public string Image2 { get; set; }

        public string Image3 { get; set; }


        // Modern Indian Art
        public string ArtistName { get; set; }
        public int Artistid { get; set; }

        public IEnumerable<SelectListItem> ArtistList { get; set; }

        public string ArtworkName { get; set; }
        public string YearofPurchaseModern { get; set; }
        public string Purchasefrom { get; set; }


        //Book Consignment
        public string AuthorPublisherName { get; set; }

        public string BookPublishedYear { get; set;}

        public string BookCondition { get; set; }

        public string YearofPurchaseBook { get; set; }
        

        //Textile Consignment
        public string ChoosAuctionOption { get; set; }

        //Vintage care
        public string Automaker { get; set; }
        public string Model { get; set; }
        public string Engine { get; set; }
        public string Chassis { get; set; }
        public string Registration { get; set; }

        //Jewellery

        public string YearofPurchaseJewellery { get; set; }
        public string BillOfPurchaseJewellery { get; set; }
        public string Weight { get; set; }


        // Timepieces (Watch)

        public string YearofPurchaseTimepieces { get; set; }
        public string BillOfPurchaseTimepieces { get; set; }
        public string Brand { get; set; }



        //[Required(ErrorMessage = "Email is required.")]
        //[RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Email is not valid.")]


        //[Required(ErrorMessage = "Automaker is required.")]


         

          

        
    }
}