﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru.Models
{
    public class CMS
    {
        public int CmsId { get; set; }

        public string Heading { get; set; }

        public string Description { get; set; }

        public List<CMS> CMSList { get; set; }
    }
}