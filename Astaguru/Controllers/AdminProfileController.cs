﻿using Astaguru.Models;
using Astaguru.Services.AdminService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Astaguru.Controllers
{
    public class AdminProfileController : Controller
    {
        User UR = new User();
        AdminUsersProfileService AUPS = new AdminUsersProfileService();
        //
        // GET: /AdminProfile/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult listProfile()
        {
            return View("~/Views/Admin/listProfile.cshtml");
        }

        public ActionResult DisplayData(int userid)
        {
            UR = AUPS.DisplayData(userid);
            return View("~/Views/Admin/viewUserDetails.cshtml", UR);
        }

        [HttpPost]
        public ActionResult BidAmount(int userid, int amountlimt)
        {
            int Result = AUPS.UpdateBidAmount(userid, amountlimt);
            return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateBidAccess(User UR)
        {
            AUPS.UpdateBidAccess(UR);
            return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
        }
	}
}