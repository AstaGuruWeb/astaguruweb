﻿using Astaguru.Models.AdminModel;
using Astaguru.Services.AdminService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Astaguru.Controllers
{
    public class AdminUserController : Controller
    {
        AdminUser Admin = new AdminUser();
        AdminUserService AUS = new AdminUserService();

        //
        // GET: /AdminUser/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Home()
        {
            return View("~/Views/Admin/Index.cshtml");
        }
        public ActionResult Login()
        {
            return View("~/Views/Admin/Login.cshtml");
        }

        [HttpPost]
        public ActionResult Login(AdminUser AU)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string Username = AU.Username;
                    string password = AU.UserPassword;
                    AU.GetUserlist = AUS.GetLogin(Username, password);
                    if (AU.GetUserlist.Count() == 0)
                    {
                        TempData["error"] = "Invalid username or password";
                    }
                    else
                    {
                        foreach (var item in AU.GetUserlist)
                        {
                            Session["AdminUsername"] = item.Username;
                            Session["Admin_id"] = item.id;
                        }
                        return RedirectToAction("Home", "AdminUser");
                    }
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                }
            }
            return View("~/Views/Admin/Login.cshtml");
        }

        public ActionResult RefreshDotNetVersion()
        {
            int result = AUS.RefreshDotNetVersion();
            return View("~/Views/Admin/abc.cshtml");
        }

	}
}