﻿using Astaguru.Models;
using Astaguru.Services.UserService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Astaguru.Controllers
{
    public class ArtistController : Controller
    {
        Auction AUC = new Auction();
        ArtistService AS = new ArtistService();
        //
        // GET: /Artist/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult artistProfile(int artistid)
        {
            string title = "Artist/artistProfile?artistid="+ artistid + "";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            AUC = AS.getArtistProfile(artistid);
            return View("~/Views/User/artistProfile.cshtml", AUC);
        }


        [HttpPost]
        public JsonResult AutoArtist(string Prefix)
        {
            Prefix = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Prefix.ToLower());
            Auction Obj = new Auction();

            //Note : you can bind same list from database  
            List<Auction> ObjList = AS.getArtistList(); //  new List<Register>()  


            //    //Searching records from list using LINQ query  
            var ArtistName = (from N in ObjList
                              where N.name.Contains(Prefix)
                              select new { N.artistid, N.name });

            if (!ArtistName.Any())
            {
                return Json(new { Status = "Failed" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(ArtistName, JsonRequestBehavior.AllowGet);
            }
        }


        //29_12_2020
        #region get MetaTags

        public static string UpdateMetaDetails(string pageUrl)
        {
            Utility util = new Utility();
            //--- StringBuilder object to store MetaTags information.
            StringBuilder sbMetaTags = new StringBuilder();

            //--Step1 Get data from database.

            DataTable dt = new DataTable();
            dt = util.Display("exec Proc_MetaTag 'bindMetatTag',0,'" + pageUrl.ToString() + "'");
            if (dt.Rows.Count > 0)
            {

                //---- Step2 In this step we will add <title> tag to our StringBuilder Object.
                sbMetaTags.Append("<title>" + dt.Rows[0]["Title"].ToString() + "</title>");

                //---- Step3 In this step we will add "Meta Description" to our StringBuilder Object.
                sbMetaTags.Append("<meta name='description' content='" + dt.Rows[0]["MetaDescription"].ToString() + "' >");

                //---- Step4 In this step we will add "Meta Keywords" to our StringBuilder Object.
                sbMetaTags.Append("<meta name='keywords' content ='" + dt.Rows[0]["MetaKeywords"].ToString() + "'>");
            }
            return sbMetaTags.ToString();
        }

        #endregion

    }
}