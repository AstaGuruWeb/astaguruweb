﻿using Astaguru.Models;
using Astaguru.Services;
using Astaguru.Services.UserService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Astaguru.Controllers
{
    public class UpcomingController : Controller
    {
        Auction auc = new Auction();
        CurrentAuctionService CAS = new CurrentAuctionService();
        Auction AUC = new Auction();
        CommonController COMC = new CommonController();
        BidController BidC = new BidController();
        UserService US = new UserService();
        //
        // GET: /Upcoming/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UpcomingAuctions()
        {
            string title = "UpcomingAuctions";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/UpcomingAuctions.cshtml");
        }
        public ActionResult UpcomingPreview(string strurl)
        {
            UpcomingAuctionService uc = new UpcomingAuctionService();
            Session["SubqueryUpcoming"] = null;
            Session["FilterArtistUpcoming"] = null;
            Session["FilterMediumUpcoming"] = null;
            Session["FilterDeptUpcoming"] = null;
            TempData["Auctionid"] = uc.getupcomingauctionid(strurl);
            return View("~/Views/User/UpcomingPreview.cshtml");
        }

        public ActionResult FilterUpcomingPreview(int Auctionid)
        {
            return View("~/Views/User/UpcomingPreview.cshtml");
        }

        //public ActionResult UpcomingPreviewList(int Auctionid)
        //{
        //    Session["SubqueryUpcoming"] = null;
        //    Session["FilterArtistUpcoming"] = null;
        //    Session["FilterMediumUpcoming"] = null;
        //    Session["FilterDeptUpcoming"] = null;
        //    return View("~/Views/User/UpcomingPreviewList.cshtml");
        //}

        public ActionResult UpcomingPreviewList(string strurl)
        {
            UpcomingAuctionService uc = new UpcomingAuctionService();
            Session["SubqueryUpcoming"] = null;
            Session["FilterArtistUpcoming"] = null;
            Session["FilterMediumUpcoming"] = null;
            Session["FilterDeptUpcoming"] = null;
            //ViewBag.Auctionid = uc.getupcomingauctionid(strurl);
            TempData["Auctionid"] = uc.getupcomingauctionid(strurl);
            return View("~/Views/User/UpcomingPreviewList.cshtml");
        }

        public ActionResult UpcomingFilterPreviewList(int Auctionid)
        {
            return View("~/Views/User/UpcomingPreviewList.cshtml");
        }

        public ActionResult UpcomingDetails(int productid)
        {
            AUC = CAS.GetAuctionDetail(productid);
            return View("~/Views/User/UpcomingDetails.cshtml", AUC);
        }

        

             [HttpPost]
        public ActionResult saveUpcomingProxyBid(Auction AUC)
        {
            try
            {
                string country = AUC.country;

                //created on 22_12_2020
                string userLocation = AUC.userLocation;
                string fullAddress = AUC.fullAddress;
                string latitude = AUC.latitude;
                string longitude = AUC.longitude;
                //

                //int amountlimt = AUC.amountlimt;
                int userid = Convert.ToInt32(Session["userid"]);   //COMC.checkUserSessionValue();
                int amountlimt = CAS.Getuserlimit(userid);
                if (userid > 0)
                {
                    auc = CAS.GetAuctionDetail(AUC.productid);
                    auc.userid = (int)Session["userid"];
                    auc.ProxyAmt = AUC.ProxyAmt;
                    auc.ProxyAmtus = AUC.ProxyAmt / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);

                    // if user bid limit Amount Exceed or not

                    if(AUC.isOldUser == 0)
                    {
                        if (country == "India")
                        {
                            if (AUC.ProxyAmt >= amountlimt)
                            {
                                return Json(new { status = "AmountLimitExceeded" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            if (auc.ProxyAmtus >= amountlimt)
                            {
                                return Json(new { status = "AmountLimitExceeded" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }

                    // created on 24_12_2020
                    //check owner of paint if its match then user can not bid   

                    var result = auc.Ownerid.Split(',');
                    for (int i = 0; i < result.Length; i++)
                    {
                        int ownerid = int.Parse(result[i]);
                        if (ownerid == userid)
                        {
                            return Json(new { status = "Ownerpainting" }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    // check owner of paint if its match then user can not bid
                    //if (auc.Ownerid == userid)
                    //{
                    //    return Json(new { status = "Ownerpainting" }, JsonRequestBehavior.AllowGet);
                    //}


                    auc.username = Session["username"].ToString();

                    // 22_12_2020
                    string ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                    if (string.IsNullOrEmpty(ipAddress))
                    {
                        auc.ipAddress = Request.ServerVariables["REMOTE_ADDR"];
                    }
                    auc.userLocation = userLocation;
                    auc.fullAddress = fullAddress;
                    auc.latitude = latitude;
                    auc.longitude = longitude;
                    //

                    int Proxyid = CAS.InsertProxyBidRecord(auc);

                    // Update Bid Limit Function
                    if (AUC.isOldUser == 0)
                    {
                        int updateLimit = amountlimt - AUC.ProxyAmt;
                        US.UpdateBidLimit(auc.userid, updateLimit);
                        Session["BidLimit"] = updateLimit;
                    }



                    auc.email = Session["emailId"].ToString();
                    auc.name = Session["name"].ToString();
                    auc.lastname = Session["lastname"].ToString();
                    string Result = COMC.upcommingProxy(auc);


                    // Add to My Auction Gallary 
                    BidC.addToGallary(AUC);

                    return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { status = "SessionExpired" }, JsonRequestBehavior.AllowGet);
                }

                
            }
            catch (Exception Ex)
            {
                return Json(new { status = "Exception" }, JsonRequestBehavior.AllowGet);
            }
        }


        #region get MetaTags

        public static string UpdateMetaDetails(string pageUrl)
        {
            Utility util = new Utility();
            //--- StringBuilder object to store MetaTags information.
            StringBuilder sbMetaTags = new StringBuilder();

            //--Step1 Get data from database.

            DataTable dt = new DataTable();
            dt = util.Display("exec Proc_MetaTag 'bindMetatTag',0,'" + pageUrl.ToString() + "'");
            if (dt.Rows.Count > 0)
            {

                //---- Step2 In this step we will add <title> tag to our StringBuilder Object.
                sbMetaTags.Append("<title>" + dt.Rows[0]["Title"].ToString() + "</title>");

                //---- Step3 In this step we will add "Meta Description" to our StringBuilder Object.
                sbMetaTags.Append("<meta name='description' content='" + dt.Rows[0]["MetaDescription"].ToString() + "' >");

                //---- Step4 In this step we will add "Meta Keywords" to our StringBuilder Object.
                sbMetaTags.Append("<meta name='keywords' content ='" + dt.Rows[0]["MetaKeywords"].ToString() + "'>");
            }
            return sbMetaTags.ToString();
        }

        #endregion
    }
}