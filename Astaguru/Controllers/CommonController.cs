﻿using Astaguru.Helper;
using Astaguru.Models;
using Astaguru.Services;
using Astaguru.Services.UserService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Astaguru.Controllers
{
    public class CommonController : Controller
    {
        CurrentAuctionService CAS = new CurrentAuctionService();
        MyAuctionGallaryService MAS = new MyAuctionGallaryService();
        List<Auction> listProxyAuc = new List<Auction>();
        List<Auction> listAuc = new List<Auction>();
        List<Auction> aucBidRecord = new List<Auction>();
        Common COM = new Common();
        UserService US = new UserService();
        int curprice = 0;
        //
        // GET: /Common/
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public Auction AddProxybidRecords(Auction AUC)
        {
            Auction bidrecordfinal = new Auction();

            int currentuser = 0;
            List<int> userlist = new List<int>();
            listProxyAuc = CAS.GetProxyInfo(AUC);
            listAuc = CAS.GetAcutionData(AUC);
            int pricers = 0;
            //curprice = AUC.curprice;
            curprice = AUC.pricers;
            while (listProxyAuc.Count > 0)
            {
                if (listProxyAuc.Count == 1 && AUC.userid == listProxyAuc[0].userid && currentuser == AUC.userid)
                {
                    AUC.recentbid = 1;
                    AUC.currentbid = 1;
                    CAS.UpdateRecentCurrentBid(AUC);
                    return AUC;

                }
                else if (listProxyAuc.Count == 1 && currentuser == listProxyAuc[0].userid)
                {
                    AUC.recentbid = 1;
                    AUC.currentbid = 1;
                    AUC.userid = listProxyAuc[0].userid;
                    CAS.UpdateRecentCurrentBid(AUC);
                    return AUC;
                }

                foreach (var proxy in listProxyAuc)
                {
                    // Added to skip record if same user come again
                    if (currentuser == proxy.userid)
                    {
                        continue;
                    }


                    // Need To Check This
                    if (!userlist.Contains(proxy.userid))
                    {
                        userlist.Add(proxy.userid);
                        aucBidRecord = CAS.getBidUserList(AUC);

                        foreach (var userBid in aucBidRecord)
                        {
                            userBid.currentbid = 0;
                        }
                    }

                    pricers = proxy.ProxyAmt;

                    if (AUC.nextValidBidRs == 0)
                    {
                        AUC.nextValidBidRs = proxy.pricers;
                    }

                    // Need To Do This
                    // Dim val_increase As Decimal = 1.1
                    //If pricelow >= 10000000 Then
                    //    val_increase = 1.05
                    //End If

                    //Changed as last proxy record was missing...........If proxy.bid.ProxyAmt > pricelow * val_increase Then

                    //if (proxy.ProxyAmt >= AUC.nextValidBidRs)
                    //created on 26-01-2020
                    #region
                    if (proxy.ProxyAmt > AUC.nextValidBidRs)
                    #endregion
                    {

                        Auction bidRecord = new Auction();

                        List<Auction> listProxyBid = new List<Auction>();
                        listProxyBid = CAS.GetproxyUser(AUC);
                        if (listProxyBid.Count > 0)
                        {
                            foreach (var proxyuser in listProxyBid)
                            {
                                bidRecord.firstname = proxy.firstname;
                                bidRecord.lastname = proxy.lastname;
                                bidRecord.thumbnail = proxy.thumbnail;
                                bidRecord.productid = proxy.productid;
                                bidRecord.pricers = AUC.pricers;

                                AUC.mailPreprice = AUC.pricers;
                                AUC.mailPrepriceUs = AUC.mailPreprice / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);

                                bidRecord.nickname = proxy.nickname;
                                bidRecord.priceus = bidRecord.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.priceus;

                                if (proxyuser.ProxyAmt == AUC.nextValidBidRs)
                                {
                                    AUC.nextValidBidRs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(AUC.nextValidBidRs, 1).ToString());
                                    bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                                    bidRecord.nextValidBidUs = bidRecord.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.nextValidBidUs;
                                    curprice = bidRecord.nextValidBidRs;
                                }
                                else
                                {
                                    bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                                    bidRecord.nextValidBidUs = bidRecord.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.nextValidBidUs;
                                    curprice = bidRecord.nextValidBidRs;
                                }

                                // bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                                // bidRecord.nextValidBidUs = bidRecord.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.nextValidBidUs;
                                // curprice = bidRecord.nextValidBidRs;
                                //bidRecord.daterec = Now : Already taken in query

                                bidRecord.reference = proxy.reference;
                                bidRecord.anoname = proxy.nickname;
                                bidRecord.username = proxy.username;
                                bidRecord.currentbid = 0;
                                bidRecord.recentbid = 0;
                                bidRecord.userid = proxy.userid;
                                bidRecord.proxy = 1;
                                bidRecord.Online = proxy.Auctionid;
                                AUC.pricers = AUC.nextValidBidRs;
                                bidRecord.nickname = proxy.nickname;
                                bidRecord.isOldUser = proxy.isOldUser;

                                //21_12_2020
                                bidRecord.longitude = proxy.longitude;
                                bidRecord.latitude = proxy.latitude;
                                bidRecord.userLocation = proxy.userLocation;
                                bidRecord.fullAddress = proxy.fullAddress;
                                bidRecord.ipAddress = proxy.ipAddress;

                                //

                                // New 
                                bidrecordfinal = bidRecord;

                                AUC.Bidrecordid = CAS.InsertBidRecord(bidRecord);
                            }
                        }
                        else
                        {
                            bidRecord.firstname = proxy.firstname;
                            bidRecord.lastname = proxy.lastname;
                            bidRecord.thumbnail = proxy.thumbnail;
                            bidRecord.productid = proxy.productid;
                            bidRecord.pricers = AUC.pricers;

                            AUC.mailPreprice = AUC.pricers;
                            AUC.mailPrepriceUs = AUC.mailPreprice / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);

                            bidRecord.nickname = proxy.nickname;
                            bidRecord.priceus = bidRecord.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.priceus;

                            bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                            bidRecord.nextValidBidUs = bidRecord.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.nextValidBidUs;
                            curprice = bidRecord.nextValidBidRs;


                            // bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                            // bidRecord.nextValidBidUs = bidRecord.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.nextValidBidUs;
                            // curprice = bidRecord.nextValidBidRs;
                            //bidRecord.daterec = Now : Already taken in query

                            bidRecord.reference = proxy.reference;
                            bidRecord.anoname = proxy.nickname;
                            bidRecord.username = proxy.username;
                            bidRecord.currentbid = 0;
                            bidRecord.recentbid = 0;
                            bidRecord.userid = proxy.userid;
                            bidRecord.proxy = 1;
                            bidRecord.Online = proxy.Auctionid;
                            AUC.pricers = AUC.nextValidBidRs;
                            bidRecord.nickname = proxy.nickname;
                            bidRecord.isOldUser = proxy.isOldUser;

                            //21_12_2020
                            bidRecord.longitude = proxy.longitude;
                            bidRecord.latitude = proxy.latitude;
                            bidRecord.userLocation = proxy.userLocation;
                            bidRecord.fullAddress = proxy.fullAddress;
                            bidRecord.ipAddress = proxy.ipAddress;
                            //

                            // New 
                            bidrecordfinal = bidRecord;

                            AUC.Bidrecordid = CAS.InsertBidRecord(bidRecord);
                        }
                        if (AUC.Bidrecordid > 0)
                        {
                            AUC.nextValidBidRs = bidRecord.nextValidBidRs;
                            AUC.nextValidBidUs = AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); //bidRecord.nextValidBidUs;
                            CAS.UpdateAcutionPrice(AUC);
                        }
                        else
                        {
                            //Failed
                        }

                        AUC.nextValidBidRs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(AUC.nextValidBidRs, 1).ToString());
                        currentuser = bidRecord.userid;

                    }
                    //created on 26-01-2020
                    #region
                    else if (proxy.ProxyAmt == AUC.nextValidBidRs)
                    {
                        Auction bidRecord = new Auction();

                        List<Auction> listProxyBid = new List<Auction>();
                        listProxyBid = CAS.Getsameproxy(AUC);
                        foreach (var proxynew in listProxyBid)
                        {
                            bidRecord.firstname = proxynew.firstname;
                            bidRecord.lastname = proxynew.lastname;
                            bidRecord.thumbnail = proxynew.thumbnail;
                            bidRecord.productid = proxynew.productid;
                            bidRecord.pricers = AUC.pricers;
                            bidRecord.nickname = proxynew.nickname;
                            bidRecord.priceus = AUC.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
                            // AUC.priceus;
                            //bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                            //bidRecord.nextValidBidUs = AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);// proxy.ProxyAmtus;
                            //curprice = bidRecord.nextValidBidRs; // bidRecord.nextValidBidRs;
                            //bidRecord.daterec = Now : Already taken in query
                            //created on 12_02_2020
                            bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                            bidRecord.nextValidBidUs = AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);// proxy.ProxyAmtus;
                            curprice = AUC.nextValidBidRs;


                            bidRecord.reference = proxynew.reference;
                            bidRecord.anoname = proxynew.nickname;
                            bidRecord.username = proxynew.username;
                            bidRecord.currentbid = 0;
                            bidRecord.recentbid = 0;
                            bidRecord.userid = proxynew.userid;
                            bidRecord.proxy = 1;
                            bidRecord.Online = AUC.Auctionid;
                            AUC.pricers = AUC.nextValidBidRs;
                            // AUC.pricers = proxy.ProxyAmt;
                            bidRecord.nickname = proxynew.nickname;
                            bidRecord.isOldUser = proxynew.isOldUser;

                            //21_12_2020
                            bidRecord.longitude = proxy.longitude;
                            bidRecord.latitude = proxy.latitude;
                            bidRecord.userLocation = proxy.userLocation;
                            bidRecord.fullAddress = proxy.fullAddress;
                            bidRecord.ipAddress = proxy.ipAddress;
                            //

                            bidrecordfinal = bidRecord;
                            AUC.Bidrecordid = CAS.InsertBidRecord(bidRecord);

                        }

                        if (AUC.Bidrecordid > 0)
                        {
                            AUC.nextValidBidRs = bidRecord.nextValidBidRs;
                            AUC.nextValidBidUs = AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); //bidRecord.nextValidBidUs;
                            CAS.UpdateAcutionPrice(AUC);
                        }
                        else
                        {
                            //Failed
                        }



                        AUC.nextValidBidRs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(AUC.nextValidBidRs, 1).ToString());
                        currentuser = bidRecord.userid;
                    }
                    #endregion
                    else if (proxy.ProxyAmt > curprice || proxy.ProxyAmt == curprice)
                    {
                        //Auction bidRecord = new Auction();
                        //bidRecord.firstname = proxy.firstname;
                        //bidRecord.lastname = proxy.lastname;
                        //bidRecord.thumbnail = proxy.thumbnail;
                        //bidRecord.productid = proxy.productid;
                        //bidRecord.pricers = AUC.pricers;
                        //bidRecord.nickname = proxy.nickname;
                        //bidRecord.priceus = bidRecord.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.priceus;
                        //bidRecord.nextValidBidRs = proxy.ProxyAmt; // Next valid bid
                        //bidRecord.nextValidBidUs = bidRecord.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);// proxy.ProxyAmtus;
                        //curprice = proxy.ProxyAmt; // bidRecord.nextValidBidRs;
                        ////bidRecord.daterec = Now : Already taken in query

                        //bidRecord.reference = proxy.reference;
                        //bidRecord.anoname = proxy.nickname;
                        //bidRecord.username = proxy.username;
                        //bidRecord.currentbid = 0;
                        //bidRecord.recentbid = 0;
                        //bidRecord.userid = proxy.userid;
                        //bidRecord.proxy = 1;
                        //bidRecord.Online = proxy.Auctionid;
                        //AUC.pricers = AUC.nextValidBidRs;
                        //bidRecord.nickname = proxy.nickname;
                        //// New 
                        //bidrecordfinal = bidRecord;
                        //AUC.Bidrecordid = CAS.InsertBidRecord(bidRecord);

                        //created on 25-01-2020
                        #region
                        Auction bidRecord = new Auction();

                        List<Auction> listProxyBid = new List<Auction>();
                        listProxyBid = CAS.GetUpdatedProxyuser(AUC);
                        if (listProxyBid.Count > 0)
                        {
                            foreach (var proxynew in listProxyBid)
                            {
                                if (proxynew.ProxyAmt > curprice)
                                {
                                    bidRecord.firstname = proxynew.firstname;
                                    bidRecord.lastname = proxynew.lastname;
                                    bidRecord.thumbnail = proxynew.thumbnail;
                                    bidRecord.productid = proxynew.productid;
                                    bidRecord.pricers = AUC.pricers;
                                    bidRecord.nickname = proxynew.nickname;
                                    bidRecord.priceus = bidRecord.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.priceus;
                                    bidRecord.nextValidBidRs = proxynew.ProxyAmt; // Next valid bid
                                    bidRecord.nextValidBidUs = proxynew.ProxyAmt / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);// proxy.ProxyAmtus;
                                    curprice = proxynew.ProxyAmt; // bidRecord.nextValidBidRs;
                                                                  //bidRecord.daterec = Now : Already taken in query

                                    bidRecord.reference = proxynew.reference;
                                    bidRecord.anoname = proxynew.nickname;
                                    bidRecord.username = proxynew.username;
                                    bidRecord.currentbid = 0;
                                    bidRecord.recentbid = 0;
                                    bidRecord.userid = proxynew.userid;
                                    bidRecord.proxy = 1;
                                    bidRecord.Online = proxynew.Auctionid;

                                    AUC.pricers = bidRecord.nextValidBidRs;
                                    bidRecord.nickname = proxynew.nickname;
                                    bidRecord.isOldUser = proxynew.isOldUser;

                                    //21_12_2020
                                    bidRecord.longitude = proxy.longitude;
                                    bidRecord.latitude = proxy.latitude;
                                    bidRecord.userLocation = proxy.userLocation;
                                    bidRecord.fullAddress = proxy.fullAddress;
                                    bidRecord.ipAddress = proxy.ipAddress;
                                    //

                                    bidrecordfinal = bidRecord;
                                    AUC.Bidrecordid = CAS.InsertBidRecord(bidRecord);
                                    proxy.ProxyAmt = proxynew.ProxyAmt;
                                }
                                else if (proxynew.ProxyAmt == curprice)
                                {
                                    bidRecord.firstname = proxynew.firstname;
                                    bidRecord.lastname = proxynew.lastname;
                                    bidRecord.thumbnail = proxynew.thumbnail;
                                    bidRecord.productid = proxynew.productid;
                                    bidRecord.pricers = AUC.pricers;
                                    bidRecord.nickname = proxynew.nickname;
                                    bidRecord.priceus = AUC.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.priceus;
                                    bidRecord.nextValidBidRs = proxynew.ProxyAmt; // Next valid bid
                                    bidRecord.nextValidBidUs = proxynew.ProxyAmt / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);// proxy.ProxyAmtus;
                                    curprice = proxynew.ProxyAmt; // bidRecord.nextValidBidRs;
                                                                  //bidRecord.daterec = Now : Already taken in query

                                    bidRecord.reference = proxynew.reference;
                                    bidRecord.anoname = proxynew.nickname;
                                    bidRecord.username = proxynew.username;
                                    bidRecord.currentbid = 0;
                                    bidRecord.recentbid = 0;
                                    bidRecord.userid = proxynew.userid;
                                    bidRecord.proxy = 1;
                                    bidRecord.Online = proxynew.Auctionid;
                                    AUC.pricers = AUC.nextValidBidRs;
                                    bidRecord.nickname = proxynew.nickname;
                                    bidRecord.isOldUser = proxynew.isOldUser;

                                    //21_12_2020
                                    bidRecord.longitude = proxy.longitude;
                                    bidRecord.latitude = proxy.latitude;
                                    bidRecord.userLocation = proxy.userLocation;
                                    bidRecord.fullAddress = proxy.fullAddress;
                                    bidRecord.ipAddress = proxy.ipAddress;
                                    //

                                    bidrecordfinal = bidRecord;
                                    AUC.Bidrecordid = CAS.InsertBidRecord(bidRecord);
                                    proxy.ProxyAmt = proxynew.ProxyAmt;
                                }
                            }
                        }
                        #endregion


                        if (AUC.Bidrecordid > 0)
                        {
                            AUC.nextValidBidRs = proxy.ProxyAmt;

                            AUC.nextValidBidUs = AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); //proxy.ProxyAmtus;
                            CAS.UpdateAcutionPrice(AUC);
                        }
                        else
                        {
                            //Failed
                        }

                        AUC.nextValidBidRs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(proxy.ProxyAmt, 1).ToString());
                        currentuser = bidRecord.userid;
                    }

                }
                AUC.curprice = curprice;
                listProxyAuc = CAS.GetUpdatedProxyInfo(AUC);

            }


            if (listProxyAuc.Count == 0)
            {

                if (bidrecordfinal.userid > 0)
                {
                    return bidrecordfinal;
                }
                else
                {
                    return null;
                }



            }
            else
            {
                AUC.nextValidBidRs = 0;
                AUC.recentbid = 1;
                AUC.currentbid = 1;
            }



            return AUC;
        }


        [HttpPost]
        public ActionResult GetInTouch(GetInTouch GIT)
        {
            Sendmail(GIT);
            return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
        }


        public int checkUserSessionValue()
        {
            int result = 0;// string.Empty;
            try
            {
                result = Convert.ToInt32(Session["userid"]);

            }
            catch (Exception Ex)
            {

            }
            return result;
        }

        public void Sendmail(GetInTouch GIT)
        {
            string FromEmail = ConfigurationManager.AppSettings["fromId"];
            string Password = ConfigurationManager.AppSettings["smtpPassword"];
            string ToEmail = GIT.Email;
            try
            {
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(FromEmail, "contact@astaguru.com");
                msg.To.Add(ToEmail);
                msg.Subject = "Enquiry on www.astaguru.com";
                string str = string.Empty;
                str = "Hi " + GIT.name + ",";
                str += "<br>Thank you for enquiring on www.astaguru.com. Your request details are mentioned below. ";
                str += "<br><br>Name: " + GIT.name;
                str += "<br>Email: " + GIT.Email;
                str += "<br>Telephone: " + GIT.Phone;
                str += "<br>Category: " + GIT.Category;
                str += "<br>Message: " + GIT.Message;
                str += "<br><br>Thanking You,";
                str += "<br>Warm Regards,";
                str += "<br>The Team of AstaGuru";

                //msg.Body = str;
                msg.IsBodyHtml = true;
                string html = str;
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(html, new ContentType("text/html"));
                msg.AlternateViews.Add(htmlView);
                //SmtpClient sc = new SmtpClient("sarpstechnologies.com", 25);
                SmtpClient sc = new SmtpClient(ConfigurationManager.AppSettings["smtpHost"], Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]));
                sc.Host = "smtp.gmail.com";
                sc.Port = 587;
                sc.UseDefaultCredentials = true;
                // sc.Host = "sarpstechnologies.com";
                // sc.Port = 25;
                sc.Credentials = new NetworkCredential(FromEmail, Password);
                sc.EnableSsl = true; // Hide this If u use instead of gmail
                sc.Send(msg);
            }
            catch (Exception ex)
            {
                TempData["ERRGetInTouch"] = ex.Message;
            }
        }


        [HttpPost]
        public ActionResult setCurrency(Auction Auc)
        {
            Session["GetCurrency"] = Auc.currency;
            return Json(new { Success = "Set Currency" }, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public ActionResult generateCurrentAuction(Auction Auc)
        {
            Auc.Subquery = "";
            int userid = Convert.ToInt32(Session["userid"]);
            int currency = Convert.ToInt32(Session["GetCurrency"]);
            string dynamicAuction = string.Empty;
            string baseUrlPath = Astaguru.Services.Common.baseUrlPath;


            List<Astaguru.Models.Auction> CurrentAuction = null;
            CurrentAuction = CAS.GetCurrentAuctionList(Auc.Subquery, Auc.page);

            if (CurrentAuction.Count > 0)
            {
                foreach (var item in CurrentAuction)
                {
                    int usdCurrentPrice = item.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
                    int NextValidBid = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(@item.pricers, 1));
                    int usdNextValidPrice = NextValidBid / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);

                    dynamicAuction += "<div class=\"col-sm-6 col-lg-4 col-xl-3 extra-padding-top-40\">";
                    dynamicAuction += "<div class=\"box_col panel-body hover_zoom\">";
                    dynamicAuction += "<div class=\"flip-container\">";
                    dynamicAuction += "<div class=\"flipper\">";
                    dynamicAuction += "<div class=\"front img_box_col\">";
                    dynamicAuction += "<img src=\"http://astaguru.com:81/" + item.thumbnail + "\" class=\"img_responsive\"/>";
                    dynamicAuction += "</div>";
                    dynamicAuction += "<div class=\"after_flipe img_box_col\">";
                    dynamicAuction += "<div>";
                    dynamicAuction += "<h6>Medium</h6>";
                    dynamicAuction += "<p>" + item.medium + "</p>";
                    dynamicAuction += "<h6>Size</h6>";
                    dynamicAuction += "<p>" + item.productsize + "</p>";
                    dynamicAuction += "<h6>Year</h6>";
                    dynamicAuction += "<p>" + item.Date + "</p>";
                    dynamicAuction += "</div>";
                    dynamicAuction += "</div><!-- after_flipe -->";
                    dynamicAuction += "</div><!-- flipper -->";
                    dynamicAuction += "</div><!-- flip-container -->";
                    dynamicAuction += "<div class=\"col_box_content\">";
                    dynamicAuction += "<a href=\"http://astaguru.com:81/" + item.image + "\" class=\"fancybox\" rel=\"gallery2\"><span class=\"icon_expand\"><i class=\"icon-fullscreen\"></i></span></a>";
                    dynamicAuction += "<a href=\"" + baseUrlPath + "/Artist/artistProfile?artistid=" + item.artistid + "\" target=\"_blank\">";
                    dynamicAuction += "<h3 class=\"uppercase nowrap_text sbold\"> " + item.firstname + item.lastname + "</h3>";
                    dynamicAuction += "</a>";
                    dynamicAuction += "<p><a href=" + baseUrlPath + "/Home/LotDetails?productid=" + item.productid + "><span class=\"lotno\">Lot " + item.reference + "</span> " + item.title + "</a></p>";
                    dynamicAuction += "<div class=\"row\">";
                    if (currency == 1)
                    {
                        dynamicAuction += "<div class=\"col-xs-6 col-sm-6 divider_right\">";
                        dynamicAuction += "<div class=\"price_live_auc\">";
                        dynamicAuction += "<h6 class=\"uppercase\">current bid</h6>";
                        dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(@item.pricers.ToString()) + "</h5>";
                        dynamicAuction += "</div>";
                        dynamicAuction += "</div><!-- col -->";
                        if (item.timeRemains < 0)
                        {
                            dynamicAuction += "";
                        }
                        else
                        {
                            dynamicAuction += "<div class=\"col-xs-6 col-sm-6\">";
                            dynamicAuction += "<div class=\"price_live_auc\">";
                            if (userid == item.MyUserID)
                            {
                                dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid current_leading\">Currently leading</h6>";
                            }
                            else
                            {
                                dynamicAuction += "<h6 class=\"uppercase\">next valid bid</h6>";
                                dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(Astaguru.Services.Common.getNextValidBidAmount(@item.pricers, currency).ToString()) + "</h5>";
                            }
                            dynamicAuction += "</div>";
                            dynamicAuction += " </div><!-- col -->";
                        }

                    }
                    else if (currency == 2)
                    {
                        dynamicAuction += "<div class=\"col-xs-6 col-sm-6 divider_right\">";
                        dynamicAuction += "<div class=\"price_live_auc\">";
                        dynamicAuction += "<h6 class=\"uppercase\">current bid</h6>";
                        dynamicAuction += "<h5>$ " + Astaguru.Services.Common.DollerFormat(usdCurrentPrice.ToString()) + "</h5>";
                        dynamicAuction += "</div>";
                        dynamicAuction += "</div><!-- col -->";


                        if (item.timeRemains < 0)
                        {

                        }
                        else
                        {
                            dynamicAuction += "<div class=\"col-xs-6 col-sm-6\">";
                            dynamicAuction += "<div class=\"price_live_auc\">";


                            if (userid == item.MyUserID)
                            {
                                dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid current_leading\">Currently leading</h6>";
                            }
                            else
                            {
                                dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid\">next valid bid</h6>";
                                dynamicAuction += "<h5>$ " + Astaguru.Services.Common.DollerFormat(usdNextValidPrice.ToString()) + " </h5>";
                            }
                            dynamicAuction += "</div>";
                            dynamicAuction += "</div><!-- col -->";
                        }
                    }
                    else
                    {
                        dynamicAuction += "<div class=\"col-xs-6 col-sm-6 divider_right\">";
                        dynamicAuction += "<div class=\"price_live_auc\">";
                        dynamicAuction += "<h6 class=\"uppercase\">current bid</h6>";
                        dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(@item.pricers.ToString()) + "</h5>";
                        dynamicAuction += "</div>";
                        dynamicAuction += "</div><!-- col -->";
                        if (item.timeRemains < 0)
                        {
                            dynamicAuction += "";
                        }
                        else
                        {
                            dynamicAuction += "<div class=\"col-xs-6 col-sm-6\">";
                            dynamicAuction += "<div class=\"price_live_auc\">";
                            if (userid == item.MyUserID)
                            {
                                dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid current_leading\">Currently leading</h6>";
                            }
                            else
                            {
                                dynamicAuction += "<h6 class=\"uppercase\">next valid bid</h6>";
                                dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(Astaguru.Services.Common.getNextValidBidAmount(@item.pricers, currency).ToString()) + "</h5>";
                            }
                            dynamicAuction += "</div>";
                            dynamicAuction += " </div><!-- col -->";
                        }
                    }

                    dynamicAuction += "</div><!-- row -->";
                    dynamicAuction += "<div class=\"live_auction_timer\">";
                    dynamicAuction += "<div class=\"timer_wrap\">";




                    if (item.timeRemains < 0)
                    {
                        dynamicAuction += "<div><span style=\"color:red\" class=\"close_auction\">Bid Closed</span></div>";



                    }
                    else
                    {
                        dynamicAuction += "<div>";
                        dynamicAuction += "<span>" + item.myBidClosingTime + "</span>";
                        dynamicAuction += "</div>";

                        dynamicAuction += "<!-- <span class=\"product_left\">left</span> -->";

                    }
                    dynamicAuction += "</div>";



                    if (item.timeRemains < 0)
                    {

                        dynamicAuction += "<a href=\"JavaScript:Void(0);\" class=\"btn_bid disabled\">Bid</a>";

                    }
                    else
                    {
                        if (userid == item.MyUserID)
                        {
                            dynamicAuction += "<a href=\"JavaScript:Void(0);\" class=\"btn_bid disabled\">Bid</a>";

                        }
                        else
                        {
                            dynamicAuction += "<a href=\"JavaScript:Void(0);\" class=\"btn_bid btnBidNow\" data-id=\"" + item.productid + "\">Bid</a>";

                        }

                    }
                    dynamicAuction += "<script>";
                    dynamicAuction += "$('#btnBidNow_" + item.productid + "').click(function () {";
                    dynamicAuction += "$('#LoginForm').modal('toggle');";
                    dynamicAuction += "});";
                    dynamicAuction += "</script>";
                    dynamicAuction += "<div class=\"clear\"></div>";
                    dynamicAuction += "</div><!-- live_auction_timer -->";
                    dynamicAuction += "<div class=\"clear\"></div>";
                    dynamicAuction += "<span id=\"Success_" + item.productid + "\" style=\"color:green\" class=\"clsClearGallary\"></span>";
                    dynamicAuction += "<span id=\"Err_" + item.productid + "\" style=\"color:red\" class=\"clsClearGallary\"></span>";

                    dynamicAuction += "</div><!-- col_auctions_content -->";
                    dynamicAuction += "<a href=\"javascript:avoid(0)\" class=\"add_tocart addToCart\" data-productid=\"" + item.productid + "\"><i class=\"icon-plus-circle\"></i></a>";

                    dynamicAuction += "</div><!-- box_col -->";
                    dynamicAuction += "</div><!-- col -->";
                }
            }
            return Json(new { Success = dynamicAuction }, JsonRequestBehavior.AllowGet);
        }
        public struct dynamicData
        {
            public int productid;
            public string dyanmicData;
        }

        [HttpPost]
        public ActionResult generateCurrentAuction1(Auction Auc)
        {
            // Dynamic List For All Type Of List
            List<Astaguru.Models.Auction> CurrentAuction = null;
            int auctionstyle = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["auctionstyle"]);
            int auctionId = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["auctionId"]);
            List<dynamicData> listDynamicAuction = new List<dynamicData>();

            Auc.Subquery = "";
            int userid = Convert.ToInt32(Session["userid"]);
            int currency = Convert.ToInt32(Session["GetCurrency"]);

            string baseUrlPath = Astaguru.Services.Common.baseUrlPath;

            if (Auc.pageName == "Current")
            {
                CurrentAuction = CAS.GetCurrentAuctionList(Auc.Subquery, Auc.page);
            }
            else if (Auc.pageName == "LatestBid")
            {
                CurrentAuction = CAS.GetLatestBid();
            }
            else if (Auc.pageName == "Lotssignificant")
            {
                CurrentAuction = CAS.GetHighValueBid();
            }
            else if (Auc.pageName == "CurrentMostPopular")
            {
                CurrentAuction = CAS.GetMostPopularLots();
            }
            else if (Auc.pageName == "CurrentClosingLots")
            {
                CurrentAuction = CAS.GetClosingLots();
            }


            if (CurrentAuction.Count > 0)
            {
                foreach (var item in CurrentAuction)
                {
                    string dynamicAuction = string.Empty;

                    int usdCurrentPrice = item.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
                    int NextValidBid = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(@item.pricers, 1));
                    int usdNextValidPrice = NextValidBid / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);


                    dynamicAuction += "<div class=\"row\">";
                    if (currency == 1)
                    {
                        if (item.timeRemains < 0)
                        {
                            if (item.pricelow > item.pricers)
                            {
                                dynamicAuction += "<div class=\"col-xs-6 col-sm-6 divider_right\">";
                                dynamicAuction += "<div class=\"price_live_auc\">";                              
                                dynamicAuction += "<h5>Bought In</h5>";
                                dynamicAuction += "</div>";
                                dynamicAuction += "</div><!-- col -->";
                            }
                            else
                            {
                                //show price with 15% margin added after Bid Closed
                                                                                 
                                long pricers = item.pricers;    //use long for large pricers calculation
                                long margin = pricers * 15;
                                margin = margin / 100;
                                long margin_pricers = pricers + margin;


                                dynamicAuction += "<div class=\"col-xs-6 col-sm-6 divider_right\">";
                                dynamicAuction += "<div class=\"price_live_auc\">";                             
                                dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(margin_pricers.ToString()) + "</h5>";
                                dynamicAuction += "<h6 style=\"font-size:10px\" > (Inclusive 15% margin)</h6>";
                                dynamicAuction += "</div>";
                                dynamicAuction += "</div><!-- col -->";
                            }

                        }
                        else
                        {
                            dynamicAuction += "<div class=\"col-xs-6 col-sm-6 divider_right\">";
                            dynamicAuction += "<div class=\"price_live_auc\">";
                            dynamicAuction += "<h6 class=\"uppercase\">current bid</h6>";
                            dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(@item.pricers.ToString()) + "</h5>";
                            dynamicAuction += "</div>";
                            dynamicAuction += "</div><!-- col -->";
                        }
                       

                        if (item.timeRemains < 0)
                        {
                            //if (userid == item.MyUserID && item.MyUserID != 0)
                            //{
                            //    dynamicAuction += "<div class=\"col-xs-6 col-sm-6\">";
                            //    dynamicAuction += "<div class=\"price_live_auc\">";
                            //    dynamicAuction += " <h6 class=\"uppercase text_next_valid_bid current_leading\">WON</h6>";
                            //    dynamicAuction += "</div>";
                            //    dynamicAuction += "</div><!-- col -->";
                            //}
                            //else
                            //{
                            //    dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                            //}

                            if (userid == item.MyUserID && item.MyUserID != 0)
                            {
                                if (item.pricelow < item.pricers)
                                {
                                    dynamicAuction += "<div class=\"col-xs-6 col-sm-6\">";
                                    dynamicAuction += "<div class=\"price_live_auc\">";
                                    dynamicAuction += " <h6 class=\"uppercase text_next_valid_bid current_leading\">WON</h6>";
                                    dynamicAuction += "</div>";
                                    dynamicAuction += "</div><!-- col -->";
                                }
                                else {
                                    dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                                }
                            }
                            else
                            {
                                dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                            }


                        }
                        else
                        {
                            dynamicAuction += "<div class=\"col-xs-6 col-sm-6\">";
                            dynamicAuction += "<div class=\"price_live_auc\">";
                            if (userid == item.MyUserID && item.MyUserID != 0)
                            {
                                dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid current_leading\">Currently leading</h6>";
                            }
                            else
                            {
                                dynamicAuction += "<h6 class=\"uppercase\">next valid bid</h6>";
                                dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(Astaguru.Services.Common.getNextValidBidAmount(@item.pricers, currency).ToString()) + "</h5>";
                            }
                            dynamicAuction += "</div>";
                            dynamicAuction += " </div><!-- col -->";
                        }

                    }
                    else if (currency == 2)
                    {
                        if (item.timeRemains < 0)
                        {
                            if (item.pricelow > item.pricers)
                            {
                                dynamicAuction += "<div class=\"col-xs-6 col-sm-6 divider_right\">";
                                dynamicAuction += "<div class=\"price_live_auc\">";
                                dynamicAuction += "<h5>Bought In</h5>";
                                dynamicAuction += "</div>";
                                dynamicAuction += "</div><!-- col -->";
                            }
                            else
                            {
                                //show price with 15% margin added after Bid Closed
                                int margin = usdCurrentPrice * 15;
                                margin = margin / 100;
                                int margin_priceus = usdCurrentPrice + margin;

                                dynamicAuction += "<div class=\"col-xs-6 col-sm-6 divider_right\">";
                                dynamicAuction += "<div class=\"price_live_auc\">";
                                dynamicAuction += "<h5>$ " + Astaguru.Services.Common.rupeeFormat(margin_priceus.ToString()) + "</h5>";
                                dynamicAuction += "<h6 style=\"font-size:10px\" > (Inclusive 15% margin)</h6>";
                                dynamicAuction += "</div>";
                                dynamicAuction += "</div><!-- col -->";
                            }

                        }
                        else
                        {

                            dynamicAuction += "<div class=\"col-xs-6 col-sm-6 divider_right\">";
                            dynamicAuction += "<div class=\"price_live_auc\">";
                            dynamicAuction += "<h6 class=\"uppercase\">current bid</h6>";
                            dynamicAuction += "<h5>$ " + Astaguru.Services.Common.DollerFormat(usdCurrentPrice.ToString()) + "</h5>";
                            dynamicAuction += "</div>";
                            dynamicAuction += "</div><!-- col -->";
                        }





                        if (item.timeRemains < 0)
                        {
                            //if (userid == item.MyUserID && item.MyUserID != 0)
                            //{
                            //    dynamicAuction += "<div class=\"col-xs-6 col-sm-6\">";
                            //    dynamicAuction += "<div class=\"price_live_auc\">";
                            //    dynamicAuction += " <h6 class=\"uppercase text_next_valid_bid current_leading\">WON</h6>";
                            //    dynamicAuction += "</div>";
                            //    dynamicAuction += "</div><!-- col -->";
                            //}
                            //else
                            //{
                            //    dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                            //}

                            if (userid == item.MyUserID && item.MyUserID != 0)
                            {
                                if (item.pricelow < item.pricers)
                                {
                                    dynamicAuction += "<div class=\"col-xs-6 col-sm-6\">";
                                    dynamicAuction += "<div class=\"price_live_auc\">";
                                    dynamicAuction += " <h6 class=\"uppercase text_next_valid_bid current_leading\">WON</h6>";
                                    dynamicAuction += "</div>";
                                    dynamicAuction += "</div><!-- col -->";
                                }
                                else
                                {
                                    dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                                }
                            }
                            else
                            {
                                dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                            }
                        }
                        else
                        {
                            dynamicAuction += "<div class=\"col-xs-6 col-sm-6\">";
                            dynamicAuction += "<div class=\"price_live_auc\">";


                            if (userid == item.MyUserID && item.MyUserID != 0)
                            {
                                dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid current_leading\">Currently leading</h6>";
                            }
                            else
                            {
                                dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid\">next valid bid</h6>";
                                dynamicAuction += "<h5>$ " + Astaguru.Services.Common.DollerFormat(usdNextValidPrice.ToString()) + " </h5>";
                            }
                            dynamicAuction += "</div>";
                            dynamicAuction += "</div><!-- col -->";
                        }
                    }
                    else
                    {
                        if (item.timeRemains < 0)
                        {
                            if (item.pricelow > item.pricers)
                            {
                                dynamicAuction += "<div class=\"col-xs-6 col-sm-6 divider_right\">";
                                dynamicAuction += "<div class=\"price_live_auc\">";
                                dynamicAuction += "<h5>Bought In</h5>";
                                dynamicAuction += "</div>";
                                dynamicAuction += "</div><!-- col -->";
                            }
                            else
                            {
                                //show price with 15% margin added after Bid Closed

                                long pricers = item.pricers;    //use long for large pricers calculation
                                long margin = pricers * 15;
                                margin = margin / 100;
                                long margin_pricers = pricers + margin;

                                dynamicAuction += "<div class=\"col-xs-6 col-sm-6 divider_right\">";
                                dynamicAuction += "<div class=\"price_live_auc\">";
                                dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(margin_pricers.ToString()) + "</h5>";
                                dynamicAuction += "<h6 style=\"font-size:10px\" > (Inclusive 15% margin)</h6>";
                                dynamicAuction += "</div>";
                                dynamicAuction += "</div><!-- col -->";
                            }

                        }
                        else
                        {
                            dynamicAuction += "<div class=\"col-xs-6 col-sm-6 divider_right\">";
                            dynamicAuction += "<div class=\"price_live_auc\">";
                            dynamicAuction += "<h6 class=\"uppercase\">current bid</h6>";
                            dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(@item.pricers.ToString()) + "</h5>";
                            dynamicAuction += "</div>";
                            dynamicAuction += "</div><!-- col -->";
                        }


                        if (item.timeRemains < 0)
                        {
                            //if (userid == item.MyUserID && item.MyUserID != 0)
                            //{
                            //    dynamicAuction += "<div class=\"col-xs-6 col-sm-6\">";
                            //    dynamicAuction += "<div class=\"price_live_auc\">";
                            //    dynamicAuction += " <h6 class=\"uppercase text_next_valid_bid current_leading\">WON</h6>";
                            //    dynamicAuction += "</div>";
                            //    dynamicAuction += "</div><!-- col -->";
                            //}
                            //else
                            //{
                            //    dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                            //}

                            if (userid == item.MyUserID && item.MyUserID != 0)
                            {
                                if (item.pricelow < item.pricers)
                                {
                                    dynamicAuction += "<div class=\"col-xs-6 col-sm-6\">";
                                    dynamicAuction += "<div class=\"price_live_auc\">";
                                    dynamicAuction += " <h6 class=\"uppercase text_next_valid_bid current_leading\">WON</h6>";
                                    dynamicAuction += "</div>";
                                    dynamicAuction += "</div><!-- col -->";
                                }
                                else
                                {
                                    dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                                }
                            }
                            else
                            {
                                dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                            }
                        }
                        else
                        {
                            dynamicAuction += "<div class=\"col-xs-6 col-sm-6\">";
                            dynamicAuction += "<div class=\"price_live_auc\">";
                            if (userid == item.MyUserID && item.MyUserID != 0)
                            {
                                dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid current_leading\">Currently leading</h6>";
                            }
                            else
                            {
                                dynamicAuction += "<h6 class=\"uppercase\">next valid bid</h6>";
                                dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(Astaguru.Services.Common.getNextValidBidAmount(@item.pricers, currency).ToString()) + "</h5>";
                            }
                            dynamicAuction += "</div>";
                            dynamicAuction += " </div><!-- col -->";
                        }
                    }

                    dynamicAuction += "</div><!-- row -->";

                    dynamicAuction += "<div class=\"live_auction_timer\">";
                    dynamicAuction += "<div class=\"timer_wrap\">";




                    if (item.timeRemains < 0)
                    {
                        if (userid == item.MyUserID)
                        {

                        }
                        else
                        {
                            //dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                        }


                    }
                    else
                    {
                        dynamicAuction += "<div class=\"timer_count\">";
                        dynamicAuction += "<figure>";
                        dynamicAuction += "<ul>";
                        dynamicAuction += "<li>" + item.hh + "</li>";
                        dynamicAuction += " </ul>";
                        dynamicAuction += " <p>Hours</p>";
                        dynamicAuction += "</figure>";
                        dynamicAuction += "<span>:</span>";
                        dynamicAuction += "<figure>";
                        dynamicAuction += "<ul>";
                        dynamicAuction += "<li>" + item.mm + "</li>";
                        dynamicAuction += "</ul>";
                        dynamicAuction += "<p>Minute</p>";
                        dynamicAuction += "</figure>";
                        dynamicAuction += "<span>:</span>";
                        dynamicAuction += "<figure>";
                        dynamicAuction += "<ul>";
                        dynamicAuction += "<li>" + item.ss + "</li>";
                        dynamicAuction += " </ul>";
                        dynamicAuction += " <p>Second</p>";
                        dynamicAuction += "</figure>";
                        dynamicAuction += "</div>";

                    }
                    dynamicAuction += "</div>";

                    if (item.timeRemains < 0)
                    { }
                    else
                    {
                        if (userid == item.MyUserID)
                        {

                        }
                        else
                        {
                            dynamicAuction += "<a href=\"JavaScript:Void(0);\" class=\"btn_bid btnBidNow\" data-id=\"" + item.productid + "\">Bid</a>";
                        }

                    }
                    dynamicAuction += "</div><!-- live_auction_timer -->";
                    dynamicAuction += "<div class=\"clear\"></div>";

                    // Add Data in List 
                    dynamicData DD = new dynamicData();
                    DD.productid = item.productid;
                    DD.dyanmicData = dynamicAuction;
                    listDynamicAuction.Add(DD);

                }
            }
            return Json(new { Success = listDynamicAuction }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult generateCurrentAuctionList(Auction Auc)
        {
            List<Astaguru.Models.Auction> CurrentAuction = null;

            List<dynamicData> listDynamicAuction = new List<dynamicData>();
            List<dynamicData> listDynamicAuctionButton = new List<dynamicData>();

            Auc.Subquery = "";
            int userid = Convert.ToInt32(Session["userid"]);
            int currency = Convert.ToInt32(Session["GetCurrency"]);

            string baseUrlPath = Astaguru.Services.Common.baseUrlPath;

            if (Auc.pageName == "Current")
            {
                CurrentAuction = CAS.GetCurrentAuctionList(Auc.Subquery, Auc.page);
            }
            else if (Auc.pageName == "LatestBid")
            {
                CurrentAuction = CAS.GetLatestBid();
            }
            else if (Auc.pageName == "Lotssignificant")
            {
                CurrentAuction = CAS.GetHighValueBid();
            }
            else if (Auc.pageName == "CurrentMostPopular")
            {
                CurrentAuction = CAS.GetMostPopularLots();
            }
            else if (Auc.pageName == "CurrentClosingLots")
            {
                CurrentAuction = CAS.GetClosingLots();
            }


            if (CurrentAuction.Count > 0)
            {
                foreach (var item in CurrentAuction)
                {
                    string dynamicAuction = string.Empty;
                    string dynamicAuctionButton = string.Empty;

                    int usdCurrentPrice = item.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
                    int NextValidBid = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(@item.pricers, 1));
                    int usdNextValidPrice = NextValidBid / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);




                    if (currency == 1)
                    {

                        if (item.timeRemains < 0)
                        {
                            if (item.pricelow > item.pricers)
                            {

                                dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                                dynamicAuction += "<div class=\"price_live_auc price_current_next divider_right\">";
                                dynamicAuction += "<h5>Bought In</h5>";
                                dynamicAuction += "</div><!-- price_live_auc -->";
                                dynamicAuction += "</div><!-- col -->";
                            }
                            else
                            {
                                //show pricers with 15% margin added after Bid Closed     
                                                                               
                                long pricers = item.pricers;    //use long for large pricers calculation
                                long margin = pricers * 15;
                                margin = margin / 100;
                                long margin_pricers = pricers + margin;

                                dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                                dynamicAuction += "<div class=\"price_live_auc price_current_next divider_right\">";
                                dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(margin_pricers.ToString()) + "</h5>";
                                dynamicAuction += "<h6 class=\"\">(Inclusive 15% margin)</h6>";
                                dynamicAuction += "</div><!-- price_live_auc -->";
                                dynamicAuction += "</div><!-- col -->";

                            }

                        }
                        else
                        {
                            dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                            dynamicAuction += "<div class=\"price_live_auc price_current_next divider_right\">";
                            dynamicAuction += "<h6 class=\"uppercase\">current bid</h6>";
                            dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(@item.pricers.ToString()) + "</h5>";
                            dynamicAuction += "</div><!-- price_live_auc -->";
                            dynamicAuction += "</div><!-- col -->";
                        }




                        //if (item.timeRemains < 0)
                        //{
                        //    if (userid == item.MyUserID && item.MyUserID != 0)
                        //    {
                        //        dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                        //        dynamicAuction += "<div class=\"price_live_auc price_current_next\">";
                        //        dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid current_leading\">WON</h6>";
                        //        dynamicAuction += "</div><!-- price_live_auc -->";
                        //        dynamicAuction += "</div><!-- col -->";
                        //    }
                        //    else
                        //    {
                        //        dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                        //    }

                        //}
                        if (item.timeRemains < 0)
                        {
                           
                            if (userid == item.MyUserID && item.MyUserID != 0)
                            {
                                if (item.pricelow < item.pricers)
                                {
                                    dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                                    dynamicAuction += "<div class=\"price_live_auc price_current_next\">";
                                    dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid current_leading\">WON</h6>";
                                    dynamicAuction += "</div><!-- price_live_auc -->";
                                    dynamicAuction += "</div><!-- col -->";
                                }
                                else
                                {
                                    dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                                }
                            }
                            else
                            {
                                dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                            }
                        }
                        else
                        {
                            dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                            dynamicAuction += "<div class=\"price_live_auc price_current_next\">";
                            if (userid == item.MyUserID && item.MyUserID != 0)
                            {
                                dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid current_leading\">Currently <br> leading</h6>";
                            }
                            else
                            {
                                dynamicAuction += "<h6 class=\"uppercase\">next valid bid</h6>";
                                dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(Astaguru.Services.Common.getNextValidBidAmount(@item.pricers, currency).ToString()) + "</h5>";

                            }
                            dynamicAuction += "</div><!-- price_live_auc -->";
                            dynamicAuction += "</div><!-- col -->";

                        }


                    }
                    else if (currency == 2)
                    {
                        if (item.timeRemains < 0)
                        {
                            if (item.pricelow > item.pricers)
                            {
                                dynamicAuction += "<div class=\"col-xs-6 col-sm-6 divider_right\">";
                                dynamicAuction += "<div class=\"price_live_auc\">";
                                dynamicAuction += "<h5>Bought In</h5>";
                                dynamicAuction += "</div>";
                                dynamicAuction += "</div><!-- col -->";
                            }
                            else
                            {
                                //show price with 15% margin added after Bid Closed
                                int margin = usdCurrentPrice * 15;
                                margin = margin / 100;
                                int margin_priceus = usdCurrentPrice + margin;

                                dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                                dynamicAuction += "<div class=\"price_live_auc price_current_next divider_right\">";
                                dynamicAuction += "<h5>$ " + Astaguru.Services.Common.DollerFormat(margin_priceus.ToString()) + "</h5>";
                                dynamicAuction += "<h6 class=\"\">(Inclusive 15% margin)</h6>";
                                dynamicAuction += "</div><!-- price_live_auc -->";
                                dynamicAuction += "</div><!-- col -->";

                            }

                        }
                        else
                        {
                            dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                            dynamicAuction += "<div class=\"price_live_auc price_current_next divider_right\">";
                            dynamicAuction += "<h6 class=\"uppercase\">current bid</h6>";
                            dynamicAuction += "<h5>$ " + Astaguru.Services.Common.DollerFormat(@item.priceus.ToString()) + "</h5>";
                            dynamicAuction += "</div><!-- price_live_auc -->";
                            dynamicAuction += "</div><!-- col -->";
                        }

                        if (item.timeRemains < 0)
                        {

                            if (userid == item.MyUserID && item.MyUserID != 0)
                            {
                                if (item.pricelow < item.pricers)
                                {
                                    dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                                    dynamicAuction += "<div class=\"price_live_auc price_current_next\">";
                                    dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid current_leading\">WON</h6>";
                                    dynamicAuction += "</div><!-- price_live_auc -->";
                                    dynamicAuction += "</div><!-- col -->";
                                }
                                else
                                {
                                    dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                                }
                            }
                            else
                            {
                                dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                            }
                        }
                        else
                        {
                            dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                            dynamicAuction += "<div class=\"price_live_auc price_current_next\">";

                            if (userid == item.MyUserID && item.MyUserID != 0)
                            {
                                dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid current_leading\">Currently <br> leading</h6>";

                            }
                            else
                            {
                                dynamicAuction += "<h6 class=\"uppercase\">next valid bid</h6>";
                                dynamicAuction += "<h5>$ " + Astaguru.Services.Common.DollerFormat(Astaguru.Services.Common.getNextValidBidAmount(@item.priceus, currency).ToString()) + "</h5>";


                            }
                            dynamicAuction += "</div><!-- price_live_auc -->";
                            dynamicAuction += "</div><!-- col -->";

                        }

                    }
                    else
                    {
                        if (item.timeRemains < 0)
                        {
                            if (item.pricelow > item.pricers)
                            {

                                dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                                dynamicAuction += "<div class=\"price_live_auc price_current_next divider_right\">";
                                dynamicAuction += "<h5>Bought In</h5>";
                                dynamicAuction += "</div><!-- price_live_auc -->";
                                dynamicAuction += "</div><!-- col -->";
                            }
                            else
                            {
                                //show pricers with 15% margin added after Bid Closed     

                                long pricers = item.pricers;    //use long for large pricers calculation
                                long margin = pricers * 15;
                                margin = margin / 100;
                                long margin_pricers = pricers + margin;

                                dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                                dynamicAuction += "<div class=\"price_live_auc price_current_next divider_right\">";
                                dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(margin_pricers.ToString()) + "</h5>";
                                dynamicAuction += "<h6 class=\"\">(Inclusive 15% margin)</h6>";
                                dynamicAuction += "</div><!-- price_live_auc -->";
                                dynamicAuction += "</div><!-- col -->";

                            }

                        }
                        else
                        {
                            dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                            dynamicAuction += "<div class=\"price_live_auc price_current_next divider_right\">";
                            dynamicAuction += "<h6 class=\"uppercase\">current bid</h6>";
                            dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(@item.pricers.ToString()) + "</h5>";
                            dynamicAuction += "</div><!-- price_live_auc -->";
                            dynamicAuction += "</div><!-- col -->";
                        }



                        if (item.timeRemains < 0)
                        {

                            if (userid == item.MyUserID && item.MyUserID != 0)
                            {
                                if (item.pricelow < item.pricers)
                                {
                                    dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                                    dynamicAuction += "<div class=\"price_live_auc price_current_next\">";
                                    dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid current_leading\">WON</h6>";
                                    dynamicAuction += "</div><!-- price_live_auc -->";
                                    dynamicAuction += "</div><!-- col -->";
                                }
                                else
                                {
                                    dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                                }
                            }
                            else
                            {
                                dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                            }
                        }
                        else
                        {
                            dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                            dynamicAuction += "<div class=\"price_live_auc price_current_next\">";
                            if (userid == item.MyUserID && item.MyUserID != 0)
                            {
                                dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid current_leading\">Currently <br> leading</h6>";
                            }
                            else
                            {
                                dynamicAuction += "<h6 class=\"uppercase\">next valid bid</h6>";
                                dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(Astaguru.Services.Common.getNextValidBidAmount(@item.pricers, currency).ToString()) + "</h5>";

                            }
                            dynamicAuction += "</div><!-- price_live_auc -->";
                            dynamicAuction += "</div><!-- col -->";

                        }
                    }

                    dynamicAuction += " </div><!-- row --></div><!-- col -->";


                    if (item.timeRemains < 0)
                    {

                    }
                    else
                    {
                        if (userid == item.MyUserID)
                        {

                        }
                        else
                        {
                            dynamicAuctionButton += "<a href=\"JavaScript:Void(0);\" class=\"btn_bid btnBidNow\" data-id=\"" + item.productid + "\">Bid</a>";
                        }

                        dynamicAuctionButton += "<div class=\"timer_wrap\">";
                        dynamicAuctionButton += "<div class=\"timer_count\">";
                        dynamicAuctionButton += "<figure>";
                        dynamicAuctionButton += "<ul>";
                        dynamicAuctionButton += "<li>" + item.hh + "</li>";
                        dynamicAuctionButton += " </ul>";
                        dynamicAuctionButton += " <p>Hours</p>";
                        dynamicAuctionButton += "</figure>";
                        dynamicAuctionButton += "<span>:</span>";
                        dynamicAuctionButton += "<figure>";
                        dynamicAuctionButton += "<ul>";
                        dynamicAuctionButton += "<li>" + item.mm + "</li>";
                        dynamicAuctionButton += "</ul>";
                        dynamicAuctionButton += "<p>Minute</p>";
                        dynamicAuctionButton += "</figure>";
                        dynamicAuctionButton += "<span>:</span>";
                        dynamicAuctionButton += "<figure>";
                        dynamicAuctionButton += "<ul>";
                        dynamicAuctionButton += "<li>" + item.ss + "</li>";
                        dynamicAuctionButton += " </ul>";
                        dynamicAuctionButton += " <p>Second</p>";
                        dynamicAuctionButton += "</figure>";
                        dynamicAuctionButton += "</div>";
                        dynamicAuctionButton += "</div>";

                    }

                    dynamicAuctionButton += " <div class=\"clear\"></div>";


                    // Add Data in List 
                    dynamicData DD = new dynamicData();
                    DD.productid = item.productid;
                    DD.dyanmicData = dynamicAuction;
                    listDynamicAuction.Add(DD);

                    dynamicData DD1 = new dynamicData();
                    DD1.productid = item.productid;
                    DD1.dyanmicData = dynamicAuctionButton;
                    listDynamicAuctionButton.Add(DD1);

                }
            }
            return Json(new { Success = listDynamicAuction, Button = listDynamicAuctionButton }, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public ActionResult generateMyGallary(Auction Auc)
        {

            List<dynamicData> listDynamicAuction = new List<dynamicData>();

            Auc.Subquery = "";
            int userid = Convert.ToInt32(Session["userid"]);
            int currency = Convert.ToInt32(Session["GetCurrency"]);

            string baseUrlPath = Astaguru.Services.Common.baseUrlPath;

            List<Astaguru.Models.Auction> MyAuctionGallary = null;
            //MyAuctionGallary = MAS.GetMyAuctionGallaryList(Auc, Auc.page);

            //created on 13_02_2020
            MyAuctionGallary = MAS.GetMyAuctionGallaryList(Auc);

            if (MyAuctionGallary.Count > 0)
            {
                foreach (var item in MyAuctionGallary)
                {
                    string dynamicAuction = string.Empty;


                    if (item.status == "Current")
                    {
                        int usdCurrentPrice = item.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
                        int NextValidBid = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(@item.pricers, 1));
                        int usdNextValidPrice = NextValidBid / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);


                        dynamicAuction += "<div class=\"row\">";
                        if (currency == 1)
                        {
                            if (item.timeRemains < 0)
                            {
                                if (item.pricelow > item.pricers)
                                {
                                    dynamicAuction += "<div class=\"col-xs-6 col-sm-6 divider_right\">";
                                    dynamicAuction += "<div class=\"price_live_auc\">";
                                    dynamicAuction += "<h5>Bought In</h5>";
                                    dynamicAuction += "</div>";
                                    dynamicAuction += "</div><!-- col -->";
                                }
                                else
                                {
                                    //show pricers with 15% margin added after Bid Closed  
                                                                                      
                                    long pricers = item.pricers;    //use long for large pricers calculation
                                    long margin = pricers * 15;
                                    margin = margin / 100;
                                    long margin_pricers = pricers + margin;

                                    dynamicAuction += "<div class=\"col-xs-6 col-sm-6 divider_right\">";
                                    dynamicAuction += "<div class=\"price_live_auc\">";
                                    dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(margin_pricers.ToString()) + "</h5>";
                                    dynamicAuction += "<h6 style=\"font-size:10px\" > (Inclusive 15% margin)</h6>";
                                    dynamicAuction += "</div>";
                                    dynamicAuction += "</div><!-- col -->";
                                }

                            }
                            else
                            {
                                dynamicAuction += "<div class=\"col-xs-6 col-sm-6 divider_right\">";
                                dynamicAuction += "<div class=\"price_live_auc\">";
                                dynamicAuction += "<h6 class=\"uppercase\">current bid</h6>";
                                dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(@item.pricers.ToString()) + "</h5>";
                                dynamicAuction += "</div>";
                                dynamicAuction += "</div><!-- col -->";
                            }


                            //if (item.timeRemains < 0)
                            //{
                            //    if (userid == item.MyUserID)
                            //    {
                            //        dynamicAuction += "<div class=\"col-xs-6 col-sm-6\">";
                            //        dynamicAuction += "<div class=\"price_live_auc\">";
                            //        dynamicAuction += " <h6 class=\"uppercase text_next_valid_bid current_leading\">WON</h6>";
                            //        dynamicAuction += "</div>";
                            //        dynamicAuction += "</div><!-- col -->";
                            //    }
                            //    else
                            //    {
                            //        dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                            //    }


                            //}

                            if (item.timeRemains < 0)
                            {
                               
                                if (userid == item.MyUserID && item.MyUserID != 0)
                                {
                                    if (item.pricelow < item.pricers)
                                    {
                                        dynamicAuction += "<div class=\"col-xs-6 col-sm-6\">";
                                        dynamicAuction += "<div class=\"price_live_auc\">";
                                        dynamicAuction += " <h6 class=\"uppercase text_next_valid_bid current_leading\">WON</h6>";
                                        dynamicAuction += "</div>";
                                        dynamicAuction += "</div><!-- col -->";
                                    }
                                    else
                                    {
                                        dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                                    }
                                }
                                else
                                {
                                    dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                                }


                            }
                            else
                            {
                                dynamicAuction += "<div class=\"col-xs-6 col-sm-6\">";
                                dynamicAuction += "<div class=\"price_live_auc\">";
                                if (userid == item.MyUserID)
                                {
                                    dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid current_leading\">Currently leading</h6>";
                                }
                                else
                                {
                                    dynamicAuction += "<h6 class=\"uppercase\">next valid bid</h6>";
                                    dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(Astaguru.Services.Common.getNextValidBidAmount(@item.pricers, currency).ToString()) + "</h5>";
                                }
                                dynamicAuction += "</div>";
                                dynamicAuction += " </div><!-- col -->";
                            }

                        }
                        else if (currency == 2)
                        {
                            if (item.timeRemains < 0)
                            {
                                if (item.pricelow > item.pricers)
                                {
                                    dynamicAuction += "<div class=\"col-xs-6 col-sm-6 divider_right\">";
                                    dynamicAuction += "<div class=\"price_live_auc\">";
                                    dynamicAuction += "<h5>Bought In</h5>";
                                    dynamicAuction += "</div>";
                                    dynamicAuction += "</div><!-- col -->";
                                }
                                else
                                {
                                    //show price with 15% margin added after Bid Closed
                                    int margin = usdCurrentPrice * 15;
                                    margin = margin / 100;
                                    int margin_priceus = usdCurrentPrice + margin;

                                    dynamicAuction += "<div class=\"col-xs-6 col-sm-6 divider_right\">";
                                    dynamicAuction += "<div class=\"price_live_auc\">";
                                    dynamicAuction += "<h5>$ " + Astaguru.Services.Common.rupeeFormat(margin_priceus.ToString()) + "</h5>";
                                    dynamicAuction += "<h6 style=\"font-size:10px\" > (Inclusive 15% margin)</h6>";
                                    dynamicAuction += "</div>";
                                    dynamicAuction += "</div><!-- col -->";
                                }

                            }
                            else
                            {
                                dynamicAuction += "<div class=\"col-xs-6 col-sm-6 divider_right\">";
                                dynamicAuction += "<div class=\"price_live_auc\">";
                                dynamicAuction += "<h6 class=\"uppercase\">current bid</h6>";
                                dynamicAuction += "<h5>$ " + Astaguru.Services.Common.DollerFormat(usdCurrentPrice.ToString()) + "</h5>";
                                dynamicAuction += "</div>";
                                dynamicAuction += "</div><!-- col -->";
                            }


                            if (item.timeRemains < 0)
                            {

                                if (userid == item.MyUserID && item.MyUserID != 0)
                                {
                                    if (item.pricelow < item.pricers)
                                    {
                                        dynamicAuction += "<div class=\"col-xs-6 col-sm-6\">";
                                        dynamicAuction += "<div class=\"price_live_auc\">";
                                        dynamicAuction += " <h6 class=\"uppercase text_next_valid_bid current_leading\">WON</h6>";
                                        dynamicAuction += "</div>";
                                        dynamicAuction += "</div><!-- col -->";
                                    }
                                    else
                                    {
                                        dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                                    }
                                }
                                else
                                {
                                    dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                                }


                            }
                            else
                            {
                                dynamicAuction += "<div class=\"col-xs-6 col-sm-6\">";
                                dynamicAuction += "<div class=\"price_live_auc\">";


                                if (userid == item.MyUserID)
                                {
                                    dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid current_leading\">Currently leading</h6>";
                                }
                                else
                                {
                                    dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid\">next valid bid</h6>";
                                    dynamicAuction += "<h5>$ " + Astaguru.Services.Common.DollerFormat(usdNextValidPrice.ToString()) + " </h5>";
                                }
                                dynamicAuction += "</div>";
                                dynamicAuction += "</div><!-- col -->";
                            }
                        }
                        else
                        {
                            if (item.timeRemains < 0)
                            {
                                if (item.pricelow > item.pricers)
                                {
                                    dynamicAuction += "<div class=\"col-xs-6 col-sm-6 divider_right\">";
                                    dynamicAuction += "<div class=\"price_live_auc\">";
                                    dynamicAuction += "<h5>Bought In</h5>";
                                    dynamicAuction += "</div>";
                                    dynamicAuction += "</div><!-- col -->";
                                }
                                else
                                {
                                    //show pricers with 15% margin added after Bid Closed  

                                    long pricers = item.pricers;    //use long for large pricers calculation
                                    long margin = pricers * 15;
                                    margin = margin / 100;
                                    long margin_pricers = pricers + margin;

                                    dynamicAuction += "<div class=\"col-xs-6 col-sm-6 divider_right\">";
                                    dynamicAuction += "<div class=\"price_live_auc\">";
                                    dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(margin_pricers.ToString()) + "</h5>";
                                    dynamicAuction += "<h6 style=\"font-size:10px\" > (Inclusive 15% margin)</h6>";
                                    dynamicAuction += "</div>";
                                    dynamicAuction += "</div><!-- col -->";
                                }

                            }
                            else
                            {
                                dynamicAuction += "<div class=\"col-xs-6 col-sm-6 divider_right\">";
                                dynamicAuction += "<div class=\"price_live_auc\">";
                                dynamicAuction += "<h6 class=\"uppercase\">current bid</h6>";
                                dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(@item.pricers.ToString()) + "</h5>";
                                dynamicAuction += "</div>";
                                dynamicAuction += "</div><!-- col -->";
                            }

                            if (item.timeRemains < 0)
                            {

                                if (userid == item.MyUserID && item.MyUserID != 0)
                                {
                                    if (item.pricelow < item.pricers)
                                    {
                                        dynamicAuction += "<div class=\"col-xs-6 col-sm-6\">";
                                        dynamicAuction += "<div class=\"price_live_auc\">";
                                        dynamicAuction += " <h6 class=\"uppercase text_next_valid_bid current_leading\">WON</h6>";
                                        dynamicAuction += "</div>";
                                        dynamicAuction += "</div><!-- col -->";
                                    }
                                    else
                                    {
                                        dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                                    }
                                }
                                else
                                {
                                    dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                                }


                            }
                            else
                            {
                                dynamicAuction += "<div class=\"col-xs-6 col-sm-6\">";
                                dynamicAuction += "<div class=\"price_live_auc\">";
                                if (userid == item.MyUserID)
                                {
                                    dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid current_leading\">Currently leading</h6>";
                                }
                                else
                                {
                                    dynamicAuction += "<h6 class=\"uppercase\">next valid bid</h6>";
                                    dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(Astaguru.Services.Common.getNextValidBidAmount(@item.pricers, currency).ToString()) + "</h5>";
                                }
                                dynamicAuction += "</div>";
                                dynamicAuction += " </div><!-- col -->";
                            }
                        }

                        dynamicAuction += "</div><!-- row -->";

                        dynamicAuction += "<div class=\"live_auction_timer\">";
                        dynamicAuction += "<div class=\"timer_wrap\">";




                        if (item.timeRemains < 0)
                        {
                            if (userid == item.MyUserID)
                            {

                            }
                            else
                            {
                                //dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                            }


                        }
                        else
                        {
                            dynamicAuction += "<div class=\"timer_count\">";
                            dynamicAuction += "<figure>";
                            dynamicAuction += "<ul>";
                            dynamicAuction += "<li>" + item.hh + "</li>";
                            dynamicAuction += " </ul>";
                            dynamicAuction += " <p>Hours</p>";
                            dynamicAuction += "</figure>";
                            dynamicAuction += "<span>:</span>";
                            dynamicAuction += "<figure>";
                            dynamicAuction += "<ul>";
                            dynamicAuction += "<li>" + item.mm + "</li>";
                            dynamicAuction += "</ul>";
                            dynamicAuction += "<p>Minute</p>";
                            dynamicAuction += "</figure>";
                            dynamicAuction += "<span>:</span>";
                            dynamicAuction += "<figure>";
                            dynamicAuction += "<ul>";
                            dynamicAuction += "<li>" + item.ss + "</li>";
                            dynamicAuction += " </ul>";
                            dynamicAuction += " <p>Second</p>";
                            dynamicAuction += "</figure>";
                            dynamicAuction += "</div>";

                        }
                        dynamicAuction += "</div>";

                        if (item.timeRemains < 0)
                        { }
                        else
                        {
                            if (userid == item.MyUserID)
                            {

                            }
                            else
                            {
                                dynamicAuction += "<a href=\"JavaScript:Void(0);\" class=\"btn_bid btnBidNow\" data-id=\"" + item.productid + "\">Bid</a>";
                            }

                        }
                        dynamicAuction += "</div><!-- live_auction_timer -->";
                        dynamicAuction += "<div class=\"clear\"></div>";

                        // Add Data in List 
                        dynamicData DD = new dynamicData();
                        DD.productid = item.productid;
                        DD.dyanmicData = dynamicAuction;
                        listDynamicAuction.Add(DD);

                    }


                }
            }
            return Json(new { Success = listDynamicAuction }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult generateMyGallaryList(Auction Auc)
        {

            List<dynamicData> listDynamicAuction = new List<dynamicData>();
            List<dynamicData> listDynamicAuctionButton = new List<dynamicData>();

            Auc.Subquery = "";
            int userid = Convert.ToInt32(Session["userid"]);
            int currency = Convert.ToInt32(Session["GetCurrency"]);

            string baseUrlPath = Astaguru.Services.Common.baseUrlPath;

            List<Astaguru.Models.Auction> MyAuctionGallary = null;
            //MyAuctionGallary = MAS.GetMyAuctionGallaryList(Auc, Auc.page);
            //created on 13_02_2020
            MyAuctionGallary = MAS.GetMyAuctionGallaryList(Auc);

            if (MyAuctionGallary.Count > 0)
            {
                foreach (var item in MyAuctionGallary)
                {
                    string dynamicAuction = string.Empty;
                    string dynamicAuctionButton = string.Empty;

                    if (item.status == "Current")
                    {
                        int usdCurrentPrice = item.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
                        int NextValidBid = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(@item.pricers, 1));
                        int usdNextValidPrice = NextValidBid / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);


                        if (currency == 1)
                        {
                            if (item.timeRemains < 0)
                            {
                                if (item.pricelow > item.pricers)
                                {

                                    dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                                    dynamicAuction += "<div class=\"price_live_auc price_current_next divider_right\">";
                                    dynamicAuction += "<h5>Bought In</h5>";
                                    dynamicAuction += "</div><!-- price_live_auc -->";
                                    dynamicAuction += "</div><!-- col -->";
                                }
                                else
                                {
                                    //show pricers with 15% margin added after Bid Closed  

                                    long pricers = item.pricers;    //use long for large pricers calculation
                                    long margin = pricers * 15;
                                    margin = margin / 100;
                                    long margin_pricers = pricers + margin;

                                    dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                                    dynamicAuction += "<div class=\"price_live_auc price_current_next divider_right\">";                                 
                                    dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(margin_pricers.ToString()) + "</h5>";
                                    dynamicAuction += "<h6 class=\"\">(Inclusive 15% margin)</h6>";
                                    dynamicAuction += "</div><!-- price_live_auc -->";
                                    dynamicAuction += "</div><!-- col -->";

                                }

                            }
                            else
                            {
                                dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                                dynamicAuction += "<div class=\"price_live_auc price_current_next divider_right\">";
                                dynamicAuction += "<h6 class=\"uppercase\">current bid</h6>";
                                dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(@item.pricers.ToString()) + "</h5>";
                                dynamicAuction += "</div><!-- price_live_auc -->";
                                dynamicAuction += "</div><!-- col -->";
                            }



                            //if (item.timeRemains < 0)
                            //{
                            //    if (userid == item.MyUserID && item.MyUserID != 0)
                            //    {
                            //        dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                            //        dynamicAuction += "<div class=\"price_live_auc price_current_next\">";
                            //        dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid current_leading\">WON</h6>";
                            //        dynamicAuction += "</div><!-- price_live_auc -->";
                            //        dynamicAuction += "</div><!-- col -->";
                            //    }
                            //    else
                            //    {
                            //        dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                            //    }

                            //}
                            if (item.timeRemains < 0)
                            {

                                if (userid == item.MyUserID && item.MyUserID != 0)
                                {
                                    if (item.pricelow < item.pricers)
                                    {
                                        dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                                        dynamicAuction += "<div class=\"price_live_auc price_current_next\">";
                                        dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid current_leading\">WON</h6>";
                                        dynamicAuction += "</div><!-- price_live_auc -->";
                                        dynamicAuction += "</div><!-- col -->";
                                    }
                                    else
                                    {
                                        dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                                    }
                                }
                                else
                                {
                                    dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                                }
                            }
                            else
                            {
                                dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                                dynamicAuction += "<div class=\"price_live_auc price_current_next\">";
                                if (userid == item.MyUserID && item.MyUserID != 0)
                                {
                                    dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid current_leading\">Currently <br> leading</h6>";
                                }
                                else
                                {
                                    dynamicAuction += "<h6 class=\"uppercase\">next valid bid</h6>";
                                    dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(Astaguru.Services.Common.getNextValidBidAmount(@item.pricers, currency).ToString()) + "</h5>";

                                }
                                dynamicAuction += "</div><!-- price_live_auc -->";
                                dynamicAuction += "</div><!-- col -->";

                            }


                        }
                        else if (currency == 2)
                        {
                            if (item.timeRemains < 0)
                            {
                                if (item.pricelow > item.pricers)
                                {
                                    dynamicAuction += "<div class=\"col-xs-6 col-sm-6 divider_right\">";
                                    dynamicAuction += "<div class=\"price_live_auc\">";
                                    dynamicAuction += "<h5>Bought In</h5>";
                                    dynamicAuction += "</div>";
                                    dynamicAuction += "</div><!-- col -->";
                                }
                                else
                                {
                                    //show price with 15% margin added after Bid Closed
                                    int margin = usdCurrentPrice * 15;
                                    margin = margin / 100;
                                    int margin_priceus = usdCurrentPrice + margin;

                                    dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                                    dynamicAuction += "<div class=\"price_live_auc price_current_next divider_right\">";
                                    dynamicAuction += "<h5>$ " + Astaguru.Services.Common.DollerFormat(margin_priceus.ToString()) + "</h5>";
                                    dynamicAuction += "<h6 class=\"\">(Inclusive 15% margin)</h6>";
                                    dynamicAuction += "</div><!-- price_live_auc -->";
                                    dynamicAuction += "</div><!-- col -->";

                                }

                            }
                            else
                            {
                                dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                                dynamicAuction += "<div class=\"price_live_auc price_current_next divider_right\">";
                                dynamicAuction += "<h6 class=\"uppercase\">current bid</h6>";
                                dynamicAuction += "<h5>$ " + Astaguru.Services.Common.DollerFormat(@item.priceus.ToString()) + "</h5>";
                                dynamicAuction += "</div><!-- price_live_auc -->";
                                dynamicAuction += "</div><!-- col -->";
                            }


                            if (item.timeRemains < 0)
                            {

                                if (userid == item.MyUserID && item.MyUserID != 0)
                                {
                                    if (item.pricelow < item.pricers)
                                    {
                                        dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                                        dynamicAuction += "<div class=\"price_live_auc price_current_next\">";
                                        dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid current_leading\">WON</h6>";
                                        dynamicAuction += "</div><!-- price_live_auc -->";
                                        dynamicAuction += "</div><!-- col -->";
                                    }
                                    else
                                    {
                                        dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                                    }
                                }
                                else
                                {
                                    dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                                }
                            }
                            else
                            {
                                dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                                dynamicAuction += "<div class=\"price_live_auc price_current_next\">";

                                if (userid == item.MyUserID && item.MyUserID != 0)
                                {
                                    dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid current_leading\">Currently <br> leading</h6>";

                                }
                                else
                                {
                                    dynamicAuction += "<h6 class=\"uppercase\">next valid bid</h6>";
                                    dynamicAuction += "<h5>$ " + Astaguru.Services.Common.DollerFormat(Astaguru.Services.Common.getNextValidBidAmount(@item.priceus, currency).ToString()) + "</h5>";


                                }
                                dynamicAuction += "</div><!-- price_live_auc -->";
                                dynamicAuction += "</div><!-- col -->";

                            }

                        }
                        else
                        {
                            if (item.timeRemains < 0)
                            {
                                if (item.pricelow > item.pricers)
                                {

                                    dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                                    dynamicAuction += "<div class=\"price_live_auc price_current_next divider_right\">";
                                    dynamicAuction += "<h5>Bought In</h5>";
                                    dynamicAuction += "</div><!-- price_live_auc -->";
                                    dynamicAuction += "</div><!-- col -->";
                                }
                                else
                                {
                                    //show pricers with 15% margin added after Bid Closed  

                                    long pricers = item.pricers;    //use long for large pricers calculation
                                    long margin = pricers * 15;
                                    margin = margin / 100;
                                    long margin_pricers = pricers + margin;

                                    dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                                    dynamicAuction += "<div class=\"price_live_auc price_current_next divider_right\">";
                                    dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(margin_pricers.ToString()) + "</h5>";
                                    dynamicAuction += "<h6 class=\"\">(Inclusive 15% margin)</h6>";
                                    dynamicAuction += "</div><!-- price_live_auc -->";
                                    dynamicAuction += "</div><!-- col -->";

                                }

                            }
                            else
                            {
                                dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                                dynamicAuction += "<div class=\"price_live_auc price_current_next divider_right\">";
                                dynamicAuction += "<h6 class=\"uppercase\">current bid</h6>";
                                dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(@item.pricers.ToString()) + "</h5>";
                                dynamicAuction += "</div><!-- price_live_auc -->";
                                dynamicAuction += "</div><!-- col -->";
                            }

                            if (item.timeRemains < 0)
                            {

                                if (userid == item.MyUserID && item.MyUserID != 0)
                                {
                                    if (item.pricelow < item.pricers)
                                    {
                                        dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                                        dynamicAuction += "<div class=\"price_live_auc price_current_next\">";
                                        dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid current_leading\">WON</h6>";
                                        dynamicAuction += "</div><!-- price_live_auc -->";
                                        dynamicAuction += "</div><!-- col -->";
                                    }
                                    else
                                    {
                                        dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                                    }
                                }
                                else
                                {
                                    dynamicAuction += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                                }
                            }
                            else
                            {
                                dynamicAuction += "<div class=\"col-xs-6 col-sm-12 col-md-12 col-xl-6\">";
                                dynamicAuction += "<div class=\"price_live_auc price_current_next\">";
                                if (userid == item.MyUserID && item.MyUserID != 0)
                                {
                                    dynamicAuction += "<h6 class=\"uppercase text_next_valid_bid current_leading\">Currently <br> leading</h6>";
                                }
                                else
                                {
                                    dynamicAuction += "<h6 class=\"uppercase\">next valid bid</h6>";
                                    dynamicAuction += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(Astaguru.Services.Common.getNextValidBidAmount(@item.pricers, currency).ToString()) + "</h5>";

                                }
                                dynamicAuction += "</div><!-- price_live_auc -->";
                                dynamicAuction += "</div><!-- col -->";

                            }
                        }

                        dynamicAuction += " </div><!-- row --></div><!-- col -->";


                        if (item.timeRemains < 0)
                        {

                        }
                        else
                        {
                            if (userid == item.MyUserID)
                            {

                            }
                            else
                            {
                                dynamicAuctionButton += "<a href=\"JavaScript:Void(0);\" class=\"btn_bid btnBidNow\" data-id=\"" + item.productid + "\">Bid</a>";
                            }

                            dynamicAuctionButton += "<div class=\"timer_wrap\">";
                            dynamicAuctionButton += "<div class=\"timer_count\">";
                            dynamicAuctionButton += "<figure>";
                            dynamicAuctionButton += "<ul>";
                            dynamicAuctionButton += "<li>" + item.hh + "</li>";
                            dynamicAuctionButton += " </ul>";
                            dynamicAuctionButton += " <p>Hours</p>";
                            dynamicAuctionButton += "</figure>";
                            dynamicAuctionButton += "<span>:</span>";
                            dynamicAuctionButton += "<figure>";
                            dynamicAuctionButton += "<ul>";
                            dynamicAuctionButton += "<li>" + item.mm + "</li>";
                            dynamicAuctionButton += "</ul>";
                            dynamicAuctionButton += "<p>Minute</p>";
                            dynamicAuctionButton += "</figure>";
                            dynamicAuctionButton += "<span>:</span>";
                            dynamicAuctionButton += "<figure>";
                            dynamicAuctionButton += "<ul>";
                            dynamicAuctionButton += "<li>" + item.ss + "</li>";
                            dynamicAuctionButton += " </ul>";
                            dynamicAuctionButton += " <p>Second</p>";
                            dynamicAuctionButton += "</figure>";
                            dynamicAuctionButton += "</div>";
                            dynamicAuctionButton += "</div>";

                        }

                        dynamicAuctionButton += " <div class=\"clear\"></div>";



                        // Add Data in List 
                        dynamicData DD = new dynamicData();
                        DD.productid = item.productid;
                        DD.dyanmicData = dynamicAuction;
                        listDynamicAuction.Add(DD);

                        dynamicData DD1 = new dynamicData();
                        DD1.productid = item.productid;
                        DD1.dyanmicData = dynamicAuctionButton;
                        listDynamicAuctionButton.Add(DD1);

                    }



                }
            }
            return Json(new { Success = listDynamicAuction, Button = listDynamicAuctionButton }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult generateCurrentDetail(Auction Auc)
        {
            int k = 0;
            int LastBidId = 0;
            string dynamicAuction = string.Empty;
            string dynamicCurrency = string.Empty;
            string dynamicBidHistory = string.Empty;
            string dynamicButtons = string.Empty;
            string dynamicPrice = string.Empty;
            string dynamicNextFiveBid = string.Empty;
            string dynamicoutbid = string.Empty;              //created on 13_02_2020


            // Dynamic List For All Type Of List
            Auction CurrentAuction = new Auction();
            int auctionstyle = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["auctionstyle"]);
            int auctionId = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["auctionId"]);
            List<dynamicData> listDynamicAuction = new List<dynamicData>();

            Auc.Subquery = "";
            int userid = Convert.ToInt32(Session["userid"]);
            int currency = Convert.ToInt32(Session["GetCurrency"]);

            string baseUrlPath = Astaguru.Services.Common.baseUrlPath;

            if (Auc.pageName == "Detail")
            {
                int productid = Auc.productid;
                CurrentAuction = CAS.GetCurrentAuctionDetail(productid);
            }

            int usdCurrentPrice = CurrentAuction.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
            int NextValidBid = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(CurrentAuction.pricers, 1));
            int usdNextValidPrice = NextValidBid / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
            double topFiveBidRs = Astaguru.Services.Common.getNextValidBidAmount(CurrentAuction.pricers, currency);
            double topFiveBidUs = usdNextValidPrice; // CurrentAuction.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
            if (CurrentAuction != null)
            {

                // Timer
                //  dynamicAuction += "<div class=\"timer_wrap\">";
                //  dynamicAuction += "<div class=\"counter_timer\"></div>";

                //    if (CurrentAuction.timeRemains < 0)
                //    {
                //        dynamicAuction += "<div><span style=\"color:red\" class=\"close_auction\">Bid Closed</span></div>";
                //    }
                //else { 
                //          dynamicAuction += "<span class=\"product_left\">left </span>";
                //          dynamicAuction += "<span>" + CurrentAuction.myBidClosingTime + " </span>";
                //    }

                //    dynamicAuction += "</div>";


                if (CurrentAuction.timeRemains < 0)
                {
                    dynamicAuction += " ";
                }
                else
                {
                    dynamicAuction += "<div class=\"timer_count extra-margin-top-30\">";
                    dynamicAuction += "<figure>";
                    dynamicAuction += "<ul>";
                    dynamicAuction += "<li>" + CurrentAuction.hh + "</li>";
                    dynamicAuction += " </ul>";
                    dynamicAuction += " <p>Hours</p>";
                    dynamicAuction += "</figure>";
                    dynamicAuction += "<span>:</span>";
                    dynamicAuction += "<figure>";
                    dynamicAuction += "<ul>";
                    dynamicAuction += "<li>" + CurrentAuction.mm + "</li>";
                    dynamicAuction += "</ul>";
                    dynamicAuction += "<p>Minute</p>";
                    dynamicAuction += "</figure>";
                    dynamicAuction += "<span>:</span>";
                    dynamicAuction += "<figure>";
                    dynamicAuction += "<ul>";
                    dynamicAuction += "<li>" + CurrentAuction.ss + "</li>";
                    dynamicAuction += " </ul>";
                    dynamicAuction += " <p>Second</p>";
                    dynamicAuction += "</figure>";
                    dynamicAuction += "</div>";
                }



                // Currency
                if (currency == 1)
                {
                    dynamicCurrency += "<div class=\"row custom_row details_text mobile_view_2 extra-margin-top-30\" style=\"max-width:460px;\">";

                    if (CurrentAuction.timeRemains < 0)
                    {
                        if (CurrentAuction.pricelow > CurrentAuction.pricers)
                        {
                            dynamicCurrency += "<div class=\"col-sm-6 col-lg-6 custom_col divider_right\">";
                            dynamicCurrency += "<h3 class=\"sbold\">Bought In</h3>";
                            dynamicCurrency += "</div><!-- col -->";
                        }
                        else
                        {

                            //show pricers with 15% margin added after Bid Closed

                            long pricers = CurrentAuction.pricers;    //use long for large pricers calculation
                            long margin = pricers * 15;
                            margin = margin / 100;
                            long margin_pricers = pricers + margin;

                            dynamicCurrency += "<div class=\"col-sm-6 col-lg-6 custom_col divider_right\">";                           
                            dynamicCurrency += "<h3 class=\"sbold\">₹ " + Astaguru.Services.Common.rupeeFormat(margin_pricers.ToString()) + "</h3>";
                            dynamicCurrency += "<p class=\"\" style=\"font-size:14px\">(Inclusive 15% margin)</p>";
                            dynamicCurrency += "</div><!-- col -->";

                        }

                    }
                    else
                    {
                        dynamicCurrency += "<div class=\"col-sm-6 col-lg-6 custom_col divider_right\">";
                        dynamicCurrency += "<p class=\"uppercase\">current bid</p>";
                        dynamicCurrency += "<h3 class=\"sbold\">₹ " + Astaguru.Services.Common.rupeeFormat(CurrentAuction.pricers.ToString()) + "</h3>";
                        dynamicCurrency += "</div><!-- col -->";
                    }



                    if (CurrentAuction.timeRemains < 0)
                    {

                        if (CurrentAuction.pricelow < CurrentAuction.pricers)
                        {
                            if (userid == CurrentAuction.MyUserID && CurrentAuction.MyUserID != 0)
                            {
                                dynamicCurrency += "<div class=\"col-sm-6 col-lg-6 custom_col\">";
                                dynamicCurrency += "<p class=\"uppercase text_next_valid_bid current_leading\">Won</p>";
                                dynamicCurrency += "</div><!-- col -->";

                            }
                            else
                            {
                                dynamicCurrency += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                            }
                        }
                        else
                        {
                            dynamicCurrency += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                        }

                    }
                    else
                    {
                        dynamicCurrency += "<div class=\"col-sm-6 col-lg-6 custom_col\">";
                        if (userid == CurrentAuction.MyUserID && CurrentAuction.MyUserID != 0)
                        {
                            dynamicCurrency += "<p class=\"uppercase text_next_valid_bid current_leading\">Currently leading</p>";
                        }
                        else
                        {
                            dynamicCurrency += "<p class=\"uppercase\">next valid bid</p>";
                            dynamicCurrency += "<h3 class=\"sbold\">₹ " + Astaguru.Services.Common.rupeeFormat(Astaguru.Services.Common.getNextValidBidAmount(CurrentAuction.pricers, currency).ToString()) + "</h3>";
                            dynamicCurrency += "<input type='hidden' id = 'checknextvalidbid' value = " + CurrentAuction.pricers + ">";
                        }

                        dynamicCurrency += " </div><!-- col -->";
                    }


                    dynamicCurrency += "</div><!-- row -->";


                }
                else if (currency == 2)
                {
                    dynamicCurrency += "<div class=\"row custom_row details_text mobile_view_2 extra-margin-top-30\" style=\"max-width:460px;\">";

                    if (CurrentAuction.timeRemains < 0)
                    {
                        if (CurrentAuction.pricelow > CurrentAuction.pricers)
                        {
                            dynamicCurrency += "<div class=\"col-sm-6 col-lg-6 custom_col divider_right\">";
                            dynamicCurrency += "<h3 class=\"sbold\">Bought In</h3>";
                            dynamicCurrency += "</div><!-- col -->";
                        }
                        else
                        {
                            //show price with 15% margin added after Bid Closed
                            int margin = usdCurrentPrice * 15;
                            margin = margin / 100;
                            int margin_priceus = usdCurrentPrice + margin;

                            dynamicCurrency += "<div class=\"col-sm-6 col-lg-6 custom_col divider_right\">";
                            dynamicCurrency += "<h3 class=\"sbold\">$ " + Astaguru.Services.Common.DollerFormat(margin_priceus.ToString()) + "</h3>";
                            dynamicCurrency += "<p class=\"\" style=\"font-size:14px\">(Inclusive 15% margin)</p>";
                            dynamicCurrency += "</div><!-- col -->";

                        }

                    }
                    else
                    {
                        dynamicCurrency += "<div class=\"col-sm-6 col-lg-6 custom_col divider_right\">";
                        dynamicCurrency += "<p class=\"uppercase\">current bid</p>";
                        dynamicCurrency += " <h3 class=\"sbold\">$ " + Astaguru.Services.Common.DollerFormat(usdCurrentPrice.ToString()) + "</h3>";
                        dynamicCurrency += " </div><!-- col -->";
                    }

                    if (CurrentAuction.timeRemains < 0)
                    {
                        //if (userid == CurrentAuction.MyUserID && CurrentAuction.MyUserID != 0)
                        //{
                        //    dynamicCurrency += "<div class=\"col-sm-6 col-lg-6 custom_col\">";
                        //    dynamicCurrency += "<p class=\"uppercase text_next_valid_bid current_leading\">Won</p>";
                        //    dynamicCurrency += "</div><!-- col -->";

                        //}
                        //else
                        //{
                        //    dynamicCurrency += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                        //}

                        if (CurrentAuction.pricelow < CurrentAuction.pricers)
                        {
                            if (userid == CurrentAuction.MyUserID && CurrentAuction.MyUserID != 0)
                            {
                                dynamicCurrency += "<div class=\"col-sm-6 col-lg-6 custom_col\">";
                                dynamicCurrency += "<p class=\"uppercase text_next_valid_bid current_leading\">Won</p>";
                                dynamicCurrency += "</div><!-- col -->";

                            }
                            else
                            {
                                dynamicCurrency += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                            }
                        }
                        else
                        {
                            dynamicCurrency += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                        }
                    }
                    else
                    {
                        dynamicCurrency += "<div class=\"col-sm-6 col-lg-6 custom_col\">";
                        if (userid == CurrentAuction.MyUserID && CurrentAuction.MyUserID != 0)
                        {
                            dynamicCurrency += "<p class=\"uppercase text_next_valid_bid current_leading\">Currently leading</p>";
                        }
                        else
                        {
                            dynamicCurrency += "<p class=\"uppercase text_next_valid_bid\">next valid bid</p>";
                            dynamicCurrency += "<h3 class=\"sbold\">$ " + Astaguru.Services.Common.DollerFormat(usdNextValidPrice.ToString()) + "</h3>";
                            dynamicCurrency += "<input type='hidden' id = 'checknextvalidbid' value = " + CurrentAuction.pricers + ">";
                        }

                        dynamicCurrency += "</div><!-- col -->";
                    }
                    dynamicCurrency += "</div><!-- row -->";

                }
                else
                {
                    dynamicCurrency += "<div class=\"row custom_row details_text mobile_view_2 extra-margin-top-30\" style=\"max-width:460px;\">";

                    if (CurrentAuction.timeRemains < 0)
                    {
                        if (CurrentAuction.pricelow > CurrentAuction.pricers)
                        {
                            dynamicCurrency += "<div class=\"col-sm-6 col-lg-6 custom_col divider_right\">";
                            dynamicCurrency += "<h3 class=\"sbold\">Bought In</h3>";
                            dynamicCurrency += "</div><!-- col -->";
                        }
                        else
                        {
                            //show pricers with 15% margin added after Bid Closed

                            long pricers = CurrentAuction.pricers;    //use long for large pricers calculation
                            long margin = pricers * 15;
                            margin = margin / 100;
                            long margin_pricers = pricers + margin;

                            dynamicCurrency += "<div class=\"col-sm-6 col-lg-6 custom_col divider_right\">";
                            dynamicCurrency += "<h3 class=\"sbold\">₹ " + Astaguru.Services.Common.rupeeFormat(margin_pricers.ToString()) + "</h3>";
                            dynamicCurrency += "<p class=\"\" style=\"font-size:14px\">(Inclusive 15% margin)</p>";
                            dynamicCurrency += "</div><!-- col -->";

                        }

                    }
                    else
                    {
                        dynamicCurrency += "<div class=\"col-sm-6 col-lg-6 custom_col divider_right\">";
                        dynamicCurrency += "<p class=\"uppercase\">current bid</p>";
                        dynamicCurrency += "<h3 class=\"sbold\">₹ " + Astaguru.Services.Common.rupeeFormat(CurrentAuction.pricers.ToString()) + "</h3>";
                        dynamicCurrency += "</div><!-- col -->";
                    }




                    if (CurrentAuction.timeRemains < 0)
                    {


                        if (CurrentAuction.pricelow < CurrentAuction.pricers)
                        {
                            if (userid == CurrentAuction.MyUserID && CurrentAuction.MyUserID != 0)
                            {
                                dynamicCurrency += "<div class=\"col-sm-6 col-lg-6 custom_col\">";
                                dynamicCurrency += "<p class=\"uppercase text_next_valid_bid current_leading\">Won</p>";
                                dynamicCurrency += "</div><!-- col -->";

                            }
                            else
                            {
                                dynamicCurrency += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                            }
                        }
                        else
                        {
                            dynamicCurrency += "<div class=\"close_auction\"><span>Bid Closed</span></div>";
                        }
                    }
                    else
                    {
                        dynamicCurrency += "<div class=\"col-sm-6 col-lg-6 custom_col\">";
                        if (userid == CurrentAuction.MyUserID && CurrentAuction.MyUserID != 0)
                        {
                            dynamicCurrency += "<p class=\"uppercase text_next_valid_bid current_leading\">Currently leading</p>";
                        }
                        else
                        {
                            dynamicCurrency += "<p class=\"uppercase\">next valid bid</p>";
                            dynamicCurrency += "<h3 class=\"sbold\">₹ " + Astaguru.Services.Common.rupeeFormat(Astaguru.Services.Common.getNextValidBidAmount(CurrentAuction.pricers, currency).ToString()) + "</h3>";
                            dynamicCurrency += "<input type='hidden' id = 'checknextvalidbid' value = " + CurrentAuction.pricers + ">";
                        }

                        dynamicCurrency += " </div><!-- col -->";

                    }

                    dynamicCurrency += "</div><!-- row -->";
                }

                //created on 12_3_2020
                #region
                var currentLeadinguserproxy = CAS.Getcurrentleadinguser(Auc.productid);          // get current leading user for outbid  another
                Auction objActionproxy = CAS.Getcurrentoutbiduser(Auc.productid, currentLeadinguserproxy.userid);    // get out bid user
                if (userid != currentLeadinguserproxy.userid && userid == objActionproxy.userid)
                {
                    int checkforprocyuser = CAS.checkforproxyuserfroutbidmessage(Auc.productid, currentLeadinguserproxy.userid);
                    if (checkforprocyuser > 0)
                    {
                        dynamicoutbid += "<p>Sorry you have been outbid by a higher proxy bid. Would you like to place another bid?</ p>";
                    }
                }
                #endregion

                // Buttons

                if (userid == CurrentAuction.MyUserID)
                {

                }
                else
                {
                    if (CurrentAuction.timeRemains < 0)
                    {
                       
                    }
                    else
                    {
                        dynamicButtons += "<div class=\"row custom_row details_text mobile_view_2\">";
                        dynamicButtons += "<div class=\"col-sm-12 custom_col\">";
                        dynamicButtons += "<a href=\"javascript:avoid(0)\" class=\"btn_lgrey btn_proxybid full_width\" id=\"btnProxyBid\"><span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Proxy Bid\">Proxy Bid</span></a>";

                        if (userid == CurrentAuction.MyUserID)
                        {

                        }
                        else
                        {

                            dynamicButtons += "<a href=\"javascript:avoid(0)\"  title=\"Bid Value\" class=\"btn_lgrey btn_grey btn_bidnow full_width\" id=\"btnBidNow\"><span>bid now</span></a>";
                        }
                        dynamicButtons += "</div><!-- Col -->";
                        dynamicButtons += "</div><!-- row -->";


                    }
                }

                // Next Five Bid


                if (currency == 1)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        topFiveBidRs = Astaguru.Services.Common.getNextValidBidAmount(@topFiveBidRs, currency);
                        dynamicNextFiveBid += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(topFiveBidRs.ToString()) + "</h5>";
                    }
                }
                else if (currency == 2)
                {


                    for (int i = 0; i < 5; i++)
                    {
                        topFiveBidUs = Astaguru.Services.Common.getNextValidBidAmount(@topFiveBidUs, 1);
                        dynamicNextFiveBid += "<h5>$ " + Astaguru.Services.Common.DollerFormat(topFiveBidUs.ToString()) + "</h5>";
                    }

                }
                else
                {
                    for (int i = 0; i < 5; i++)
                    {
                        topFiveBidRs = Astaguru.Services.Common.getNextValidBidAmount(@topFiveBidRs, currency);
                        dynamicNextFiveBid += "<h5>₹ " + Astaguru.Services.Common.rupeeFormat(topFiveBidRs.ToString()) + "</h5>";
                    }
                }






                // Bid History



                dynamicBidHistory += "<div class=\"table-responsive table_box table_bid_history valign_cell extra-margin-top-20\">";
                dynamicBidHistory += "<table class=\"table table-striped\">";
                dynamicBidHistory += "<thead class=\"table_head\">";
                dynamicBidHistory += "<tr>";
                dynamicBidHistory += "<th>Nick Name</th>";
                dynamicBidHistory += "<th>Amount ($)</th>";
                dynamicBidHistory += "<th>Amount (₹)</th>";
                dynamicBidHistory += "<th>Date & Time (UTC)</th>";
                dynamicBidHistory += "</tr>";
                dynamicBidHistory += "</thead>";
                dynamicBidHistory += "<tbody>";
                var usercheck = CAS.Gethighestproxybytime(CurrentAuction.productid);
                int a = 0;
                foreach (var item in CAS.GetBidHistory(CurrentAuction.productid))
                {
                    LastBidId = item.userid;
                    k++;
                    dynamicBidHistory += "<tr>";
                    dynamicBidHistory += "<td>" + item.anoname + "</td>";
                    dynamicBidHistory += "<td>" + Astaguru.Services.Common.DollerFormat(@item.Bidpriceus.ToString()) + "</td>";
                    dynamicBidHistory += "<td>" + Astaguru.Services.Common.rupeeFormat(@item.Bidpricers.ToString()) + "</td>";
                    dynamicBidHistory += "<td>";

                    //if (item.proxy1 == "1")
                    //{
                    //    dynamicBidHistory += "<p>" + item.daterec + " proxy</p>";
                    //}
                    //else
                    //{
                    //    dynamicBidHistory += "<p>" + item.daterec + "</p>";
                    //}

                    //created on  27-1-20
                    #region
                    if (item.proxy1 == "1")
                    {
                        if (a == 0)
                        {
                            if (item.userid == usercheck)
                            {
                                dynamicBidHistory += "<p>" + item.daterec + " proxy <img src='/Content/images/info2.png' data-toggle='tooltip' data-placement='top' title='In case of the same proxy amount,user to place the first bid has the claim on the artwork.'/></p>";
                                //dynamicBidHistory += "<p>" + item.daterec + " proxy <img src='/Content/images/info2.png' data-toggle='tooltip' data-placement='top' title='' data-original-title='In case of the same proxy amount,user to place the first bid has the claim on the artwork.'/></p>";
                                a++;
                            }
                            else
                            {
                                dynamicBidHistory += "<p>" + item.daterec + " proxy</p>";
                            }
                        }
                        else
                        {
                            dynamicBidHistory += "<p>" + item.daterec + " proxy</p>";
                        }
                    }

                    else
                    {
                        dynamicBidHistory += "<p>" + item.daterec + "</p>";
                    }
                    #endregion
                    dynamicBidHistory += "</td>";
                    dynamicBidHistory += "</tr>";
                }
                dynamicBidHistory += "</tbody>";
                dynamicBidHistory += "</table>";
                dynamicBidHistory += "</div>";
                dynamicBidHistory += "<input type=\"hidden\" id=\"hiddenBidCount\" value=\"" + k + "\" />";


            }


            return Json(new { Success = dynamicAuction, Currency = dynamicCurrency, BidHistory = dynamicBidHistory, dynamicButtons = dynamicButtons, BidCount = k, lastuserId = LastBidId, dynamicNextFiveBid = dynamicNextFiveBid, timer = CurrentAuction.timeRemains, dynamicoutbid }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult MeterInfoPartial(int productid)
        {
            Auction AUC = new Auction();
            AUC = CAS.GetCurrentAuctionDetail(productid);
            return PartialView("_partialproxyBid", AUC);
        }

        [HttpPost]
        public ActionResult partialNextFiveBid(int productid)
        {
            Auction AUC = new Auction();
            AUC = CAS.GetCurrentAuctionDetail(productid);
            return PartialView("_partialNextFiveBid", AUC);
        }


        [HttpPost]
        public JsonResult AutoCountry(string Prefix)
        {
            Prefix = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Prefix.ToLower());
            //Product Obj = new Product();
            //Note : you can bind same list from database  
            List<User> ObjList = COM.LoadCountryAuto(); //  new List<Register>()  

            //    //Searching records from list using LINQ query  
            var Country = (from N in ObjList
                           where N.countryname.StartsWith(Prefix)
                           select new { N.countryname, N.countryid });
            if (!Country.Any())
            {
                return Json(new { Status = "Failed" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(Country, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpPost]
        public JsonResult AutoState(string Prefix, int Country_id)
        {
            Prefix = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Prefix.ToLower());
            //Product Obj = new Product();
            //Note : you can bind same list from database  
            List<User> ObjList = COM.LoadStateAuto(Country_id); //  new List<Register>()  

            //    //Searching records from list using LINQ query  
            var State = (from N in ObjList
                         where N.statename.StartsWith(Prefix)
                         select new { N.statename, N.stateid });

            if (!State.Any())
            {
                return Json(new { Status = "Failed" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(State, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult AutoCity(string Prefix, int State_id)
        {
            Prefix = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Prefix.ToLower());
            // Product Obj = new Product();
            //Note : you can bind same list from database  
            List<User> ObjList = COM.LoadCityAuto(State_id); //  new List<Register>()  

            //    //Searching records from list using LINQ query  
            var City = (from N in ObjList
                        where N.cityname.StartsWith(Prefix)
                        select new { N.cityname, N.cityid });

            if (!City.Any())
            {
                return Json(new { Status = "Failed" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(City, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult ForgotPassword(string emailId)
        {
            User UR = new Models.User();
            UR = COM.GetPassword(emailId);
            string GetResult = MailForgorPassword(emailId, UR.username, UR.password);
            if (GetResult == "Success")
            {
                return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = "Exception", Message = GetResult }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult partialAuctionReview(string status)
        {
            Auction AUC = new Auction();

            if (status == "LatestBid")
            {
                AUC.GetReviewData = CAS.GetLatestBid();
            }
            else if (status == "Lotssignificant")
            {
                AUC.GetReviewData = CAS.GetHighValueBid();
            }
            else if (status == "CurrentMostPopular")
            {
                AUC.GetReviewData = CAS.GetMostPopularLots();
            }
            else if (status == "CurrentClosingLots")
            {
                AUC.GetReviewData = CAS.GetClosingLots();
            }
            return PartialView("_partialAuctionReview", AUC);
        }

        [HttpPost]
        public ActionResult partialListAuctionReview(string status)
        {
            Auction AUC = new Auction();

            if (status == "LatestBid")
            {
                AUC.GetReviewData = CAS.GetLatestBid();
            }
            else if (status == "Lotssignificant")
            {
                AUC.GetReviewData = CAS.GetHighValueBid();
            }
            else if (status == "CurrentMostPopular")
            {
                AUC.GetReviewData = CAS.GetMostPopularLots();
            }
            else if (status == "CurrentClosingLots")
            {
                AUC.GetReviewData = CAS.GetClosingLots();
            }
            return PartialView("_partialListAuctionReview", AUC);
        }


        [HttpPost]
        public ActionResult partialMyAuctionGallary(int userid, int page)
        {
            Auction AUC = new Auction();
            AUC.userid = userid;
            //AUC.GetReviewData = MAS.GetMyAuctionGallaryList(AUC, page);

            //created on 13_02_2020
            AUC.GetReviewData = MAS.GetMyAuctionGallaryList(AUC);
            return PartialView("_partialMyAuctionGallary", AUC);
        }

        [HttpPost]
        public ActionResult partialListMyAuctionGallary(int userid, int page)
        {
            Auction AUC = new Auction();
            AUC.userid = userid;
            //AUC.GetReviewData = MAS.GetMyAuctionGallaryList(AUC, page);
            //created on 13_2_2020
            AUC.GetReviewData = MAS.GetMyAuctionGallaryList(AUC);
            return PartialView("_partialListMyAuctionGallary", AUC);
        }

        public string MailForgorPassword(string emailId, string username, string password)
        {
            string FromEmail = ConfigurationManager.AppSettings["fromId"];
            string Password = ConfigurationManager.AppSettings["smtpPassword"];
            string ToEmail = emailId;
            try
            {
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(FromEmail, "contact@astaguru.com");
                msg.To.Add(ToEmail);
                msg.Subject = "Login & Password details of www.astaguru.com";
                string str = string.Empty;
                str = "<table width='650' align='center' cellpadding='0' cellspacing='0' style='font-family: 'Montserrat', sans-serif; line-height:24px ; font-size:14px; font-weight:normal; border:1px solid #ebebeb;'>";
                str += "<tr bgcolor='#282831'>";
                str += "<td width='220' style='padding:20px 0 20px 20px'><a href='https://astaguru.com/'><img src='https://www.astaguru.com/content/images/Email/logo.png' width='175' height='44' /></a></td>";
                str += "<td colspan='2'>";
                str += "<a href='https://www.astaguru.com/LiveAuction' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Live Auctions</a>";
                str += "<a href='https://www.astaguru.com/UpcomingAuctions' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Upcoming Auctions</a>";
                str += "<a href='https://www.astaguru.com/PastAuctions' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Past Auctions</a>";
                str += "</td>";
                str += "</tr>";
                str += "<td colspan='3' style='position:relative;'><a href='https://www.astaguru.com/ContactUs'><img src='https://www.astaguru.com/content/images/Email/banner_ContactUs.jpg'  width='650'/></a> ";
                //str += "<a href='https://www.astaguru.com/ContactUs' style='background:#c1aa5c; color:#fff; border:1px solid #fff; border-radius:3px; position:absolute; text-decoration: none; font-size: 15px; font-weight: normal;  text-align: center; padding: 5px 10px; left: 38%; bottom: 35px; transform:all .3s ease-in;'>Verify your Account</a>";
                str += "</td>";
                str += "</tr>";
                str += "<tr>";
                str += "<td  colspan='3' style='padding:30px;'>";
                str += "Hi, ";
                str += "<br>Please find below your username and password to login into our website - <a href='http://www.astaguru.com'>http://www.astaguru.com/</a>";
                str += "<br><br>Username: " + username;
                str += "<br>Password: " + password;
                str += "<br><br>Regards,";
                str += "<br>AstaGuru";
                str += "<br><a href='http://www.astaguru.com'>http://www.astaguru.com/</a>";
                str += "</td>";
                str += "</tr> ";
                str += "<tr  bgcolor='#ebebeb' >";
                str += "<td  width='220' style='padding:20px 0 20px 30px'><img src='https://www.astaguru.com/content/images/Email/logo_black.png' width='122' height='31' /></td>";
                str += "<td align='center'>";
                str += "<p style='font-size:12px; text-align:center; margin-bottom:5px; '><span style='border-bottom:1px solid #000;'>Follow Us</span></p>";
                str += "<a href='https://www.facebook.com/Astaguru-Auction-House-375561749218131/?fref=ts' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/facebook_icon.png'  title='Facebook' /></a>";
                str += "<a href='https://in.pinterest.com/astaguru/' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/pinterest_icon.png' title='Pinterest' /></a>";
                str += "<a href='https://twitter.com/astagurutweets' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/twitter_icon.png'  title='Twitter' /></a>";
                str += "<a href='https://www.instagram.com/astaguru/' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/instagram_icon.png'  title='Instagram' /></a>";
                str += "<a href='https://www.youtube.com/channel/UCmTqSUMAHV5l0mACoK72t7g' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/youtube_icon.png'  title='Youtube' /></a>";
                str += "</td>";
                str += "<td align='center'>";
                str += "<p style='font-size:12px; text-align:center; margin-bottom:5px; '><span style='border-bottom:1px solid #000;'>Download Our App</span></p>";
                str += "<a href='https://play.google.com/store/apps/details?id=com.astaguru&hl=en' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/google_play.png'  title='Google Play' /></a>";
                str += "<a href='https://apps.apple.com/app/id1483891832' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/app_store.png'  title='App Store' /></a>";
                str += "</td>";
                str += "</tr>";
                str += "</table>";

                //msg.Body = str;
                msg.IsBodyHtml = true;
                string html = str;
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(html, new ContentType("text/html"));
                msg.AlternateViews.Add(htmlView);
                //SmtpClient sc = new SmtpClient("sarpstechnologies.com", 25);
                SmtpClient sc = new SmtpClient(ConfigurationManager.AppSettings["smtpHost"], Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]));
                sc.Host = "smtp.gmail.com";
                sc.Port = 587;
                sc.UseDefaultCredentials = true;
                // sc.Host = "sarpstechnologies.com";
                //  sc.Port = 25;
                sc.Credentials = new NetworkCredential(FromEmail, Password);
                sc.EnableSsl = true; // Hide this If u use instead of gmail
                sc.Send(msg);

                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        public string mailFunction(User UR)
        {
            string basePathUrl = Common.baseUrlPath;
            string FromEmail = ConfigurationManager.AppSettings["fromId"];
            string Password = ConfigurationManager.AppSettings["smtpPassword"];
            string ToEmail = UR.email;
            try
            {
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(FromEmail, "contact@astaguru.com");
                msg.To.Add(ToEmail);
                msg.Subject = "Your Registration with AstaGuru.com is confirmed & complete.";
                string str = string.Empty;
                //str = "Dear  " + UR.name + " " + UR.lastname + ",";
                //str += "<br><br><b>Congratulations!</b> Your AstaGuru account is now active.";
                //str += "<br><br>Your AstaGuru Login-ID and Password are provided below. Please use the same Login-Id and Password to access your account.";
                //str += "<br><br><b>Your Login Id:</b> " + UR.username;
                //str += "<br><br><b>Password:</b> " + UR.password;
                //str += "<br><br><b><u>Please Note:</u></b> In order to participate in our auctions first time bidders will need  " + "\"Bidding Access\"" + " that will be provided only upon completion of the following verification process.";

                //str += "<br><br>A: Login to Astaguru.com and upload your KYC documents in ‘My Profile’ section.";
                //str += "<br><br>B: A telephonic verification with our representative. You will be contacted on the registered mobile number, please note this is an onetime procedure.";
                //str += "<br><br>C: First time bidders are requested to transfer a sum of Rs. 5,00,000 or equivalent US$ value as an onetime ‘Security Deposit’. Please note this amount will be adjusted in case you have won any lot(s). If you have not acquired any lot(s), the amount will be credited in your respective bank account within 7 working days";
                //str += "<br><br>Mode of Transfer: DD/RTGS/NEFT";
                //str += "<br><br>AstaGuru Bank Details:";
                //str += "<br><br>Name of the Beneficiary: Astaguru.com";
                //str += "<br><br>Bank Name: ICICI Bank,";
                //str += "<br><br>Branch: 240 Navsari Building, D. N. Road, Fort, Mumbai - 400001. India.";
                //str += "<br><br>Account No: 623505385049";
                //str += "<br><br>Swift Code: ICICINBBCTS";
                //str += "<br><br>RTGS / NEFT/ IFSC Code: ICIC0006235";
                //str += "<br><br>*Please do share the details of your bank account in which you would like AstaGuru to credit the balance / full ‘Security Deposit’ amount in the ‘My Profile’ section.";
                //str += "<br><br>We sincerely appreciate your due diligent response. As part of AstaGuru’s objective of security and procedural efficacy we request you to complete the verification process as advised. Failing to complete the same would result in your account being deactivated during the course of our auctions. Rest assured that customer confidentially is of utmost importance to us. Your information will be never be shared with any third party.";
                //str += "<br><br><br>Thank You & Warm Regards,";
                //str += "<br><br>Team AstaGuru.";

                str = "<table width='650' align='center' cellpadding='0' cellspacing='0' style='font-family: 'Montserrat', sans-serif; line-height:24px ; font-size:14px; font-weight:normal; border:1px solid #ebebeb;'>";
                str += "<tr bgcolor='#282831'>";
                str += "<td width='220' style='padding:20px 0 20px 20px'><a href='https://astaguru.com/'><img src='https://www.astaguru.com/content/images/Email/logo.png' width='175' height='44' /></a></td>";
                str += "<td colspan='2'>";
                str += "<a href='https://www.astaguru.com/LiveAuction' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Live Auctions</a>";
                str += "<a href='https://www.astaguru.com/UpcomingAuctions' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Upcoming Auctions</a>";
                str += "<a href='https://www.astaguru.com/PastAuctions' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Past Auctions</a>";
                str += "</td>";
                str += "</tr>";
                str += "<td colspan='3' style='position:relative;'><a href='https://www.astaguru.com/ContactUs'><img src='https://www.astaguru.com/content/images/Email/banner_ContactUs.jpg'  width='650'/></a> ";
                //str += "<a href='https://www.astaguru.com/ContactUs' style='background:#c1aa5c; color:#fff; border:1px solid #fff; border-radius:3px; position:absolute; text-decoration: none; font-size: 15px; font-weight: normal;  text-align: center; padding: 5px 10px; left: 38%; bottom: 35px; transform:all .3s ease-in;'>Verify your Account</a>";
                str += "</td>";
                str += "</tr>";
                str += "<tr>";
                str += "<td  colspan='3' style='padding:30px;'>";
                //str += "<h2>Congratulations !</h2>";
                str += "<p style='text-align:justify'>Dear  " + UR.name + " " + UR.lastname + ",</p>";
                str += "<p style='text-align:justify'><b>Congratulations!</b> Your AstaGuru account is now active.<br />Your AstaGuru Login - ID and Password are provided below.Please use the same Login-Id and Password to access your account.</p> ";
                str += "<p style='text-align:justify'><strong>Your Login Id:</strong> " + UR.username + "<br/><strong>Password:</strong> " + UR.password + "</p> ";
                str += "<p style='text-align:justify'><strong><u>Please Note:</u></strong><br />In order to participate in our auctions first time bidders will need <b>'Bidding Access'</b> that will be provided only upon completion of the following verification process.</p>";
                str += "<p style='text-align:justify'><strong>A:</strong> Login to Astaguru.com and upload your KYC documents in 'My Profile' section.<br /><br />";
                str += "<strong>B: </strong>A telephonic verification with our representative. You will be contacted on the registered mobile number, please note this is an onetime procedure.<br /><br />";
                str += "<strong>C:</strong> First time bidders are requested to transfer a sum of Rs. 5,00,000 or equivalent US$ value as an onetime <b>'Security Deposit'</b>. Please note this amount will be adjusted in case you have won any lot(s). If you have not acquired any lot(s), the amount will be credited in your respective bank account within 7 working days.</p>";
                str += "<p style='text-align:justify;'><b>Mode of Transfer:</b> DD/RTGS/NEFT</p>";
                str += "<p style=text-align:justify;'><b>AstaGuru Bank Details:</b> <br />";
                str += "<b>Name of the Beneficiary:</b>AstaGuru Auction House Private Limited<br/>";
                str += "<b>Bank Name:</b> ICICI Bank,<br />";
                str += "<b>Branch:</b> 240 Navsari Building, D. N. Road, Fort, Mumbai - 400001. India.<br />";
                str += "<b>Account No:</b> 623505386641<br />";
                str += "<b>Swift Code:</b> ICICINBBCTS<br />";
                str += "<b>RTGS / NEFT/ IFSC Code:</b> ICIC0006235</p>";
                str += "<p style='text-align:justify;'>*Please do share the details of your bank account in which you would like AstaGuru to credit the balance / full 'Security Deposit' amount in the <b>'My Profile'</b> section.</p>";
                str += "<p style='text-align:justify;'>We sincerely appreciate your due diligent response. As part of AstaGuru's objective of security and procedural efficacy we request you to complete the verification process as advised. Failing to complete the same would result in your account being deactivated during the course of our auctions. Rest assured that customer confidentially is of utmost importance to us. Your information will be never be shared with any third party.</p>";
                str += "<p style='text-align:justify;'>Thank You & Warm Regards,</p>";
                str += "<p style='text-align:justify;'>Team AstaGuru.</p>";
                str += "</td>";
                str += "</tr> ";
                str += "<tr  bgcolor='#ebebeb' >";
                str += "<td  width='220' style='padding:20px 0 20px 30px'><img src='https://www.astaguru.com/content/images/Email/logo_black.png' width='122' height='31' /></td>";
                str += "<td align='center'>";
                str += "<p style='font-size:12px; text-align:center; margin-bottom:5px; '><span style='border-bottom:1px solid #000;'>Follow Us</span></p>";
                str += "<a href='https://www.facebook.com/Astaguru-Auction-House-375561749218131/?fref=ts' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/facebook_icon.png'  title='Facebook' /></a>";
                str += "<a href='https://in.pinterest.com/astaguru/' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/pinterest_icon.png' title='Pinterest' /></a>";
                str += "<a href='https://twitter.com/astagurutweets' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/twitter_icon.png'  title='Twitter' /></a>";
                str += "<a href='https://www.instagram.com/astaguru/' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/instagram_icon.png'  title='Instagram' /></a>";
                str += "<a href='https://www.youtube.com/channel/UCmTqSUMAHV5l0mACoK72t7g' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/youtube_icon.png'  title='Youtube' /></a>";
                str += "</td>";
                str += "<td align='center'>";
                str += "<p style='font-size:12px; text-align:center; margin-bottom:5px; '><span style='border-bottom:1px solid #000;'>Download Our App</span></p>";
                str += "<a href='https://play.google.com/store/apps/details?id=com.astaguru&hl=en' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/google_play.png'  title='Google Play' /></a>";
                str += "<a href='https://apps.apple.com/app/id1483891832' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/app_store.png'  title='App Store' /></a>";
                str += "</td>";
                str += "</tr>";
                str += "</table>";


                msg.IsBodyHtml = true;
                string html = str;
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(html, new ContentType("text/html"));
                msg.AlternateViews.Add(htmlView);
                //SmtpClient sc = new SmtpClient("sarpstechnologies.com", 25);
                SmtpClient sc = new SmtpClient(ConfigurationManager.AppSettings["smtpHost"], Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]));
                sc.Host = "smtp.gmail.com";
                sc.Port = 587;
                sc.UseDefaultCredentials = true;
                // sc.Host = "sarpstechnologies.com";
                //  sc.Port = 25;
                sc.Credentials = new NetworkCredential(FromEmail, Password);
                sc.EnableSsl = true; // Hide this If u use instead of gmail
                sc.Send(msg);

                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        public string sendMailOutbid(Auction AUC)
        {
            User UR = US.GetBillingAddress(AUC.LastBidId);

            string basePathUrl = Common.baseUrlPath;
            string FromEmail = ConfigurationManager.AppSettings["fromId"];
            string Password = ConfigurationManager.AppSettings["smtpPassword"];
            string ToEmail = UR.email;
            try
            {
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(FromEmail, "contact@astaguru.com");
                msg.To.Add(ToEmail);
                msg.Subject = "AstaGuru - You have been Outbid on Lot# " + AUC.reference.Trim() + "";
                string str = string.Empty;
                //str = "Dear  " + AUC.firstname;
                //str += "</b><br><br>We would like to bring it to your notice that you have been outbid on Lot# " + AUC.reference + ",in the ongoing AstaGuru Online Auction. Your highest bid was on Rs." + Common.rupeeFormat(AUC.mailPreprice.ToString()) + "($" + AUC.mailPrepriceUs + ")" + " The current highest bid stands at  " + Common.rupeeFormat(AUC.curprice.ToString()) + "($" + AUC.curpriceUs + "). Continue to contest for Lot# " + AUC.reference + ", please place your updated bid here <a href=\"" + basePathUrl + "/Home/LotDetails?productid=" + AUC.productid + "\">Click here.</a>";
                //str += "<br><br><br>";
                //str += "<br><br><br>In case you have any queries with regards to the Lots that are part of the auction or the bidding process, please feel free to contact us on 91-22 2204 8138/39 or write to us at contact@astaguru.com. Our team will be glad to assist you with the same.<br><br><br>";

                //str += "Lot No : " + AUC.reference + " <br>Title :" + AUC.title + "<br>";
                //str += "Current Highest Bid : Rs." + Common.rupeeFormat(AUC.curprice.ToString()) + " ($" + AUC.curpriceUs + ")<br>Next Incremental Bid Amount : Rs." + Common.rupeeFormat(AUC.nextValidBidRs.ToString()) + " ($" + AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]) + ")<br><br><br><img src='http://www.astaguru.com/" + AUC.thumbnail + "'><br><br><br>";
                //str += "In case you have any queries with regards to the Lots that are part of the auction or the bidding process, please feel free to contact us on 91-22 2204 8138/39 or write to us at contact@astaguru.com. Our team will be glad to assist you with the same.<br><br><br>";
                //str += "Warmest Regards,<br>Team AstaGuru.";

                str = "<table width='650' align='center' cellpadding='0' cellspacing='0' style='font-family: 'Montserrat', sans-serif; line-height:24px ; font-size:14px; font-weight:normal; border:1px solid #ebebeb;'>";
                str += "<tr bgcolor='#282831'>";
                str += "<td width='220' style='padding:20px 0 20px 20px'><a href='https://astaguru.com/'><img src='https://www.astaguru.com/content/images/Email/logo.png' width='175' height='44' /></a></td>";
                str += "<td colspan='2'>";
                str += "<a href='https://www.astaguru.com/LiveAuction' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Live Auctions</a>";
                str += "<a href='https://www.astaguru.com/UpcomingAuctions' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Upcoming Auctions</a>";
                str += "<a href='https://www.astaguru.com/PastAuctions' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Past Auctions</a>";
                str += "</td>";
                str += "</tr>";
                str += "<tr>";
                str += "<td colspan='3' style='position:relative;'><a href='" + basePathUrl + "/Home/LotDetails?productid=" + AUC.productid + "&page=0' target='_blank'><img src='https://www.astaguru.com/content/images/Email/banner_place_bid.jpg'  width='650'/></a>";
                //str += "<a href='" + basePathUrl + "/Home/LotDetails?productid=" + AUC.productid + "&page=0' target='_blank' style='background:#c1aa5c; color:#fff; border:1px solid #fff; border-radius:3px; position:absolute; text-decoration: none; font-size: 15px; font-weight: normal;  text-align: center; padding: 5px 10px; left: 38%; bottom: 35px; transform:all .3s ease-in;'>Place Another Bid</a>";
                str += "</td>";
                str += "</tr>";
                str += "<tr>";
                str += "<td  colspan='3' style='padding:30px;'>";
                //str += "<h2>You have been Outbid on Lot #" + AUC.reference + "</h2>";
                str += "<p style='text-align:justify'>Dear " + AUC.name + " " + AUC.lastname + ",</p>";
                str += "<p style='text-align:justify'>We would like to bring it to your notice that you have been outbid on Lot #" + AUC.reference.Trim() + ", in the ongoing AstaGuru Online Auction. Your bid value was <b>Rs. " + Common.rupeeFormat(AUC.mailPreprice.ToString()) + "($" + Common.DollerFormat((AUC.mailPreprice / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"])).ToString()) + ")</b>. The current highest bid stands at <b>Rs. " + Common.rupeeFormat(AUC.curprice.ToString()) + "($" + Common.DollerFormat((AUC.curprice / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"])).ToString()) + ")</b>. Continue to contest for Lot# " + AUC.reference.Trim() + ", please place your updated bid here <a href=\"" + basePathUrl + "/Home/LotDetails?productid=" + AUC.productid + "&page=0\">Click here.</a></p>";
                str += "<p style='text-align:justify'>In case you have any queries with regards to the Lots that are part of the auction or the bidding process, please feel free to contact us on <strong>+91-22-69014800</strong> or write to us at <a href='mailto:contact@astaguru.com'>contact@astaguru.com</a>. Our team will be glad to assist you with the same.</p>";

                str += "<p style='text-align:justify'><b>Lot#</b>: " + AUC.reference.Trim() + " <br><b>Title</b>:" + AUC.title + "<br>";
                str += "<b>Current Highest Bid</b>: Rs." + Common.rupeeFormat(AUC.curprice.ToString()) + " ($" + Common.DollerFormat((AUC.curprice / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"])).ToString()) + ")<br><b>Next Incremental Bid Amount</b>: Rs. " + Common.rupeeFormat(AUC.nextValidBidRs.ToString()) + " ($" + Common.DollerFormat((AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"])).ToString()) + ")</p><p><img src='http://www.astaguru.com/" + AUC.thumbnail + "' width='200' height='200'></p>";
                str += "<p style='text-align:justify'>In case you have any queries with regards to the Lots that are part of the auction or the bidding process, please feel free to contact us on 91-22-69014800or or write to us at <a href='mailto:contact@astaguru.com'>contact@astaguru.com</a>. Our team will be glad to assist you with the same.</p>";
                str += "<p style='text-align:justify'>Warmest Regards,</p>";
                str += "<p style='text-align:justify'>Team AstaGuru.</p>";
                str += "</td>";
                str += "</tr> ";
                str += "<tr bgcolor='#ebebeb' >";
                str += "<td  width='220' style='padding:20px 0 20px 30px'><img src='https://www.astaguru.com/content/images/Email/logo_black.png' width='122' height='31' /></td>";
                str += "<td align='center'>";
                str += "<p style='font-size:12px; text-align:center; margin-bottom:5px; '><span style='border-bottom:1px solid #000;'>Follow Us</span></p>";
                str += "<a href='https://www.facebook.com/Astaguru-Auction-House-375561749218131/?fref=ts' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/facebook_icon.png'  title='Facebook' /></a>";
                str += "<a href='https://in.pinterest.com/astaguru/' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/pinterest_icon.png' title='Pinterest' /></a>";
                str += "<a href='https://twitter.com/astagurutweets' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/twitter_icon.png'  title='Twitter' /></a>";
                str += "<a href='https://www.instagram.com/astaguru/' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/instagram_icon.png'  title='Instagram' /></a>";
                str += "<a href='https://www.youtube.com/channel/UCmTqSUMAHV5l0mACoK72t7g' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/youtube_icon.png'  title='Youtube' /></a>";
                str += "</td>";
                str += "<td align='center'>";
                str += "<p style='font-size:12px; text-align:center; margin-bottom:5px; '><span style='border-bottom:1px solid #000;'>Download Our App</span></p>";
                str += "<a href='https://play.google.com/store/apps/details?id=com.astaguru&hl=en' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/google_play.png'  title='Google Play' /></a>";
                str += "<a href='https://apps.apple.com/app/id1483891832' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/app_store.png'  title='App Store' /></a>";
                str += "</td>";
                str += "</tr> ";
                str += "</table>";

                msg.IsBodyHtml = true;
                string html = str;
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(html, new ContentType("text/html"));
                msg.AlternateViews.Add(htmlView);
                //SmtpClient sc = new SmtpClient("sarpstechnologies.com", 25);
                SmtpClient sc = new SmtpClient(ConfigurationManager.AppSettings["smtpHost"], Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]));
                sc.Host = "smtp.gmail.com";
                sc.Port = 587;
                sc.UseDefaultCredentials = true;
                // sc.Host = "sarpstechnologies.com";
                //  sc.Port = 25;
                sc.Credentials = new NetworkCredential(FromEmail, Password);
                sc.EnableSsl = true; // Hide this If u use instead of gmail
                sc.Send(msg);

                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string upcommingProxy(Auction AUC)
        {
            string basePathUrl = Common.baseUrlPath;
            string FromEmail = ConfigurationManager.AppSettings["fromId"];
            string Password = ConfigurationManager.AppSettings["smtpPassword"];
            string ToEmail = AUC.email;
            try
            {
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(FromEmail, "contact@astaguru.com");
                msg.To.Add(ToEmail);
                msg.Subject = "Proxy Bid acknowledgement";
                string str = string.Empty;
                //str = "Dear  " + AUC.name + " " + AUC.lastname;
                //str += "<br><br>Thank you for placing a Proxy Bid amount of Rs. " + Astaguru.Services.Common.rupeeFormat(AUC.ProxyAmt.ToString()) + "($ " + Astaguru.Services.Common.DollerFormat(AUC.ProxyAmtus.ToString()) + ") for Lot No " + AUC.reference + ", part of our '" + AUC.Auctionname + "' Auction dated " + AUC.auctiondate + ".";
                //str += "<br><br>We would like to acknowledge having received your Proxy Bid, our operations team will review it and revert with confirmation of the approval.";
                //str += "<br><br>In case you are unaware of this transaction please notify us at the earliest about the misrepresentation. ";
                //str += "<br><br>In case you would like to edit the Proxy Bid value please contact us for the same at contact@astaguru.com or call us on 91-22 2204 8138/39. We will be glad to assist you.";
                //str += "<br><br><br>Regards,";
                //str += "<br><br><br>Team Astaguru.";
                //msg.Body = str;

                StringBuilder sb = new StringBuilder();
                sb.Append("<table width='650' align='center' cellpadding='0' cellspacing='0' style='font-family: 'Montserrat', sans-serif); line-height:24px ); font-size:14px); font-weight:normal); border:1px solid #ebebeb;'>");
                sb.Append("<tr bgcolor='#282831'>");
                sb.Append("<td width='220' style='padding:20px 0 20px 20px'><a href='https://astaguru.com/'><img src='https://www.astaguru.com/content/images/Email/logo.png' width='175' height='44' /></a></td>");
                sb.Append("<td colspan='2'>");
                sb.Append("<a href='https://www.astaguru.com/LiveAuction' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Live Auctions</a>");
                sb.Append("<a href='https://www.astaguru.com/UpcomingAuctions' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Upcoming Auctions</a>");
                sb.Append("<a href='https://www.astaguru.com/PastAuctions' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Past Auctions</a>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td colspan='3' style='position:relative;'><a href='https://www.astaguru.com/ContactUs'><img src='https://www.astaguru.com/content/images/Email/banner_ContactUs.jpg'  width='650'/></a>");
                //sb.Append("<a href='https://www.astaguru.com/ContactUs' target='_blank' style='background:#c1aa5c; color:#fff; border:1px solid #fff; border-radius:3px; position:absolute; text-decoration: none; font-size: 15px; font-weight: normal;  text-align: center; padding: 5px 10px; left: 38%; bottom: 35px; transform:all .3s ease-in;'>Verify your Account</a>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td colspan='3' style='padding:30px;'>");
                //sb.Append("<h2>Intimation about Proxy-Bid Approval</h2>");
                sb.Append("<p style='text-align:justify'>Dear " + AUC.name + " " + AUC.lastname + ",</p>");
                sb.Append("<p style='text-align:justify'>Thank you for placing a Proxy Bid amount of <b>Rs. " + Common.rupeeFormat(AUC.ProxyAmt.ToString()) + "($" + Common.DollerFormat(AUC.ProxyAmtus.ToString()) + ")</b> for Lot# "+ AUC.reference.Trim() +", part of our <b>'" + AUC.Auctionname + "'</b> Auction dated " + AUC.auctiondate + ".</p>");
                sb.Append("<p style='text-align:justify'>We would like to acknowledge having received your Proxy Bid, our operations team will review it and revert with confirmation of the approval.</p>");
                sb.Append("<p style='text-align:justify'>In case you are unaware of this transaction please notify us at the earliest about the misrepresentation.</p>");
                sb.Append("<p style='text-align:justify'>For any further assistance please feel free to write to us at, <a href = 'mailto:contact@astaguru.com'> contact@astaguru.com </a> or call us on <strong>+91-22-69014800</strong>. We will be glad to assist.</p> ");
                sb.Append("<p style='text-align:justify'>Thanking You & Warm Regards,</p>");
                sb.Append("<p style='text-align:justify'>Team AstaGuru.</p>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("<tr bgcolor='#ebebeb'>");
                sb.Append("<td width='220' style='padding:20px 0 20px 30px'><img src='https://www.astaguru.com/content/images/Email/logo_black.png' width='122' height='31' /></td>");
                sb.Append("<td align='center'>");
                sb.Append("<p style='font-size:12px)); text-align:center)); margin-bottom:5px)); '><span style='border-bottom:1px solid #000);'>Follow Us</span></p>");
                sb.Append("<a href='https://www.facebook.com/Astaguru-Auction-House-375561749218131/?fref=ts' target='_blank' style='margin:0 2px);'><img src='https://www.astaguru.com/content/images/Email/facebook_icon.png'  title='Facebook'/></a>");
                sb.Append("<a href='https://in.pinterest.com/astaguru/' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/pinterest_icon.png' title='Pinterest' /></a>");
                sb.Append("<a href='https://twitter.com/astagurutweets' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/twitter_icon.png'  title='Twitter' /></a>");
                sb.Append("<a href='https://www.instagram.com/astaguru/' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/instagram_icon.png'  title='Instagram' /></a>");
                sb.Append("<a href='https://www.youtube.com/channel/UCmTqSUMAHV5l0mACoK72t7g' target='_blank' style='margin:0 2px);'><img src='https://www.astaguru.com/content/images/Email/youtube_icon.png'  title='Youtube'/></a>");
                sb.Append("</td>");
                sb.Append("<td align='center'>");
                sb.Append("<p style='font-size:12px; text-align:center; margin-bottom:5px; '><span style='border-bottom:1px solid #000;'>Download Our App</span></p>");
                sb.Append("<a href='https://play.google.com/store/apps/details?id=com.astaguru&hl=en' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/google_play.png'  title='Google Play' /></a>");
                sb.Append("<a href='https://apps.apple.com/app/id1483891832' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/app_store.png'  title='App Store' /></a>");
                sb.Append("</td>");
                sb.Append("</tr> ");
                sb.Append("</table>");


                msg.IsBodyHtml = true;
                //string html = str;
                string html = sb.ToString();
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(html, new ContentType("text/html"));
                msg.AlternateViews.Add(htmlView);
                //SmtpClient sc = new SmtpClient("sarpstechnologies.com", 25);
                SmtpClient sc = new SmtpClient(ConfigurationManager.AppSettings["smtpHost"], Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]));
                sc.Host = "smtp.gmail.com";
                sc.Port = 587;
                sc.UseDefaultCredentials = true;
                // sc.Host = "sarpstechnologies.com";
                //  sc.Port = 25;
                sc.Credentials = new NetworkCredential(FromEmail, Password);
                sc.EnableSsl = true; // Hide this If u use instead of gmail
                sc.Send(msg);

                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        //created on 02_03_2020
        public string sendMailOutbiduser(Auction AUC, int userid, string name,string lastname)
        {
            User UR = US.GetBillingAddress(userid);

            string basePathUrl = Common.baseUrlPath;
            string FromEmail = ConfigurationManager.AppSettings["fromId"];
            string Password = ConfigurationManager.AppSettings["smtpPassword"];
            string ToEmail = UR.email;
            try
            {
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(FromEmail, "contact@astaguru.com");
                msg.To.Add(ToEmail);
                msg.Subject = "AstaGuru - You have been Outbid on Lot #" + AUC.reference + "";
                string str = string.Empty;
                //str = "Dear  " + username;
                //str += "</b><br><br>We would like to bring it to your notice that you have been outbid on Lot# " + AUC.reference + ",in the ongoing AstaGuru Online Auction. Your bid value was INR." + Common.rupeeFormat(AUC.mailPreprice.ToString()) + "($" + AUC.mailPreprice / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]) + ")" + " The current highest bid stands at  " + Common.rupeeFormat(AUC.curprice.ToString()) + "($" + AUC.curprice / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]) + "). Continue to contest for Lot# " + AUC.reference + ", please place your updated bid here <a href=\"" + basePathUrl + "/Home/LotDetails?productid=" + AUC.productid + "&page=" + AUC.page + "\">Click here.</a>";
                //str += "<br><br><br>";
                //str += "<br><br><br>In case you have any queries with regards to the Lots that are part of the auction or the bidding process, please feel free to contact us on 91-22-69014800or or write to us at contact@astaguru.com. Our team will be glad to assist you with the same.<br><br><br>";

                //str += "Lot No : " + AUC.reference + " <br>Title :" + AUC.title + "<br>";
                //str += "Current Highest Bid : Rs." + Common.rupeeFormat(AUC.curprice.ToString()) + " ($" + AUC.curprice / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]) + ")<br>Next Incremental Bid Amount : Rs." + Common.rupeeFormat(AUC.nextValidBidRs.ToString()) + " ($" + AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]) + ")<br><br><br><img src='http://www.astaguru.com/" + AUC.thumbnail + "'><br><br><br>";
                //str += "In case you have any queries with regards to the Lots that are part of the auction or the bidding process, please feel free to contact us on 91-22-69014800or or write to us at contact@astaguru.com. Our team will be glad to assist you with the same.<br><br><br>";
                //str += "Warmest Regards,<br>Team AstaGuru.";

                str = "<table width='650' align='center' cellpadding='0' cellspacing='0' style='font-family: 'Montserrat', sans-serif; line-height:24px ; font-size:14px; font-weight:normal; border:1px solid #ebebeb;'>";
                str += "<tr bgcolor='#282831'>";
                str += "<td width='220' style='padding:20px 0 20px 20px'><a href='https://astaguru.com/'><img src='https://www.astaguru.com/content/images/Email/logo.png' width='175' height='44' /></a></td>";
                str += "<td colspan='2'>";
                str += "<a href='https://www.astaguru.com/LiveAuction' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Live Auctions</a>";
                str += "<a href='https://www.astaguru.com/UpcomingAuctions' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Upcoming Auctions</a>";
                str += "<a href='https://www.astaguru.com/PastAuctions' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Past Auctions</a>";
                str += "</td>";
                str += "</tr>";
                str += "<tr>";
                str += "<td colspan='3' style='position:relative;'><a href='" + basePathUrl + "/Home/LotDetails?productid=" + AUC.productid + "&page=0' target='_blank'><img src='https://www.astaguru.com/content/images/Email/banner_place_bid.jpg'  width='650'/></a>";
                //str += "<a href='"+ basePathUrl + "/Home/LotDetails?productid=" + AUC.productid + "&page=0' target='_blank' style='background:#c1aa5c; color:#fff; border:1px solid #fff; border-radius:3px; position:absolute; text-decoration: none; font-size: 15px; font-weight: normal;  text-align: center; padding: 5px 10px; left: 38%; bottom: 35px; transform:all .3s ease-in;'>Place Another Bid</a>";
                str += "</td>";
                str += "</tr>";
                str += "<tr>";
                str += "<td  colspan='3' style='padding:30px;'>";
                //str += "<h2>You have been Outbid on Lot# " + AUC.reference + "</h2>";
                str += " <p style='text-align:justify'>Dear " + name + " "+ lastname +",</p>";
                str += "<p style='text-align:justify'>We would like to bring it to your notice that you have been outbid on Lot# "+AUC.reference.Trim()+", in the ongoing AstaGuru Online Auction. Your bid value was <b>Rs. " + Common.rupeeFormat(AUC.mailPreprice.ToString())+"($"+Common.DollerFormat((AUC.mailPreprice / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"])).ToString())+")</b>. The current highest bid stands at <b>Rs. " + Common.rupeeFormat(AUC.curprice.ToString()) + "($" + Common.DollerFormat((AUC.curprice / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"])).ToString()) + ")</b>. Continue to contest for Lot# "+AUC.reference.Trim()+", please place your updated bid here <a href=\"" + basePathUrl + "/Home/LotDetails?productid=" + AUC.productid + "&page=0\">Click here.</a></p>";
                str += "<p style='text-align:justify'>In case you have any queries with regards to the Lots that are part of the auction or the bidding process, please feel free to contact us on <strong>+91-22-69014800</strong> or write to us at <a href='mailto:contact@astaguru.com'>contact@astaguru.com</a>. Our team will be glad to assist you with the same.</p>";
                str += "<p style='text-align:justify'><b>Lot#:</b> " + AUC.reference.Trim() + " <br><b>Title:</b> " + AUC.title + "<br>";
                str += "<b>Current Highest Bid:</b> Rs. " + Common.rupeeFormat(AUC.curprice.ToString()) + "($" + Common.DollerFormat((AUC.curprice / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"])).ToString()) + ")<br><b>Next Incremental Bid Amount:</b> Rs. " + Common.rupeeFormat(AUC.nextValidBidRs.ToString()) + "($" + Common.DollerFormat((AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"])).ToString()) + ")</p><p><img src='http://www.astaguru.com/" + AUC.thumbnail + "' width='200' height='200'></p>";
                str += "<p style='text-align:justify'>In case you have any queries with regards to the Lots that are part of the auction or the bidding process, please feel free to contact us on 91-22-69014800or or write to us at <a href='mailto:contact@astaguru.com'>contact@astaguru.com</a>. Our team will be glad to assist you with the same.</p>";
                str += "<p style='text-align:justify'>Warmest Regards,</p>";
                str += "<p style='text-align:justify'>Team AstaGuru.</p>";
                str += "</td>";
                str += "</tr>";
                str += "<tr bgcolor='#ebebeb' >";
                str += "<td width='220' style='padding:20px 0 20px 30px'><img src='https://www.astaguru.com/content/images/Email/logo_black.png' width='122' height='31' /></td>";
                str += "<td align='center'>";
                str += "<p style='font-size:12px; text-align:center; margin-bottom:5px; '><span style='border-bottom:1px solid #000;'>Follow Us</span></p>";
                str += "<a href='https://www.facebook.com/Astaguru-Auction-House-375561749218131/?fref=ts' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/facebook_icon.png'  title='Facebook' /></a>";
                str += "<a href='https://in.pinterest.com/astaguru/' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/pinterest_icon.png' title='Pinterest' /></a>";
                str += "<a href='https://twitter.com/astagurutweets' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/twitter_icon.png'  title='Twitter' /></a>";
                str += "<a href='https://www.instagram.com/astaguru/' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/instagram_icon.png'  title='Instagram' /></a>";
                str += "<a href='https://www.youtube.com/channel/UCmTqSUMAHV5l0mACoK72t7g' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/youtube_icon.png'  title='Youtube' /></a>";
                str += "</td>";
                str += "<td align='center'>";
                str += "<p style='font-size:12px; text-align:center; margin-bottom:5px; '><span style='border-bottom:1px solid #000;'>Download Our App</span></p>";
                str += "<a href='https://play.google.com/store/apps/details?id=com.astaguru&hl=en' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/google_play.png'  title='Google Play' /></a>";
                str += "<a href='https://apps.apple.com/app/id1483891832' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/app_store.png'  title='App Store' /></a>";
                str += "</td>";
                str += "</tr> ";
                str += "</table>";
                

                //msg.Body = str;
                msg.IsBodyHtml = true;
                string html = str;
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(html, new ContentType("text/html"));
                msg.AlternateViews.Add(htmlView);
                //SmtpClient sc = new SmtpClient("sarpstechnologies.com", 25);
                SmtpClient sc = new SmtpClient(ConfigurationManager.AppSettings["smtpHost"], Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]));
                sc.Host = "smtp.gmail.com";
                sc.Port = 587;
                sc.UseDefaultCredentials = true;
                // sc.Host = "sarpstechnologies.com";
                //  sc.Port = 25;
                sc.Credentials = new NetworkCredential(FromEmail, Password);
                sc.EnableSsl = true; // Hide this If u use instead of gmail
                sc.Send(msg);

                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        //created on 13_08_20
        public void sendoutbidmsg(string reference, string name, string mobile,string lastname)
        {
            //string authKey = "105048AuvvUDCr56c199f0";
            string authkey = "338500AD9H4VOHQl5f3135e8P1";
            //Multiple mobiles numbers separated by comma
            string mobiles = mobile;
            //Sender ID,While using route4 sender id should be 6 characters long.
            //string sender = "ASTGUR";
            string sender = "ASTGUR";
            //Your message to send, Add URL encoding here.
            string message = HttpUtility.UrlEncode("Dear " + name + " "+ lastname + ", please note you have been outbid on Lot No." + reference.Trim() + ". Place a new bid on www.astaguru.com or mobile App");
            string route = "4";

            // dlt_te_id added 24_02_2021
            string dlt_te_id = "1307161353757663068";
            //string country = "91";

            //Prepare you post parameters
            StringBuilder sbPostData = new StringBuilder();
            sbPostData.AppendFormat("authkey={0}", authkey);
            sbPostData.AppendFormat("&mobiles={0}", mobiles);
            sbPostData.AppendFormat("&message={0}", message);
            sbPostData.AppendFormat("&sender={0}", sender);
            sbPostData.AppendFormat("&route={0}", route);
            sbPostData.AppendFormat("&DLT_TE_ID={0}", dlt_te_id);
            //sbPostData.AppendFormat("&country={0}", country);

            try
            {
                //Call Send SMS API
                string sendSMSUri = "http://api.msg91.com/api/sendhttp.php";
                //Create HTTPWebrequest
                HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(sendSMSUri);
                //Prepare and Add URL Encoded data
                UTF8Encoding encoding = new UTF8Encoding();
                byte[] data = encoding.GetBytes(sbPostData.ToString());
                //Specify post method
                httpWReq.Method = "POST";
                httpWReq.ContentType = "application/x-www-form-urlencoded";
                httpWReq.ContentLength = data.Length;
                using (Stream stream = httpWReq.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                //Get the response
                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string responseString = reader.ReadToEnd();

                //Close the response
                reader.Close();
                response.Close();


            }
            catch (SystemException ex)
            {

            }
        }

        //created on 21_12_2020
        public string CurrentProxyMail(Auction AUC)
        {
            string basePathUrl = Common.baseUrlPath;
            string FromEmail = ConfigurationManager.AppSettings["fromId"];
            string Password = ConfigurationManager.AppSettings["smtpPassword"];
            string ToEmail = AUC.email;
            try
            {
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(FromEmail, "contact@astaguru.com");
                msg.To.Add(ToEmail);
                msg.Subject = "Intimation about Proxy-Bid Approval";
                //string str = string.Empty;
                //str = "Dear  " + AUC.name + " " + AUC.lastname;
                //str += "<br><br>We are glad to inform you that your Proxy Bid amount of Rs. " + Astaguru.Services.Common.rupeeFormat(AUC.ProxyAmt.ToString()) + "($ " + Astaguru.Services.Common.DollerFormat(AUC.ProxyAmtus.ToString()) + ") for Lot No " + AUC.reference + ", part of our '" + AUC.Auctionname + "' Auction dated " + AUC.auctiondate + "has been accepted.";
                //str += "<br><br>For any further assistance please feel free to write to us at contact@astaguru.com or call us on 91-22 2204 8138/39. We will be glad to assist you.";
                //str += "<br><br><br>Thank You.";
                //str += "<br><br><br>Team Astaguru.";

                StringBuilder sb = new StringBuilder();
                sb.Append("<table width='650' align='center' cellpadding='0' cellspacing='0' style='font-family: 'Montserrat', sans-serif); line-height:24px ); font-size:14px); font-weight:normal); border:1px solid #ebebeb;'>");
                sb.AppendFormat("<tr bgcolor='#282831'>");
                sb.AppendFormat("<td width='220' style='padding:20px 0 20px 20px'><a href='https://astaguru.com/'><img src='https://www.astaguru.com/content/images/Email/logo.png' width='175' height='44' /></a></td>");
                sb.AppendFormat("<td colspan='2'>");
                sb.AppendFormat("<a href='https://www.astaguru.com/LiveAuction' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Live Auctions</a>");
                sb.AppendFormat("<a href='https://www.astaguru.com/UpcomingAuctions' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Upcoming Auctions</a>");
                sb.AppendFormat("<a href='https://www.astaguru.com/PastAuctions' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Past Auctions</a>");
                sb.AppendFormat("</td>");
                sb.AppendFormat("</tr>");
                sb.AppendFormat("<tr>");
                sb.AppendFormat("<td colspan='3' style='position:relative;'><a href='https://www.astaguru.com/ContactUs'><img src='https://www.astaguru.com/content/images/Email/banner_ContactUs.jpg'  width='650'/></a> ");
                //sb.AppendFormat("<a href='https://www.astaguru.com/ContactUs' target='_blank' style='background:#c1aa5c; color:#fff; border:1px solid #fff; border-radius:3px; position:absolute; text-decoration: none; font-size: 15px; font-weight: normal;  text-align: center; padding: 5px 10px; left: 38%; bottom: 35px; transform:all .3s ease-in;'>Verify your Account</a>");
                sb.AppendFormat("</td>");
                sb.AppendFormat("</tr>");
                sb.AppendFormat("<tr>");
                sb.AppendFormat("<td colspan='3' style='padding:30px;'>");
                //sb.AppendFormat("<h2>Intimation about Proxy-Bid Approval</h2>");
                sb.AppendFormat("<p style='text-align:justify'>Dear " + AUC.name + " " + AUC.lastname+",</p>");
                sb.AppendFormat("<p style='text-align:justify'>We are glad to inform you that your Proxy Bid amount of <strong> Rs. " + Common.rupeeFormat(AUC.ProxyAmt.ToString()) + "($" + Common.DollerFormat(AUC.ProxyAmtus.ToString()) + ")</strong> for Lot# " + AUC.reference.Trim() + ", part of our <b>'" + AUC.Auctionname + "'</b> Auction dated " + AUC.auctiondate + " has been accepted.</p>");
                sb.AppendFormat("<p style='text-align:justify'>For any further assistance please feel free to write to us at, <a href = 'mailto:contact@astaguru.com'> contact@astaguru.com </a> or call us on <strong>+91-22-69014800</strong>. We will be glad to assist.</p> ");
                sb.AppendFormat("<p style='text-align:justify'>Thanking You & Warm Regards,</p>");
                sb.AppendFormat("<p style='text-align:justify'>Team AstaGuru.</p>");
                sb.AppendFormat("</td>");
                sb.AppendFormat("</tr>");
                sb.AppendFormat("<tr bgcolor='#ebebeb'>");
                sb.AppendFormat("<td width='220' style='padding:20px 0 20px 30px'><img src='https://www.astaguru.com/content/images/Email/logo_black.png' width='122' height='31' /></td>");
                sb.AppendFormat("<td align='center'>");
                sb.AppendFormat("<p style='font-size:12px)); text-align:center)); margin-bottom:5px)); '><span style='border-bottom:1px solid #000);'>Follow Us</span></p>");
                sb.AppendFormat("<a href='https://www.facebook.com/Astaguru-Auction-House-375561749218131/?fref=ts' target='_blank' style='margin:0 2px);'><img src='https://www.astaguru.com/content/images/Email/facebook_icon.png'  title='Facebook'/></a>");
                sb.AppendFormat("<a href='https://in.pinterest.com/astaguru/' target='_blank' style='margin:0 2px);'><img src='https://www.astaguru.com/content/images/Email/pinterest_icon.png' title='Pinterest' /></a>");
                sb.AppendFormat("<a href='https://twitter.com/astagurutweets' target='_blank' style='margin:0 2px);'><img src='https://www.astaguru.com/content/images/Email/twitter_icon.png'  title='Twitter' /></a>");
                sb.AppendFormat("<a href='https://www.instagram.com/astaguru/' target='_blank' style='margin:0 2px);'><img src='https://www.astaguru.com/content/images/Email/instagram_icon.png'  title='Instagram' /></a>");
                sb.AppendFormat("<a href='https://www.youtube.com/channel/UCmTqSUMAHV5l0mACoK72t7g' target='_blank' style='margin:0 2px);'><img src='https://www.astaguru.com/content/images/Email/youtube_icon.png'  title='Youtube'/></a>");
                sb.AppendFormat("</td>");
                sb.AppendFormat("<td align='center'>");
                sb.AppendFormat("<p style='font-size:12px; text-align:center; margin-bottom:5px; '><span style='border-bottom:1px solid #000;'>Download Our App</span></p>");
                sb.AppendFormat("<a href='https://play.google.com/store/apps/details?id=com.astaguru&hl=en' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/google_play.png'  title='Google Play' /></a>");
                sb.AppendFormat("<a href='https://apps.apple.com/app/id1483891832' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/app_store.png'  title='App Store' /></a>");
                sb.AppendFormat("</td>");
                sb.AppendFormat("</tr> ");
                sb.AppendFormat("</table>");
                //msg.Body = str;
                msg.IsBodyHtml = true;
                string html = sb.ToString();
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(html, new ContentType("text/html"));
                msg.AlternateViews.Add(htmlView);
                //SmtpClient sc = new SmtpClient("sarpstechnologies.com", 25);
                SmtpClient sc = new SmtpClient(ConfigurationManager.AppSettings["smtpHost"], Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]));
                sc.Host = "smtp.gmail.com";
                sc.Port = 587;
                sc.UseDefaultCredentials = true;
                // sc.Host = "sarpstechnologies.com";
                //  sc.Port = 25;
                sc.Credentials = new NetworkCredential(FromEmail, Password);
                sc.EnableSsl = true; // Hide this If u use instead of gmail
                sc.Send(msg);

                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        public async Task<Auction> AddProxybidRecordsAsync(Auction AUC)
        {
            DAL.DbHelper dbhelper = new DAL.DbHelper(Utilities.Cnstr);
            //CurrentAuctionServiceNew CASNew = new CurrentAuctionServiceNew(Utilities.Cnstr);
            Auction bidrecordfinal = new Auction();

            int currentuser = 0;
            List<int> userlist = new List<int>();
            listProxyAuc = await dbhelper.GetProxyInfoAsync(AUC);
            listAuc = await dbhelper.GetAcutionDataAsync(AUC);
            int pricers = 0;
            //curprice = AUC.curprice;
            curprice = AUC.pricers;
            while (listProxyAuc.Count > 0)
            {
                if (listProxyAuc.Count == 1 && AUC.userid == listProxyAuc[0].userid && currentuser == AUC.userid)
                {
                    AUC.recentbid = 1;
                    AUC.currentbid = 1;
                    await dbhelper.UpdateRecentCurrentBidAsync(AUC);
                    return AUC;

                }
                else if (listProxyAuc.Count == 1 && currentuser == listProxyAuc[0].userid)
                {
                    AUC.recentbid = 1;
                    AUC.currentbid = 1;
                    AUC.userid = listProxyAuc[0].userid;
                    await dbhelper.UpdateRecentCurrentBidAsync(AUC);
                    return AUC;
                }

                foreach (var proxy in listProxyAuc)
                {
                    // Added to skip record if same user come again
                    if (currentuser == proxy.userid)
                    {
                        continue;
                    }


                    // Need To Check This
                    if (!userlist.Contains(proxy.userid))
                    {
                        userlist.Add(proxy.userid);
                        aucBidRecord = await dbhelper.getBidUserListAsync(AUC);

                        foreach (var userBid in aucBidRecord)
                        {
                            userBid.currentbid = 0;
                        }
                    }

                    pricers = proxy.ProxyAmt;

                    if (AUC.nextValidBidRs == 0)
                    {
                        AUC.nextValidBidRs = proxy.pricers;
                    }

                    // Need To Do This
                    // Dim val_increase As Decimal = 1.1
                    //If pricelow >= 10000000 Then
                    //    val_increase = 1.05
                    //End If

                    //Changed as last proxy record was missing...........If proxy.bid.ProxyAmt > pricelow * val_increase Then

                    //if (proxy.ProxyAmt >= AUC.nextValidBidRs)
                    //created on 26-01-2020
                    #region
                    if (proxy.ProxyAmt > AUC.nextValidBidRs)
                    #endregion
                    {

                        Auction bidRecord = new Auction();

                        List<Auction> listProxyBid = new List<Auction>();
                        listProxyBid = await dbhelper.GetproxyUserAsync(AUC);
                        if (listProxyBid.Count > 0)
                        {
                            foreach (var proxyuser in listProxyBid)
                            {
                                bidRecord.firstname = proxy.firstname;
                                bidRecord.lastname = proxy.lastname;
                                bidRecord.thumbnail = proxy.thumbnail;
                                bidRecord.productid = proxy.productid;
                                bidRecord.pricers = AUC.pricers;

                                AUC.mailPreprice = AUC.pricers;
                                AUC.mailPrepriceUs = AUC.mailPreprice / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);

                                bidRecord.nickname = proxy.nickname;
                                bidRecord.priceus = bidRecord.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.priceus;

                                if (proxyuser.ProxyAmt == AUC.nextValidBidRs)
                                {
                                    AUC.nextValidBidRs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(AUC.nextValidBidRs, 1).ToString());
                                    bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                                    bidRecord.nextValidBidUs = bidRecord.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.nextValidBidUs;
                                    curprice = bidRecord.nextValidBidRs;
                                }
                                else
                                {
                                    bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                                    bidRecord.nextValidBidUs = bidRecord.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.nextValidBidUs;
                                    curprice = bidRecord.nextValidBidRs;
                                }

                                // bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                                // bidRecord.nextValidBidUs = bidRecord.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.nextValidBidUs;
                                // curprice = bidRecord.nextValidBidRs;
                                //bidRecord.daterec = Now : Already taken in query

                                bidRecord.reference = proxy.reference;
                                bidRecord.anoname = proxy.nickname;
                                bidRecord.username = proxy.username;
                                bidRecord.currentbid = 0;
                                bidRecord.recentbid = 0;
                                bidRecord.userid = proxy.userid;
                                bidRecord.proxy = 1;
                                bidRecord.Online = proxy.Auctionid;
                                AUC.pricers = AUC.nextValidBidRs;
                                bidRecord.nickname = proxy.nickname;
                                bidRecord.isOldUser = proxy.isOldUser;

                                //21_12_2020
                                bidRecord.longitude = proxy.longitude;
                                bidRecord.latitude = proxy.latitude;
                                bidRecord.userLocation = proxy.userLocation;
                                bidRecord.fullAddress = proxy.fullAddress;
                                bidRecord.ipAddress = proxy.ipAddress;
                                bidRecord.name = proxy.name;
                                //

                                // New 
                                bidrecordfinal = bidRecord;

                                AUC.Bidrecordid = await dbhelper.InsertBidRecordAsync(bidRecord);
                            }
                        }
                        else
                        {
                            bidRecord.firstname = proxy.firstname;
                            bidRecord.lastname = proxy.lastname;
                            bidRecord.thumbnail = proxy.thumbnail;
                            bidRecord.productid = proxy.productid;
                            bidRecord.pricers = AUC.pricers;

                            AUC.mailPreprice = AUC.pricers;
                            AUC.mailPrepriceUs = AUC.mailPreprice / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);

                            bidRecord.nickname = proxy.nickname;
                            bidRecord.priceus = bidRecord.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.priceus;

                            bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                            bidRecord.nextValidBidUs = bidRecord.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.nextValidBidUs;
                            curprice = bidRecord.nextValidBidRs;


                            // bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                            // bidRecord.nextValidBidUs = bidRecord.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.nextValidBidUs;
                            // curprice = bidRecord.nextValidBidRs;
                            //bidRecord.daterec = Now : Already taken in query

                            bidRecord.reference = proxy.reference;
                            bidRecord.anoname = proxy.nickname;
                            bidRecord.username = proxy.username;
                            bidRecord.currentbid = 0;
                            bidRecord.recentbid = 0;
                            bidRecord.userid = proxy.userid;
                            bidRecord.proxy = 1;
                            bidRecord.Online = proxy.Auctionid;
                            AUC.pricers = AUC.nextValidBidRs;
                            bidRecord.nickname = proxy.nickname;
                            bidRecord.isOldUser = proxy.isOldUser;

                            //21_12_2020
                            bidRecord.longitude = proxy.longitude;
                            bidRecord.latitude = proxy.latitude;
                            bidRecord.userLocation = proxy.userLocation;
                            bidRecord.fullAddress = proxy.fullAddress;
                            bidRecord.ipAddress = proxy.ipAddress;
                            bidRecord.name = proxy.name;
                            //

                            // New 
                            bidrecordfinal = bidRecord;
                            AUC.Bidrecordid = await dbhelper.InsertBidRecordAsync(bidRecord);
                        }
                        if (AUC.Bidrecordid > 0)
                        {
                            AUC.nextValidBidRs = bidRecord.nextValidBidRs;
                            AUC.nextValidBidUs = AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); //bidRecord.nextValidBidUs;
                            await dbhelper.UpdateAcutionPriceAsync(AUC);
                        }
                        else
                        {
                            //Failed
                        }

                        AUC.nextValidBidRs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(AUC.nextValidBidRs, 1).ToString());
                        currentuser = bidRecord.userid;

                    }
                    //created on 26-01-2020
                    #region
                    else if (proxy.ProxyAmt == AUC.nextValidBidRs)
                    {
                        Auction bidRecord = new Auction();

                        List<Auction> listProxyBid = new List<Auction>();
                        listProxyBid = await dbhelper.GetsameproxyAsync(AUC);
                        foreach (var proxynew in listProxyBid)
                        {
                            bidRecord.firstname = proxynew.firstname;
                            bidRecord.lastname = proxynew.lastname;
                            bidRecord.thumbnail = proxynew.thumbnail;
                            bidRecord.productid = proxynew.productid;
                            bidRecord.pricers = AUC.pricers;
                            bidRecord.nickname = proxynew.nickname;
                            bidRecord.priceus = AUC.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
                            // AUC.priceus;
                            //bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                            //bidRecord.nextValidBidUs = AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);// proxy.ProxyAmtus;
                            //curprice = bidRecord.nextValidBidRs; // bidRecord.nextValidBidRs;
                            //bidRecord.daterec = Now : Already taken in query
                            //created on 12_02_2020
                            bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                            bidRecord.nextValidBidUs = AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);// proxy.ProxyAmtus;
                            curprice = AUC.nextValidBidRs;


                            bidRecord.reference = proxynew.reference;
                            bidRecord.anoname = proxynew.nickname;
                            bidRecord.username = proxynew.username;
                            bidRecord.currentbid = 0;
                            bidRecord.recentbid = 0;
                            bidRecord.userid = proxynew.userid;
                            bidRecord.proxy = 1;
                            bidRecord.Online = AUC.Auctionid;
                            AUC.pricers = AUC.nextValidBidRs;
                            // AUC.pricers = proxy.ProxyAmt;
                            bidRecord.nickname = proxynew.nickname;
                            bidRecord.isOldUser = proxynew.isOldUser;

                            //21_12_2020
                            bidRecord.longitude = proxy.longitude;
                            bidRecord.latitude = proxy.latitude;
                            bidRecord.userLocation = proxy.userLocation;
                            bidRecord.fullAddress = proxy.fullAddress;
                            bidRecord.ipAddress = proxy.ipAddress;
                            bidRecord.name = proxy.name;
                            //

                            bidrecordfinal = bidRecord;
                            AUC.Bidrecordid = await dbhelper.InsertBidRecordAsync(bidRecord);
                        
                        }

                        if (AUC.Bidrecordid > 0)
                        {
                            AUC.nextValidBidRs = bidRecord.nextValidBidRs;
                            AUC.nextValidBidUs = AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); //bidRecord.nextValidBidUs;
                            await dbhelper.UpdateAcutionPriceAsync(AUC);
                        }
                        else
                        {
                            //Failed
                        }



                        AUC.nextValidBidRs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(AUC.nextValidBidRs, 1).ToString());
                        currentuser = bidRecord.userid;
                    }
                    #endregion
                    else if (proxy.ProxyAmt > curprice || proxy.ProxyAmt == curprice)
                    {              
                        //created on 25-01-2020
                        #region
                        Auction bidRecord = new Auction();

                        List<Auction> listProxyBid = new List<Auction>();
                        listProxyBid = await dbhelper.GetUpdatedProxyuserAsync(AUC);
                        if (listProxyBid.Count > 0)
                        {
                            foreach (var proxynew in listProxyBid)
                            {
                                if (proxynew.ProxyAmt > curprice)
                                {
                                    bidRecord.firstname = proxynew.firstname;
                                    bidRecord.lastname = proxynew.lastname;
                                    bidRecord.thumbnail = proxynew.thumbnail;
                                    bidRecord.productid = proxynew.productid;
                                    bidRecord.pricers = AUC.pricers;
                                    bidRecord.nickname = proxynew.nickname;
                                    bidRecord.priceus = bidRecord.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.priceus;
                                    bidRecord.nextValidBidRs = proxynew.ProxyAmt; // Next valid bid
                                    bidRecord.nextValidBidUs = proxynew.ProxyAmt / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);// proxy.ProxyAmtus;
                                    curprice = proxynew.ProxyAmt; // bidRecord.nextValidBidRs;
                                                                  //bidRecord.daterec = Now : Already taken in query

                                    bidRecord.reference = proxynew.reference;
                                    bidRecord.anoname = proxynew.nickname;
                                    bidRecord.username = proxynew.username;
                                    bidRecord.currentbid = 0;
                                    bidRecord.recentbid = 0;
                                    bidRecord.userid = proxynew.userid;
                                    bidRecord.proxy = 1;
                                    bidRecord.Online = proxynew.Auctionid;

                                    AUC.pricers = bidRecord.nextValidBidRs;
                                    bidRecord.nickname = proxynew.nickname;
                                    bidRecord.isOldUser = proxynew.isOldUser;

                                    //21_12_2020
                                    bidRecord.longitude = proxy.longitude;
                                    bidRecord.latitude = proxy.latitude;
                                    bidRecord.userLocation = proxy.userLocation;
                                    bidRecord.fullAddress = proxy.fullAddress;
                                    bidRecord.ipAddress = proxy.ipAddress;
                                    bidRecord.name = proxy.name;
                                    //

                                    bidrecordfinal = bidRecord;
                                    AUC.Bidrecordid = await dbhelper.InsertBidRecordAsync(bidRecord);
                                    proxy.ProxyAmt = proxynew.ProxyAmt;
                                }
                                else if (proxynew.ProxyAmt == curprice)
                                {
                                    bidRecord.firstname = proxynew.firstname;
                                    bidRecord.lastname = proxynew.lastname;
                                    bidRecord.thumbnail = proxynew.thumbnail;
                                    bidRecord.productid = proxynew.productid;
                                    bidRecord.pricers = AUC.pricers;
                                    bidRecord.nickname = proxynew.nickname;
                                    bidRecord.priceus = AUC.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.priceus;
                                    bidRecord.nextValidBidRs = proxynew.ProxyAmt; // Next valid bid
                                    bidRecord.nextValidBidUs = proxynew.ProxyAmt / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);// proxy.ProxyAmtus;
                                    curprice = proxynew.ProxyAmt; // bidRecord.nextValidBidRs;
                                                                  //bidRecord.daterec = Now : Already taken in query

                                    bidRecord.reference = proxynew.reference;
                                    bidRecord.anoname = proxynew.nickname;
                                    bidRecord.username = proxynew.username;
                                    bidRecord.currentbid = 0;
                                    bidRecord.recentbid = 0;
                                    bidRecord.userid = proxynew.userid;
                                    bidRecord.proxy = 1;
                                    bidRecord.Online = proxynew.Auctionid;
                                    AUC.pricers = AUC.nextValidBidRs;
                                    bidRecord.nickname = proxynew.nickname;
                                    bidRecord.isOldUser = proxynew.isOldUser;

                                    //21_12_2020
                                    bidRecord.longitude = proxy.longitude;
                                    bidRecord.latitude = proxy.latitude;
                                    bidRecord.userLocation = proxy.userLocation;
                                    bidRecord.fullAddress = proxy.fullAddress;
                                    bidRecord.ipAddress = proxy.ipAddress;
                                    bidRecord.name = proxy.name;
                                    //

                                    bidrecordfinal = bidRecord;
                                    AUC.Bidrecordid = await dbhelper.InsertBidRecordAsync(bidRecord);
                                    proxy.ProxyAmt = proxynew.ProxyAmt;
                                }
                            }
                        }
                        #endregion


                        if (AUC.Bidrecordid > 0)
                        {
                            AUC.nextValidBidRs = proxy.ProxyAmt;

                            AUC.nextValidBidUs = AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); //proxy.ProxyAmtus;
                            await dbhelper.UpdateAcutionPriceAsync(AUC);
                        }
                        else
                        {
                            //Failed
                        }

                        AUC.nextValidBidRs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(proxy.ProxyAmt, 1).ToString());
                        currentuser = bidRecord.userid;
                    }

                }
                AUC.curprice = curprice;
                listProxyAuc = await dbhelper.GetUpdatedProxyInfoAsync(AUC);

            }


            if (listProxyAuc.Count == 0)
            {

                if (bidrecordfinal?.userid > 0)
                {
                    return bidrecordfinal;
                }
                else
                {
                    return null;
                }


            }
            else
            {
                AUC.nextValidBidRs = 0;
                AUC.recentbid = 1;
                AUC.currentbid = 1;
            }



            return AUC;
        }



        public string sendOutbidNotification(int userid, string subject, string description)
        {
            Notification obj = new Notification();
            DataTable dt = new DataTable();
            Utility util = new Utility();
            dt = util.Display("Exec Proc_Savepushdevice 'GetDeviceIdForCommonNotification'," + userid + "");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    obj.device_id = dr["device_id"].ToString();
                }

            }

            obj.subject = subject;
            obj.description = description;

            using (SqlCommand cmd = new SqlCommand("Proc_PushNotification"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Add");
                cmd.Parameters.AddWithValue("@userid", userid);
                cmd.Parameters.AddWithValue("@device_id", obj.device_id);
                cmd.Parameters.AddWithValue("@subject", obj.subject);
                cmd.Parameters.AddWithValue("@description", obj.description);

                if (util.Execute(cmd))
                {

                    try
                    {
                        var applicationID = "AAAAa-wlbPw:APA91bEBK31iJ8EdA6cElzIxaPfyTgfS77poQUuIkc1qbJezwifX1Q6hnLjED6IycmhyM5eZGwPdh-XJCvUSX8-UcOCLfd-rB_oThZhQ8_sZqOOTOOqSsGsdMjCGFA5-DgxGvUGlHc9j";

                        //var senderId = "57-------55";

                        string deviceId = obj.device_id;

                        WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

                        tRequest.Method = "post";

                        tRequest.ContentType = "application/json";

                        var data = new

                        {

                            to = deviceId,

                            notification = new

                            {

                                body = obj.description,

                                title = obj.subject,

                                //shortdesc = shortdescription
                                //icon = "myicon"

                            }
                        };

                        var serializer = new JavaScriptSerializer();

                        var json = serializer.Serialize(data);

                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);


                        tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                        //tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                        tRequest.ContentLength = byteArray.Length;


                        using (Stream dataStream = tRequest.GetRequestStream())
                        {

                            dataStream.Write(byteArray, 0, byteArray.Length);


                            using (WebResponse tResponse = tRequest.GetResponse())
                            {

                                using (Stream dataStreamResponse = tResponse.GetResponseStream())
                                {

                                    using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                    {

                                        String sResponseFromServer = tReader.ReadToEnd();

                                        string str = sResponseFromServer;
                                        return "Success";

                                    }
                                }
                            }
                        }
                    }

                    catch (Exception ex)
                    {

                        string str = ex.Message;
                        return str;

                    }
                }
            }
            return null;
        }

    }
}