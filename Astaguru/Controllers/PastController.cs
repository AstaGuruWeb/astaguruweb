﻿using Astaguru.Models;
using Astaguru.Services.UserService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Astaguru.Controllers
{
    public class PastController : Controller
    {
        Auction AUC = new Auction();
        CurrentAuctionService CAS = new CurrentAuctionService();
        //
        // GET: /Past/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PastAuctions()
        {
            string title = "PastAuctions";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/PastAuctions.cshtml");
        }

        //public ActionResult PastAuctionResult()
        //{
        //    Session["SubqueryPast"] = null;
        //    Session["FilterArtistPast"] = null;
        //    Session["FilterMediumPast"] = null;
        //    Session["FilterDeptPast"] = null;

        //    return View("~/Views/User/PastAuctionResults.cshtml");
        //}

        public ActionResult PastAuctionResult(string strurl)
        {
            PastAuctionService ps = new PastAuctionService();
            Session["SubqueryPast"] = null;
            Session["FilterArtistPast"] = null;
            Session["FilterMediumPast"] = null;
            Session["FilterDeptPast"] = null;

            TempData["Auctionid"] = ps.getPastauctionid(strurl);
            TempData["slug"] = strurl;

            return View("~/Views/User/PastAuctionResults.cshtml");
        }

        public ActionResult FilterPastAuctionResult()
        {
            return View("~/Views/User/PastAuctionResults.cshtml");
        }

        public ActionResult listPastAuctionResult(string strurl)
        {
            PastAuctionService ps = new PastAuctionService();
            Session["SubqueryPast"] = null;
            Session["FilterArtistPast"] = null;
            Session["FilterMediumPast"] = null;
            Session["FilterDeptPast"] = null;

            TempData["Auctionid"] = ps.getPastauctionid(strurl);
            TempData["slug"] = strurl;

            return View("~/Views/User/listPastAuctionResult.cshtml");
        }

        public ActionResult listFilterPastAuctionResult()
        {

            return View("~/Views/User/listPastAuctionResult.cshtml");
        }



        

        public ActionResult PastDetails(int productid)
        {
            AUC = CAS.GetAuctionDetail(productid);
            return View("~/Views/User/PastDetails.cshtml", AUC);
        }

        public ActionResult AuctionAnalysis(int Auctionid)
        {
            return View("~/Views/User/AuctionAnalysis.cshtml");
        }

        #region get MetaTags

        public static string UpdateMetaDetails(string pageUrl)
        {
            Utility util = new Utility();
            //--- StringBuilder object to store MetaTags information.
            StringBuilder sbMetaTags = new StringBuilder();

            //--Step1 Get data from database.

            DataTable dt = new DataTable();
            dt = util.Display("exec Proc_MetaTag 'bindMetatTag',0,'" + pageUrl.ToString() + "'");
            if (dt.Rows.Count > 0)
            {

                //---- Step2 In this step we will add <title> tag to our StringBuilder Object.
                sbMetaTags.Append("<title>" + dt.Rows[0]["Title"].ToString() + "</title>");

                //---- Step3 In this step we will add "Meta Description" to our StringBuilder Object.
                sbMetaTags.Append("<meta name='description' content='" + dt.Rows[0]["MetaDescription"].ToString() + "' >");

                //---- Step4 In this step we will add "Meta Keywords" to our StringBuilder Object.
                sbMetaTags.Append("<meta name='keywords' content ='" + dt.Rows[0]["MetaKeywords"].ToString() + "'>");
            }
            return sbMetaTags.ToString();
        }

        #endregion
    }
}