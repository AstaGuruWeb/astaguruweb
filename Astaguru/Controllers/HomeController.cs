﻿using Astaguru.Models;
using Astaguru.Services.UserService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Astaguru.Controllers
{

    public class HomeController : Controller
    {
        Utility util = new Utility();

        Auction AUC = new Auction();
        User User = new User();
        UserService US = new UserService();
        CurrentAuctionService CAS = new CurrentAuctionService();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Home()
        {
            if (Session["GetCurrency"] == null)
            {
                Session["GetCurrency"] = 1;
            }

            string title = "/";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/Home.cshtml");
        }

        public ActionResult AboutUs()
        {

            string title = "AboutUs";
            ViewBag.MetaTags = UpdateMetaDetails(title);
            ViewBag.Description = CMS(1);
            return View("~/Views/User/aboutUs.cshtml");
        }

        public ActionResult astaguruAuction()
        {
            return View("~/Views/User/astaguru-auction.cshtml");
        }

        public ActionResult Services()
        {
            string title = "Service";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            int pageid = 1;
            ViewBag.Services = GetPageCMSMaster(pageid);
            return View("~/Views/User/services.cshtml");
        }

        public ActionResult TermsAndConditions()
        {
            string title = "TermsAndConditions";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            int pageid = 2;
            ViewBag.TermsAndConditions = GetPageCMSMaster(pageid);
            return View("~/Views/User/TermsAndConditions.cshtml");
        }


        #region
        public ActionResult Careers()
        {
            string title = "Careers";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            ViewBag.CareerDesc = CMS(2);
            ViewBag.CurrentVacancy = CurrentVacancy();
            return View("~/Views/User/Careers.cshtml");
        }

        [HttpPost]
        public ActionResult Careers(Careers obj, HttpPostedFileBase[] Resume)
        {
            try
            {
                ModelState.Remove("Resume");
                if (ModelState.IsValid)
                {
                    string VirtualFile = "~/Content/uploads/Resume/";
                    string Img = string.Empty;
                    Stream inputStr = null;
                    if (Resume != null)
                    {
                        foreach (HttpPostedFileBase file in Resume)
                        {
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidResumeFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only doc/docx and png pdf files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;

                                    return View("~/Views/User/Careers.cshtml", obj);
                                }
                                else
                                {
                                    var fileName = "" + obj.Name + "_Resume-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    inputStr = file.InputStream;
                                    Img += fileName + ",";

                                }
                            }
                        }

                        if (Img != "")
                        {
                            obj.Resume = Img.Substring(0, Img.Length - 1);
                        }
                    }

                    if (obj.Id == 0)
                    {
                        if (SaveCareer(obj, inputStr))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Careers");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Careers");
                        }
                    }
                }
                return View("~/Views/User/Careers.cshtml", obj);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("Careers");
            }

        }


        public bool SaveCareer(Careers objApplyForJob, Stream input)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_Career"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@fullname", objApplyForJob.Name);
                cmd.Parameters.AddWithValue("@emailaddress", objApplyForJob.Email);
                cmd.Parameters.AddWithValue("@path", objApplyForJob.Resume);
                cmd.Parameters.AddWithValue("@message", objApplyForJob.Message);
                cmd.Parameters.AddWithValue("@jobtitle", objApplyForJob.JobTitle);
                cmd.Parameters.AddWithValue("@hearaboutus", objApplyForJob.aboutus);

                if (util.Execute(cmd))
                {
                    StringBuilder sb = new StringBuilder();

                    sb.Append("<p>Hi " + objApplyForJob.Name + ",</p>");
                    sb.Append("<p>Thank you for applying on www.astaguru.com. Your details are mentioned below.</p>");
                    sb.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ddd; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
                    sb.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#2f72b4\"><strong><font color=\"#FFFFFF\">Carrer Enquiry</font></strong></td></tr>");

                    sb.AppendFormat("<tr><td width=\"200px\"><strong>Name : </strong></td><td> " + objApplyForJob.Name + " </td></tr>");
                    sb.AppendFormat("<tr> <td><strong>Email : </strong></td> <td> " + objApplyForJob.Email + " </td></tr>");
                    sb.AppendFormat("<tr> <td><strong>Job title : </strong></td> <td> " + objApplyForJob.JobTitle + " </td></tr>");
                    sb.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Source : </strong></td> <td> " + objApplyForJob.aboutus + " </td> </tr>");

                    sb.Append("</table>");
                    sb.Append("<p>" + objApplyForJob.Message + "</p>");

                    string UserEmail = objApplyForJob.Email;
                    string ToEmailid = ConfigurationManager.AppSettings["CareerEmail"].ToString();
                    String[] emailid = new String[] { ToEmailid };

                    //var path1 = "~/Content/uploads/Resume/" + objApplyForJob.Resume;
                    //var path = Server.MapPath(path1);
                    util.SendEmailcareer(sb.ToString(), emailid, "" + objApplyForJob.Name + " - Carrer Enquiry for www.astaguru.com", objApplyForJob.Resume, input, UserEmail);

                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public string CurrentVacancy()
        {
            string response = "";
            StringBuilder strbuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_Current_Vacancy 'get'");
            int Count = 1;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (Count == 1)
                    {
                        strbuild.Append("<div class='card'>");
                        strbuild.Append("<div class='card-header' id='headingOne'>");
                        strbuild.Append("<h5 class='mb-0'>");
                        strbuild.Append("<button class='btn btn-link' type='button' data-toggle='collapse' data-target='#" + Generateslg(dr["Job_Title"].ToString()) + "' aria-expanded='true' aria-controls='collapseOne'>");
                        strbuild.Append(dr["Job_Title"].ToString());
                        strbuild.Append("</button>");
                        strbuild.Append("</h5>");
                        strbuild.Append("</div>");
                        strbuild.Append("<div id=" + Generateslg(dr["Job_Title"].ToString()) + " class='collapse show' aria-labelledby='headingOne' data-parent='#accordionExample'>");
                        strbuild.Append("<div class='card-body'>");
                        strbuild.Append("<h5 class='sbold black'>Job Title:</h5>");
                        strbuild.Append("<p>" + dr["Job_Title"].ToString() + "</p>");
                        strbuild.Append("<h5 class='sbold black'>Business Unit:</h5>");
                        strbuild.Append("<p>" + dr["Business_Unit"].ToString() + "</p>");
                        strbuild.Append("<h5 class='sbold black'>Job Responsibility:</h5>");
                        strbuild.Append("<p>" + dr["Job_Responsibility"].ToString() + "</p>");
                        strbuild.Append("<h5 class='sbold black'>Responsibilities:</h5>");
                        strbuild.Append(dr["Responsibilities"].ToString());
                        strbuild.Append("<h5 class='sbold black'>Functional Skills:</h5>");
                        strbuild.Append(dr["Functional_Skills"].ToString());
                        strbuild.Append("<h5 class='sbold black'>Technical Skills:</h5>");
                        strbuild.Append(dr["Technical_skills"].ToString());
                        strbuild.Append("<h5 class='sbold black'>Required Qualification:</h5>");
                        strbuild.Append("<p>" + dr["Qualification"].ToString() + "</p>");
                        strbuild.Append("<h5 class='sbold black'>Salary:</h5>");
                        strbuild.Append("<p>" + dr["Salary"].ToString() + "</p>");
                        strbuild.Append("<h5 class='sbold black'>Experience:</h5>");
                        strbuild.Append("<p>" + dr["Experience"].ToString() + "</p>");
                        strbuild.Append("<a href='#apply_now_form' class='a_scroll btn_grey_dark mobile_only extra-margin-top-20'><span>Apply Now</span></a>");
                        strbuild.Append("</div>");
                        strbuild.Append("</div>");
                        strbuild.Append("</div>");
                    }
                    else
                    {
                        strbuild.Append("<div class='card'>");
                        strbuild.Append("<div class='card-header' id='headingTwo'>");
                        strbuild.Append("<h5 class='mb-0'>");
                        strbuild.Append("<button class='btn btn-link collapsed' type='button' data-toggle='collapse' data-target='#" + Generateslg(dr["Job_Title"].ToString()) + "' aria-expanded='false' aria-controls='collapseTwo'>");
                        strbuild.Append(dr["Job_Title"].ToString());
                        strbuild.Append("</button>");
                        strbuild.Append("</h5>");
                        strbuild.Append("</div>");
                        strbuild.Append("<div id=" + Generateslg(dr["Job_Title"].ToString()) + " class='collapse' aria-labelledby='headingTwo' data-parent='#accordionExample'>");
                        strbuild.Append("<div class='card-body'>");
                        strbuild.Append("<h5 class='sbold black'>Job Title:</h5>");
                        strbuild.Append("<p>" + dr["Job_Title"].ToString() + "</p>");
                        strbuild.Append("<h5 class='sbold black'>Business Unit:</h5>");
                        strbuild.Append("<p>" + dr["Business_Unit"].ToString() + "</p>");
                        strbuild.Append("<h5 class='sbold black'>Job Responsibility:</h5>");
                        strbuild.Append("<p>" + dr["Job_Responsibility"].ToString() + "</p>");
                        strbuild.Append("<h5 class='sbold black'>Responsibilities:</h5>");
                        strbuild.Append(dr["Responsibilities"].ToString());
                        strbuild.Append("<h5 class='sbold black'>Functional Skills:</h5>");
                        strbuild.Append(dr["Functional_Skills"].ToString());
                        strbuild.Append("<h5 class='sbold black'>Technical Skills:</h5>");
                        strbuild.Append(dr["Technical_skills"].ToString());
                        strbuild.Append("<h5 class='sbold black'>Required Qualification:</h5>");
                        strbuild.Append("<p>" + dr["Qualification"].ToString() + "</p>");
                        strbuild.Append("<h5 class='sbold black'>Salary:</h5>");
                        strbuild.Append("<p>" + dr["Salary"].ToString() + "</p>");
                        strbuild.Append("<h5 class='sbold black'>Experience:</h5>");
                        strbuild.Append("<p>" + dr["Experience"].ToString() + "</p>");
                        strbuild.Append("<a href='#apply_now_form' class='a_scroll btn_grey_dark mobile_only extra-margin-top-20'><span>Apply Now</span></a>");
                        strbuild.Append("</div>");
                        strbuild.Append("</div>");
                        strbuild.Append("</div>");
                    }

                    Count++;
                }

            }

            response = strbuild.ToString();
            return response;
        }


        #endregion

        public ActionResult Departments()
        {
            string title = "Departments";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/departments.cshtml");
        }

        public ActionResult Policy()
        {
            string title = "Home/Policy";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            ViewBag.Policy = CMS(4);
            return View("~/Views/User/privacyPolicy.cshtml");
        }

        public ActionResult DepartmentDetail()
        {
            string title = "Home/DepartmentDetail";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/departmentDetail.cshtml");
        }
        public ActionResult valuation()
        {
            string title = "Valuation";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            ViewBag.Valuation = CMS(3);
            return View("~/Views/User/valuation.cshtml");
        }

        public ActionResult howtobuy()
        {

            string title = "howtobuy";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            int Pageid = 3;
            ViewBag.howtobuy = GetPageCMSMaster(Pageid);
            return View("~/Views/User/howtobuy.cshtml");
        }

        public ActionResult howToSell()
        {
            string title = "HowToSell";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            int Pageid = 4;
            ViewBag.howToSell = GetPageCMSMaster(Pageid);
            return View("~/Views/User/howToSell.cshtml");
        }

        public ActionResult contactUs()
        {
            string title = "ContactUs";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/contactUs.cshtml");
        }

        [HttpPost]
        public ActionResult contactUs(ContactUsForm obj)
        {
            try
            {
 
                if (ModelState.IsValid)
                {
                    
                    if (obj.Id == 0)
                    {
                        if (SaveContactUs(obj))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("contactUs");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("contactUs");
                        }
                    }
                }
                return View("~/Views/User/contactUs.cshtml", obj);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("contactUs");
            }
        }

        public bool SaveContactUs(ContactUsForm obj)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_ContactUs"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Name", obj.Name);
                cmd.Parameters.AddWithValue("@Email", obj.Email);
                cmd.Parameters.AddWithValue("@Phone", obj.Phone);
                cmd.Parameters.AddWithValue("@Queries", obj.Queries);

                if (util.Execute(cmd))
                {
                    string str = string.Empty;
                    str = "<table width='650' align='center' cellpadding='0' cellspacing='0' style='font-family: 'Montserrat', sans-serif; line-height:24px ; font-size:14px; font-weight:normal; border:1px solid #ebebeb;'>";
                    str += "<tr bgcolor='#282831'>";
                    str += "<td width='220' style='padding:20px 0 20px 20px'><a href='https://astaguru.com/'><img src='https://www.astaguru.com/content/images/Email/logo.png' width='175' height='44' /></a></td>";
                    str += "<td colspan='2'>";
                    str += "<a href='https://www.astaguru.com/LiveAuction' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Live Auctions</a>";
                    str += "<a href='https://www.astaguru.com/UpcomingAuctions' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Upcoming Auctions</a>";
                    str += "<a href='https://www.astaguru.com/PastAuctions' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Past Auctions</a>";
                    str += "</td>";
                    str += "</tr>";
                    str += "<td colspan='3' style='position:relative;'><a href='https://www.astaguru.com/ContactUs'><img src='https://www.astaguru.com/content/images/Email/banner_ContactUs.jpg'  width='650'/></a> ";
                    //str += "<a href='https://www.astaguru.com/ContactUs' style='background:#c1aa5c; color:#fff; border:1px solid #fff; border-radius:3px; position:absolute; text-decoration: none; font-size: 15px; font-weight: normal;  text-align: center; padding: 5px 10px; left: 38%; bottom: 35px; transform:all .3s ease-in;'>Verify your Account</a>";
                    str += "</td>";
                    str += "</tr>";
                    str += "<tr>";
                    str += "<td  colspan='3' style='padding:30px;'>";
                    str += "<br>Name:" + obj.Name;
                    str += "<br>Emailid:" + obj.Email;
                    str += "<br>Phone:" + obj.Phone;
                    str += "<br>Queries:" + obj.Queries;
                    str += "</td>";
                    str += "</tr> ";
                    str += "<tr  bgcolor='#ebebeb' >";
                    str += "<td  width='220' style='padding:20px 0 20px 30px'><img src='https://www.astaguru.com/content/images/Email/logo_black.png' width='122' height='31' /></td>";
                    str += "<td align='center'>";
                    str += "<p style='font-size:12px; text-align:center; margin-bottom:5px; '><span style='border-bottom:1px solid #000;'>Follow Us</span></p>";
                    str += "<a href='https://www.facebook.com/Astaguru-Auction-House-375561749218131/?fref=ts' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/facebook_icon.png'  title='Facebook' /></a>";
                    str += "<a href='https://in.pinterest.com/astaguru/' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/pinterest_icon.png' title='Pinterest' /></a>";
                    str += "<a href='https://twitter.com/astagurutweets' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/twitter_icon.png'  title='Twitter' /></a>";
                    str += "<a href='https://www.instagram.com/astaguru/' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/instagram_icon.png'  title='Instagram' /></a>";
                    str += "<a href='https://www.youtube.com/channel/UCmTqSUMAHV5l0mACoK72t7g' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/youtube_icon.png'  title='Youtube' /></a>";
                    str += "</td>";
                    str += "<td align='center'>";
                    str += "<p style='font-size:12px; text-align:center; margin-bottom:5px; '><span style='border-bottom:1px solid #000;'>Download Our App</span></p>";
                    str += "<a href='https://play.google.com/store/apps/details?id=com.astaguru&hl=en' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/google_play.png'  title='Google Play' /></a>";
                    str += "<a href='https://apps.apple.com/app/id1483891832' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/app_store.png'  title='App Store' /></a>";
                    str += "</td>";
                    str += "</tr>";
                    str += "</table>";

                    string ToEmailid = ConfigurationManager.AppSettings["ContactUs"].ToString();
                    String[] emailid = new String[] { ToEmailid };
                    util.SendEmail(str.ToString(), emailid, "Inquiry from contact us page","", null);

                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }


        public ActionResult bidhistory(int productid)
        {
            AUC = CAS.GetCurrentAuctionDetail(productid);
            return View("~/Views/User/bidhistory.cshtml", AUC);
        }

        public ActionResult CurrentAuction()
        {

            Session["Subquery"] = null;
            Session["FilterArtist"] = null;
            Session["FilterMedium"] = null;
            Session["FilterDept"] = null;

            if (Session["userid"] == null)
            {
                Session["userid"] = -1;
            }

            string title = "LiveAuction";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/CurrentAuction.cshtml");


        }


        public ActionResult FilterCurrentAuction()
        {
            if (Session["userid"] == null)
            {
                Session["userid"] = -1;
            }

            string title = "Home/CurrentAuction";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/CurrentAuction.cshtml");
        }

        public ActionResult ListCurrentAuction()
        {

            Session["Subquery"] = null;
            Session["FilterArtist"] = null;
            Session["FilterMedium"] = null;
            Session["FilterDept"] = null;


            string title = "Home/ListCurrentAuction";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/listCurrentAuction.cshtml");
        }

        public ActionResult ListFilterCurrentAuction()
        {
            string title = "Home/ListCurrentAuction";
            ViewBag.MetaTags = UpdateMetaDetails(title);
            return View("~/Views/User/listCurrentAuction.cshtml");
        }


        public ActionResult LotDetails(int productid)
        {
            string title = "Home/LotDetails";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            AUC = CAS.GetCurrentAuctionDetail(productid);
            return View("~/Views/User/lotDetails.cshtml", AUC);
        }

        public ActionResult MyAuctionGallary()
        {
            string title = "Home/MyAuctionGallary";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/myAuctionGallary.cshtml");
        }

        public ActionResult closingSchedule()
        {
            return View("~/Views/User/closingSchedule.cshtml");
        }

        public ActionResult billingAddress(int productid)
        {
            int userid = Convert.ToInt32(Session["userid"]);
            if (userid > 0)
            {
                User = US.GetBillingAddress(userid);
            }
            else
            {

            }

            return View("~/Views/User/billingAddress.cshtml", User);
        }
        public ActionResult liveAuctionReview()
        {
            string title = "Home/liveAuctionReview";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/liveAuctionReview.cshtml");
        }

        public ActionResult listLiveAuctionReview()
        {
            string title = "Home/listLiveAuctionReview";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/listLiveAuctionReview.cshtml");
        }

        public ActionResult liveAuctionHighValue()
        {
            string title = "Home/liveAuctionHighValue";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/liveAuctionHighValueBid.cshtml");
        }

        public ActionResult listLiveAuctionHighValueBid()
        {
            string title = "Home/listLiveAuctionHighValueBid";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/listLiveAuctionHighValueBid.cshtml");
        }


        public ActionResult liveAuctionMostPopular()
        {
            string title = "Home/liveAuctionMostPopular";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/liveAuctionMostPopular.cshtml");
        }

        public ActionResult listLiveMostPopular()
        {
            string title = "Home/listLiveMostPopular";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/listLiveMostPopular.cshtml");
        }


        public ActionResult liveAuctionClosingLots()
        {
            string title = "Home/liveAuctionClosingLots";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/liveAuctionClosingLots.cshtml");
        }

        public ActionResult listClosingLots()
        {
            string title = "Home/listClosingLots";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/listClosingLots.cshtml");
        }

        public ActionResult ListMyAuctionGallary()
        {
            string title = "Home/ListMyAuctionGallary";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/ListMyAuctionGallary.cshtml");
        }

        public ActionResult searchArtist()
        {
            string title = "Home/searchArtist";
            ViewBag.category = "";
            ViewBag.MetaTags = UpdateMetaDetails(title);



            return View("~/Views/User/searchArtist.cshtml");
        }

        [HttpPost]
        public ActionResult searchArtist(Search objsearch)
        {
            ViewBag.category = objsearch.SearchType;

            objsearch.artistid = int.Parse(TempData["artistid"].ToString());

            TempData.Keep();
            return View("~/Views/User/searchArtist.cshtml", objsearch);
        }



        public ActionResult listSearchArtist()
        {
            string title = "Home/listSearchArtist";
            ViewBag.MetaTags = UpdateMetaDetails(title);
            ViewBag.category = "";

            return View("~/Views/User/listSearchArtist.cshtml");
        }


        [HttpPost]
        public ActionResult listSearchArtist(Search objsearch)
        {
            ViewBag.category = objsearch.SearchType;

            objsearch.artistid = int.Parse(TempData["artistid"].ToString());

            TempData.Keep();

            return View("~/Views/User/listSearchArtist.cshtml", objsearch);
        }

        public ActionResult registration()
        {
            string title = "Home/registration";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/registration.cshtml");
        }

        public ActionResult recordPriceArtwork()
        {
            string title = "Home/recordPriceArtwork";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/recordPriceArtwork.cshtml");
        }

        public ActionResult listRecordPriceArtwork()
        {
            string title = "Home/listRecordPriceArtwork";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/listRecordPriceArtwork.cshtml");
        }

        public ActionResult myProfile(int userid)
        {
            //string title = "Home/myProfile";
            //ViewBag.MetaTags = UpdateMetaDetails(title);

            User = US.GetProfileData(userid);
            //return View("~/Views/User/myProfile_old.cshtml", User);

            return View("~/Views/User/myProfile_New.cshtml", User);
        }

        [HttpGet]
        public ActionResult emailVerification(string activationcode)
        {
            string title = "Home/emailVerification";
            ViewBag.MetaTags = UpdateMetaDetails(title);
            return View("~/Views/User/emailVerification.cshtml");
        }

        public ActionResult listMyPurchase()
        {
            string title = "Home/listMyPurchase";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/listMyPurchase.cshtml");
        }

        public ActionResult MyPurchase()
        {
            string title = "Home/MyPurchase";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/MyPurchase.cshtml");
        }

        public ActionResult deptArtist()
        {
            string title = "Home/deptArtist";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/deptArtist.cshtml");
        }
        public ActionResult deptBooks()
        {
            string title = "Departments/RareBooksAndManuscripts";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/deptBooks.cshtml");
        }

        public ActionResult deptClassic()
        {
            string title = "Departments/Vintage&ClassicCars";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/deptClassic.cshtml");
        }

        public ActionResult deptEstate()
        {
            string title = "Departments/EstateSales";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/deptEstate.cshtml");
        }
        public ActionResult deptFine()
        {
            string title = "Departments/FineWritingInstruments";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/deptFine.cshtml");
        }

        public ActionResult deptFurniture()
        {
            string title = "Departments/FurnitureAndDecorativeArt";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/deptFurniture.cshtml");
        }

        public ActionResult deptIndian()
        {
            string title = "Departments/IndianAntiques";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/deptIndian.cshtml");
        }

        public ActionResult deptModernArt()
        {
            string title = "Departments/IndianModernArt";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/deptModernArt.cshtml");
        }

        public ActionResult deptJewellery()
        {
            string title = "Departments/JewelleryAndSilver";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/deptJewellery.cshtml");
        }

        public ActionResult deptMemorabilia()
        {
            string title = "Departments/Memorabilia";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/deptMemorabilia.cshtml");
        }

        public ActionResult deptNumismatic()
        {

            string title = "Departments/NumismaticAndPhilatelic";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/deptNumismatic.cshtml");
        }

        public ActionResult deptPrints()
        {
            string title = "Home/deptPrints";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/deptPrints.cshtml");
        }

        public ActionResult deptScience()
        {
            string title = "Departments/ScienceAndNaturalHistory";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/deptScience.cshtml");
        }
        public ActionResult deptSoutheast()
        {
            string title = "SouthEastAsianContemporaryArt";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/deptSoutheast.cshtml");
        }

        public ActionResult deptTexilesView1()
        {
            string title = "Departments/Textiles";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/deptTexilesView1.cshtml");
        }

        public ActionResult Timepieces()
        {
            string title = "Departments/Timepieces";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            return View("~/Views/User/Timepieces.cshtml");
        }

        #region video
        public ActionResult video()
        {
            string title = "Home/video";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            Video obj = new Video();
            obj.CategoryList = BindcategoryforVideo(obj);
            return View("~/Views/User/video.cshtml", obj);
        }

        [HttpPost]
        public ActionResult video(Video obj)
        {

            if (obj.categoryid != 0)
            {
                //ViewBag.categoryid = Catid[Catid.Length - 1];
                ViewBag.category = obj.categoryid;


            }


            obj.CategoryList = BindcategoryforVideo(obj);

            return View("~/Views/User/video.cshtml", obj);


        }

        public List<SelectListItem> BindcategoryforVideo(Video obj)
        {
            DataTable dt = new DataTable();
            dt = util.Display("Execute Proc_video 'Getcategoryforvideo'");

            List<SelectListItem> objList = new List<SelectListItem>();
            if (dt.Rows.Count > 0)
            {

                foreach (DataRow dr in dt.Rows)
                {
                    SelectListItem list = new SelectListItem();
                    list.Text = dr["category"].ToString();
                    list.Value = dr["categoryid"].ToString();

                    if (obj.categoryid == int.Parse(dr["categoryid"].ToString()))
                    {
                        list.Selected = true;
                    }

                    objList.Add(list);
                }

            }
            return objList.ToList();
        }

        #endregion

        #region media
        public ActionResult media()
        {

            string title = "Home/media";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            StringBuilder strbuild = new StringBuilder();

            DataSet ds = new DataSet();
            ds = util.Display1("Exec Proc_MediaNews 'bindMediaNews'");
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {

                        strbuild.Append("<div class='row py-3 px-1 bg-light mb-5 aos-item' data-aos='fade-up'>");
                        strbuild.Append("<div class='col-lg-4 imageData'>");
                        if (dr["Type"].ToString() == "Image")
                        {
                            strbuild.Append("<img src='/Content/uploads/MediaNews/" + dr["Image"].ToString() + "' class='img-fluid'>");
                        }
                        else
                        {
                            strbuild.Append("<iframe src=" + dr["VideoUrl"].ToString() + " frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>");
                        }

                        strbuild.Append("</div>");
                        strbuild.Append("<div class='col-lg-8 matter'>");
                        strbuild.Append("<h2>" + dr["Title"].ToString() + "</h2>");
                        strbuild.Append("<div class='date'><i class='fa fa-clock-o' aria-hidden='true'></i> " + dr["Date"].ToString() + "  /  " + dr["CreatedBy"].ToString() + " </div>");
                        strbuild.Append(dr["Description"].ToString());
                        strbuild.Append("<a href='/mediadetail/" + dr["Slug"].ToString() + "' class='readMore'>Read More</a>");

                        strbuild.Append("</div>");
                        strbuild.Append("</div>");




                    }
                }
            }



            ViewBag.media = strbuild.ToString();
            return View("~/Views/User/media.cshtml");
        }

        [HttpPost]
        public ActionResult media(FormCollection fobj)
        {
            string title = "Home/media";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            string value = fobj["hiddenSearchvalue"].ToString();

            StringBuilder strbuild = new StringBuilder();

            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_MediaNews 'bindMediaNewsbySearch',0,'','','','','','','','','" + value + "'");
            if (dt.Rows.Count > 0)
            {


                foreach (DataRow dr in dt.Rows)
                {

                    strbuild.Append("<div class='row py-3 px-1 bg-light mb-5 aos-item' data-aos='fade-up'>");
                    strbuild.Append("<div class='col-lg-4 imageData'>");
                    if (dr["Type"].ToString() == "Image")
                    {
                        strbuild.Append("<img src='/Content/uploads/MediaNews/" + dr["Image"].ToString() + "' class='img-fluid'>");
                    }
                    else
                    {
                        strbuild.Append("<iframe src=" + dr["VideoUrl"].ToString() + " frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>");
                    }

                    strbuild.Append("</div>");
                    strbuild.Append("<div class='col-lg-8 matter'>");
                    strbuild.Append("<h2>" + dr["Title"].ToString() + "</h2>");
                    strbuild.Append("<div class='date'><i class='fa fa-clock-o' aria-hidden='true'></i> " + dr["Date"].ToString() + "  /  " + dr["CreatedBy"].ToString() + " </div>");
                    strbuild.Append(dr["Description"].ToString());
                    strbuild.Append("<a href='/mediadetail/" + dr["Slug"].ToString() + "' class='readMore'>Read More</a>");

                    strbuild.Append("</div>");
                    strbuild.Append("</div>");


                }

            }

            else
            {
                strbuild.Append("<h3 class='text-center'>");
                strbuild.Append("No data found");
                strbuild.Append("<h3>");
            }



            ViewBag.media = strbuild.ToString();
            return View("~/Views/User/media.cshtml");

        }


        public ActionResult mediadetail(string strNews)
        {
            string title = "Home/mediadetail";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            var objNews = Bindmediadetail(strNews);
            ViewBag.mediadetail = objNews["NewsDetail"].ToString();
            return View("~/Views/User/mediadetail.cshtml");
        }

        public Dictionary<string, string> Bindmediadetail(string strNews)
        {
            Dictionary<string, string> objNews = new Dictionary<string, string>();

            StringBuilder strBuild = new StringBuilder();
            //StringBuilder strType = new StringBuilder();

            StringBuilder strMetaTitle = new StringBuilder();



            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_MediaNews 'bindMediaNewsDeatils',0,'','','','','','" + strNews + "'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    strMetaTitle.Append(dr["Slug"].ToString());

                    strBuild.Append("<div class='container pt-4'>");
                    strBuild.Append("<div class='row align-items-center justify-content-center mb-4'>");
                    strBuild.Append("<div class='col-12  col-lg-10 mb-4'>");
                    strBuild.Append("<h1 class='bigHeading'>");
                    strBuild.Append(dr["Title"].ToString());
                    strBuild.Append("</h1>");
                    strBuild.Append("</div>");
                    strBuild.Append("<div class='col-12 col-lg-10'>");
                    strBuild.Append("<div class='author'>By <span>" + dr["CreatedBy"].ToString() + "</span></div>");
                    strBuild.Append("<div class='date'><i class='fa fa-clock-o' aria-hidden='true'></i>" + dr["Date"].ToString() + "</div>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                    strBuild.Append("<div class='border-bottom-0 border mb-5'></div>");
                    strBuild.Append("<div class='container'>");
                    strBuild.Append("<div class='row justify-content-center mb-5'>");
                    strBuild.Append("<div class='col-12 col-lg-10 text-center'>");
                    strBuild.Append("<div class='mb-4 blogBanner'>");
                    if (dr["Type"].ToString() == "Image")
                    {
                        strBuild.Append("<img src='/Content/uploads/MediaNews/" + dr["Image"].ToString() + "'>");
                    }
                    else
                    {
                        strBuild.Append("<iframe src=" + dr["VideoUrl"].ToString() + " frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>");
                    }
                    //strBuild.Append("<img src='images/blog/blog_banner.jpg'>");
                    strBuild.Append("</div>");
                    strBuild.Append(dr["Description"].ToString());
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                    strBuild.Append("<div class='border-bottom-0 border mb-5'></div>");


                    strBuild.Append("<div class='row mb-5 justify-content-center'>");
                    strBuild.Append("<div class='col-12 col-lg-10 social_share'>");
                    //strBuild.Append("<a href='#' class='share'><i class='fa fa-share-alt' aria-hidden='true'></i></a>");
                    //strBuild.Append("<a class='fb' target='_blank' href='http://www.facebook.com/sharer/sharer.php?u="+ Request.Url.ToString()+"'><i class='fa fa-facebook' aria-hidden='true'></i></a>");
                    strBuild.Append("<a href='#' class='fb'><i class='fa fa-facebook' aria-hidden='true'></i></a>");
                    strBuild.Append("<a href='#' class='twit'><i class='fa fa-twitter' aria-hidden='true'></i></a>");
                    strBuild.Append("<a href='#' class='pinterest'><i class='fa fa-pinterest-p' aria-hidden='true'></i></a>");
                    strBuild.Append("<a href='#' class='whatsApp'><i class='fa fa-whatsapp' aria-hidden='true'></i></a>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");

                    strBuild.Append("</div>");
                }

            }
            //objNews.Add("Type", strType.ToString());
            objNews.Add("NewsDetail", strBuild.ToString());

            //objNews.Add("MetatTitle", strMetaTitle.ToString());

            return objNews;
        }

        #endregion


        #region Blog
        public ActionResult blog()
        {
            string title = "Home/blog";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            Blog obj = new Blog();
            obj.CategoryList = BindcategoryforBlog(obj);

            return View("~/Views/User/blog.cshtml", obj);


        }

        [HttpPost]
        public ActionResult blog(Blog obj)
        {



            if (obj.categoryid != 0)
            {
                //ViewBag.categoryid = Catid[Catid.Length - 1];
                ViewBag.category = obj.categoryid;


            }


            obj.CategoryList = BindcategoryforBlog(obj);

            return View("~/Views/User/blog.cshtml", obj);


        }

        public List<SelectListItem> BindcategoryforBlog(Blog obj)
        {
            DataTable dt = new DataTable();
            dt = util.Display("Execute Proc_Blog 'Getcategoryforblog'");

            List<SelectListItem> objList = new List<SelectListItem>();
            if (dt.Rows.Count > 0)
            {

                foreach (DataRow dr in dt.Rows)
                {
                    SelectListItem list = new SelectListItem();
                    list.Text = dr["category"].ToString();
                    list.Value = dr["categoryid"].ToString();

                    if (obj.categoryid == int.Parse(dr["categoryid"].ToString()))
                    {
                        list.Selected = true;
                    }

                    objList.Add(list);
                }

            }
            return objList.ToList();
        }
        public ActionResult blogdetailpage(string strblog)
        {
            string title = "Home/blogdetailpage";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            var obj = Bindblogdetailpage(strblog);
            ViewBag.blogdetailpage = obj["Blogdetails"].ToString();
            return View("~/Views/User/blogdetailpage.cshtml");
        }

        public Dictionary<string, string> Bindblogdetailpage(string strblog)
        {
            Dictionary<string, string> objNews = new Dictionary<string, string>();

            StringBuilder strBuild = new StringBuilder();
            //StringBuilder strType = new StringBuilder();

            StringBuilder strMetaTitle = new StringBuilder();
            //StringBuilder strrecent = new StringBuilder();

            DataSet ds = new DataSet();
            ds = util.Display1("Exec Proc_Blog 'bindBlogbyId',0,'','','','','" + strblog + "'");
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {

                        strMetaTitle.Append(dr["Slug"].ToString());

                        strBuild.Append("<div class='container pt-4'>");
                        strBuild.Append("<div class='row align-items-center justify-content-center mb-4'>");
                        strBuild.Append("<div class='col-12  col-lg-10 mb-4'>");
                        strBuild.Append("<h1 class='bigHeading'>");
                        strBuild.Append(dr["Title"].ToString());
                        strBuild.Append("</h1>");
                        strBuild.Append("</div>");
                        strBuild.Append("<div class='col-12 col-lg-10'>");
                        strBuild.Append("<div class='author'>By <span>" + dr["CreatedBy"].ToString() + "</span></div>");
                        strBuild.Append("<div class='date'><i class='fa fa-clock-o' aria-hidden='true'></i>" + dr["PostDate"].ToString() + "</div>");
                        strBuild.Append("</div>");
                        strBuild.Append("</div>");
                        strBuild.Append("</div>");
                        strBuild.Append("<div class='border-bottom-0 border mb-5'></div>");
                        strBuild.Append("<div class='container'>");
                        strBuild.Append("<div class='row justify-content-center mb-5'>");
                        strBuild.Append("<div class='col-12 col-lg-10 text-center'>");
                        strBuild.Append("<div class='mb-4 blogBanner'>");
                        strBuild.Append("<img src='/Content/uploads/Blog/" + dr["Image"].ToString() + "'>");
                        //strBuild.Append("<img src='images/blog/blog_banner.jpg'>");
                        strBuild.Append("</div>");
                        strBuild.Append(dr["Description"].ToString());
                        strBuild.Append("</div>");
                        strBuild.Append("</div>");
                        strBuild.Append("<div class='border-bottom-0 border mb-5'></div>");


                        strBuild.Append("<div class='row mb-5 justify-content-center'>");
                        strBuild.Append("<div class='col-12 col-lg-10 social_share'>");
                        //strBuild.Append("<a href='#' class='share'><i class='fa fa-share-alt' aria-hidden='true'></i></a>");
                        strBuild.Append("<a href='#' class='fb'><i class='fa fa-facebook' aria-hidden='true'></i></a>");
                        strBuild.Append("<a href='#' class='twit'><i class='fa fa-twitter' aria-hidden='true'></i></a>");
                        strBuild.Append("<a href='#' class='pinterest'><i class='fa fa-pinterest-p' aria-hidden='true'></i></a>");
                        strBuild.Append("<a href='#' class='whatsApp'><i class='fa fa-whatsapp' aria-hidden='true'></i></a>");
                        strBuild.Append("</div>");
                        strBuild.Append("</div>");


                        int categoryid = int.Parse(dr["Categoryid"].ToString());
                        DataTable dt = new DataTable();
                        dt = util.Display("Exec Proc_Blog 'GetRecentblogBycategory',0,'','','','','" + strblog + "'," + categoryid + "");
                        if (dt.Rows.Count > 0)
                        {
                            strBuild.Append("<div class='row justify-content-center pt-5'>");
                            foreach (DataRow drrecent in dt.Rows)
                            {
                                strBuild.Append("<div class='col-sm-6 col-lg-4 mb-5'>");
                                strBuild.Append("<a href='/blogdetailpage/" + drrecent["Slug"].ToString() + "' class='rounded borderLight box fadeAnim'>");
                                strBuild.Append("<div class='imageData'>");
                                strBuild.Append("<img src='/Content/uploads/Blog/" + drrecent["Image"].ToString() + "' class='img-fluid fadeAnim'>");
                                strBuild.Append("</div>");
                                strBuild.Append("<div class='matter p-4'>");
                                strBuild.Append("<h2>" + drrecent["title"].ToString() + "</h2>");
                                strBuild.Append("<div class='date'><i class='fa fa-clock-o' aria-hidden='true'></i> " + drrecent["PostDate"].ToString() + "</div>");
                                strBuild.Append(drrecent["Description"].ToString());
                                strBuild.Append("</div>");
                                strBuild.Append("<div class='authorName'>");
                                strBuild.Append(drrecent["CreatedBy"].ToString());
                                strBuild.Append("</div>");
                                strBuild.Append("</a>");
                                strBuild.Append("</div>");
                            }
                            strBuild.Append("</div>");
                        }

                        strBuild.Append("</div>");

                    }

                }

            }
            //objNews.Add("Type", strType.ToString());
            objNews.Add("Blogdetails", strBuild.ToString());

            //objNews.Add("MetatTitle", strMetaTitle.ToString());

            return objNews;
        }


        #endregion


        #region Faq

        public ActionResult faq()
        {
            string title = "Home/faq";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            var obj = GetFaq();
            ViewBag.Menu = obj["Menu"].ToString();
            ViewBag.Faqdetail = obj["Faqetails"].ToString();
            return View("~/Views/User/faq.cshtml");
        }

        public Dictionary<string, string> GetFaq()
        {
            Dictionary<string, string> objfaq = new Dictionary<string, string>();

            StringBuilder strmenu = new StringBuilder();
            StringBuilder strbuild = new StringBuilder();

            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_Faq 'getCategory'");

            if (dt.Rows.Count > 0)
            {
                int count = 1;

                foreach (DataRow dr in dt.Rows)
                {

                    if (count == 1)
                    {
                        strmenu.Append("<li class='active'><a href='#" + dr["Category"] + "'>" + dr["Category"] + "</a></li>");
                        count++;
                    }
                    else
                    {
                        strmenu.Append("<li><a href='#" + dr["Category"] + "'>" + dr["Category"] + "</a></li>");
                    }


                    strbuild.Append("<div id=" + dr["Category"].ToString() + ">");
                    strbuild.Append("<h2 class='text-uppercase'>" + dr["Category"].ToString() + "</h2>");
                    strbuild.Append("<ul>");
                    DataTable dt1 = new DataTable();
                    dt1 = util.Display("Exec Proc_Faq 'bindFaq',0,'" + dr["Category"].ToString() + "'");
                    if (dt1.Rows.Count > 0)
                    {
                        int countNew = 1;
                        foreach (DataRow dr1 in dt1.Rows)
                        {

                            if (countNew == 1 & dr["Category"].ToString() == "Buyer")
                            {
                                strbuild.Append("<li class='active'>");
                                strbuild.Append("<a class='head'>" + dr1["Title"].ToString() + "</a>");
                                strbuild.Append("<div>");
                                strbuild.Append(dr1["description"].ToString());
                                strbuild.Append("</div>");
                                strbuild.Append("</li>");



                            }
                            else
                            {
                                strbuild.Append("<li>");
                                strbuild.Append("<a class='head'>" + dr1["Title"].ToString() + "</a>");
                                strbuild.Append("<div>");
                                strbuild.Append(dr1["description"].ToString());
                                strbuild.Append("</div>");
                                strbuild.Append("</li>");

                            }
                            countNew++;
                        }

                    }

                    strbuild.Append("</ul>");
                    strbuild.Append("</div>");



                }
            }


            objfaq.Add("Menu", strmenu.ToString());
            objfaq.Add("Faqetails", strbuild.ToString());


            return objfaq;
        }


        #endregion

        //created on 21_04_2020

        public string GetPageCMSMaster(int id)
        {
            string response = "";
            StringBuilder strbuild = new StringBuilder();

            DataSet ds = new DataSet();
            ds = util.Display1("Exec Proc_PageCMSMaster 'get',''," + id + "");
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    int count = 1;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        if (count == 1)
                        {
                            strbuild.Append("<div class='card'>");
                            strbuild.Append(" <div class='card-header' id='headingOne'>");

                            strbuild.Append("<h5 class='mb-0'>");
                            strbuild.Append("<button class='btn btn-link' type='button' data-toggle='collapse' data-target='#" + Generateslg(dr["Title"].ToString()) + "' aria-expanded='true' aria-controls='collapseOne'>");
                            strbuild.Append(dr["Title"].ToString());
                            strbuild.Append("</button>");
                            strbuild.Append(" </h5>");
                            strbuild.Append("</div>");
                            strbuild.Append("<div id = '" + Generateslg(dr["Title"].ToString()) + "' class='collapse show' aria-labelledby='headingOne' data-parent='#accordionExample'>");
                            strbuild.Append("<div class='card-body'>");
                            strbuild.Append(dr["Description"].ToString());
                            strbuild.Append("</div>");
                            strbuild.Append("</div>");
                            strbuild.Append("</div>");
                        }
                        else
                        {
                            strbuild.Append("<div class='card'>");
                            strbuild.Append("<div class='card-header' id='headingTwo'>");

                            strbuild.Append("<h5 class='mb-0'>");
                            strbuild.Append("<button class='btn btn-link collapsed' type='button' data-toggle='collapse' data-target='#" + Generateslg(dr["Title"].ToString()) + "' aria-expanded='false' aria-controls='collapseTwo'>");
                            strbuild.Append(dr["Title"].ToString());
                            strbuild.Append("</button>");
                            strbuild.Append(" </h5>");
                            strbuild.Append("</div>");
                            strbuild.Append("<div id = '" + Generateslg(dr["Title"].ToString()) + "' class='collapse' aria-labelledby='headingTwo' data-parent='#accordionExample'>");
                            strbuild.Append("<div class='card-body'>");
                            strbuild.Append(dr["Description"].ToString());
                            strbuild.Append("</div>");
                            strbuild.Append("</div>");
                            strbuild.Append("</div>");
                        }


                        count++;
                    }
                }
            }
            response = strbuild.ToString();
            return response;

        }

        public string CMS(int Id)
        {
            string response = "";
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_CMS 'bindCMS','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                response = dt.Rows[0]["Description"].ToString();
            }
            return response;
        }


        public string Generateslg(string strtext)
        {
            string str = strtext.ToString().ToLower();
            str = Regex.Replace(str, @"[^0-9a-zA-Z]+", "");
            return str;
        }

        #region get MetaTags

        public static string UpdateMetaDetails(string pageUrl)
        {
            Utility util = new Utility();
            //--- StringBuilder object to store MetaTags information.
            StringBuilder sbMetaTags = new StringBuilder();

            //--Step1 Get data from database.

            DataTable dt = new DataTable();
            dt = util.Display("exec Proc_MetaTag 'bindMetatTag',0,'" + pageUrl.ToString() + "'");
            if (dt.Rows.Count > 0)
            {

                //---- Step2 In this step we will add <title> tag to our StringBuilder Object.
                sbMetaTags.Append("<title>" + dt.Rows[0]["Title"].ToString() + "</title>");

                //---- Step3 In this step we will add "Meta Description" to our StringBuilder Object.
                sbMetaTags.Append("<meta name='description' content='" + dt.Rows[0]["MetaDescription"].ToString() + "' >");

                //---- Step4 In this step we will add "Meta Keywords" to our StringBuilder Object.
                sbMetaTags.Append("<meta name='keywords' content ='" + dt.Rows[0]["MetaKeywords"].ToString() + "'>");
            }
            return sbMetaTags.ToString();
        }

        #endregion



        // promotion form for vinatge and classic cars
        public ActionResult VintageAndClassicCar()
        {

            string title = "Home/VintageAndClassicCar";

            ViewBag.MetaTags = UpdateMetaDetails(title);
            return View("~/Views/User/VintageAndClassicCar.cshtml");
        }

        [HttpPost]
        public ActionResult VintageAndClassicCar(VintageAndClassicCar obj, HttpPostedFileBase[] Image, HttpPostedFileBase[] Image2, HttpPostedFileBase[] Image3)
        {
            try
            {
                if (obj.Id != 0)
                {
                    ModelState.Remove("Image");
                    ModelState.Remove("Image2");
                    ModelState.Remove("Image3");
                }

                if (ModelState.IsValid)
                {
                    string VirtualFileImage = "~/images/Vintage_Car_promotion/";
                    string VirtualFileImage2 = "~/images/Vintage_Car_promotion/";
                    string VirtualFileImage3 = "~/images/Vintage_Car_promotion/";

                    string Img = string.Empty;
                    string Img2 = string.Empty;
                    string Img3 = string.Empty;

                    if (Image != null)
                    {
                        foreach (HttpPostedFileBase file in Image)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("Vintage_Car_promotion");
                                }
                                else
                                {
                                    var fileName = "Image-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFileImage + fileName));
                                    VirtualFileImage = VirtualFileImage.Replace("~", "");
                                    Img += VirtualFileImage + fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            obj.Image = Img.Substring(0, Img.Length - 1);
                        }
                    }


                    if (Image2 != null)
                    {
                        foreach (HttpPostedFileBase file in Image2)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("Vintage_Car_promotion");
                                }
                                else
                                {
                                    var fileName = "Image-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFileImage2 + fileName));
                                    VirtualFileImage2 = VirtualFileImage2.Replace("~", "");
                                    Img2 += VirtualFileImage2 + fileName + ",";
                                }
                            }
                        }

                        if (Img2 != "")
                        {
                            obj.Image2 = Img2.Substring(0, Img2.Length - 1);
                        }
                    }

                    if (Image3 != null)
                    {
                        foreach (HttpPostedFileBase file in Image3)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("Vintage_Car_promotion");
                                }
                                else
                                {
                                    var fileName = "Image-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFileImage3 + fileName));
                                    VirtualFileImage3 = VirtualFileImage3.Replace("~", "");
                                    Img3 += VirtualFileImage3 + fileName + ",";
                                }
                            }
                        }

                        if (Img3 != "")
                        {
                            obj.Image3 = Img3.Substring(0, Img3.Length - 1);
                        }
                    }



                    if (obj.Id == 0)
                    {
                        if (InsertVintageAndClassicCar(obj))
                        {
                            ModelState.Clear();

                            TempData["Message"] = "Success";
                            TempData["Message1"] = "Thank you for registered on AstaGuru Online Auction House";

                            return RedirectToAction("VintageAndClassicCar");
                        }
                        else
                        {
                            return RedirectToAction("VintageAndClassicCar");
                        }
                    }

                }
                return View("~/Views/User/VintageAndClassicCar.cshtml", obj);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);


                return null;
            }
        }

        public bool InsertVintageAndClassicCar(VintageAndClassicCar obj)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_VintageAndClassicCar"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Add");
                cmd.Parameters.AddWithValue("@Email", obj.Email);
                cmd.Parameters.AddWithValue("@Automaker", obj.Automaker);
                cmd.Parameters.AddWithValue("@Model", obj.Model);
                cmd.Parameters.AddWithValue("@Engine", obj.Engine);
                cmd.Parameters.AddWithValue("@Chassis", obj.Chassis);
                cmd.Parameters.AddWithValue("@Registration", obj.Registration);
                cmd.Parameters.AddWithValue("@Aboutus", obj.Aboutus);
                cmd.Parameters.AddWithValue("@Request", obj.Request);
                cmd.Parameters.AddWithValue("@Image", obj.Image);
                cmd.Parameters.AddWithValue("@Image2", obj.Image2);
                cmd.Parameters.AddWithValue("@Image3", obj.Image3);

                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;

        }


        public ActionResult truefittandhill()
        {
            return View("~/Views/User/truefittandhill.cshtml");
        }


        //created on 22_12_2020
        public ActionResult ShowProductByDepartment()
        {
            string title = "Home/ShowProductByDepartment";
            ViewBag.category = "";
            ViewBag.MetaTags = UpdateMetaDetails(title);



            return View("~/Views/User/ShowProductByDepartment.cshtml");
        }

        public ActionResult ShowProductByDepartmentList()
        {
            string title = "Home/ShowProductByDepartmentList";
            ViewBag.category = "";
            ViewBag.MetaTags = UpdateMetaDetails(title);



            return View("~/Views/User/ShowProductByDepartmentList.cshtml");
        }

        //



        //created on 21_12_2020
        // Promotion Form
        public ActionResult Promotion()
        {

            string title = "Home/Promotion";
            ViewBag.MetaTags = UpdateMetaDetails(title);

            Promotion obj = new Promotion();

            return View("~/Views/User/Promotion.cshtml");

        }

        public List<SelectListItem> BindArtist()
        {
            DataTable dt = new DataTable();

            dt = util.Display("select distinct artistid,firstname+ ' ' +lastname as Artistname from artist order by Artistname");
            List<SelectListItem> li = new List<SelectListItem>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    //li.Add(new SelectListItem { Text = dr["auctiondate"].ToString(), Value = dr["AuctionId"].ToString() });
                    li.Add(new SelectListItem { Text = dr["Artistname"].ToString(), Value = dr["Artistname"].ToString() });
                    //obj.SubId = int.Parse(dr["SubId"].ToString());


                }
            }


            return li;
        }


        [HttpPost]
        public ActionResult Promotion(Promotion obj, HttpPostedFileBase[] Image, HttpPostedFileBase[] Image2, HttpPostedFileBase[] Image3, FormCollection fobj)
        {
            try
            {
                if (obj.Id != 0)
                {
                    ModelState.Remove("Image");
                    ModelState.Remove("Image2");
                    ModelState.Remove("Image3");
                }

                if (ModelState.IsValid)
                {
                    string VirtualFileImage = "~/images/Promotion/";
                    string VirtualFileImage2 = "~/images/Promotion/";
                    string VirtualFileImage3 = "~/images/Promotion/";

                    string Img = string.Empty;
                    string Img2 = string.Empty;
                    string Img3 = string.Empty;

                    obj.ArtistName = fobj["hiddenArtist"].ToString();

                    if (Image != null)
                    {
                        foreach (HttpPostedFileBase file in Image)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("Promotion");
                                }
                                else
                                {
                                    var fileName = "Image-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFileImage + fileName));
                                    VirtualFileImage = VirtualFileImage.Replace("~", "");
                                    Img += VirtualFileImage + fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            obj.Image = Img.Substring(0, Img.Length - 1);
                        }
                    }


                    if (Image2 != null)
                    {
                        foreach (HttpPostedFileBase file in Image2)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("Promotion");
                                }
                                else
                                {
                                    var fileName = "Image-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFileImage2 + fileName));
                                    VirtualFileImage2 = VirtualFileImage2.Replace("~", "");
                                    Img2 += VirtualFileImage2 + fileName + ",";
                                }
                            }
                        }

                        if (Img2 != "")
                        {
                            obj.Image2 = Img2.Substring(0, Img2.Length - 1);
                        }
                    }

                    if (Image3 != null)
                    {
                        foreach (HttpPostedFileBase file in Image3)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("Promotion");

                                }
                                else
                                {
                                    var fileName = "Image-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFileImage3 + fileName));
                                    VirtualFileImage3 = VirtualFileImage3.Replace("~", "");
                                    Img3 += VirtualFileImage3 + fileName + ",";
                                }
                            }
                        }

                        if (Img3 != "")
                        {
                            obj.Image3 = Img3.Substring(0, Img3.Length - 1);
                        }
                    }



                    if (obj.Id == 0)
                    {
                        if (InsertPromotion(obj))
                        {
                            ModelState.Clear();

                            TempData["Message"] = "Success";
                            TempData["Message1"] = "Thank you for registered on AstaGuru Online Auction House";

                            return RedirectToAction("Promotion");
                        }
                        else
                        {
                            return RedirectToAction("Promotion");
                        }
                    }

                }
                return View("~/Views/User/Promotion.cshtml", obj);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);


                return null;
            }
        }

        public bool InsertPromotion(Promotion obj)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_Promotion"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Add");
                cmd.Parameters.AddWithValue("@Category", obj.Category);
                cmd.Parameters.AddWithValue("@Name", obj.Name);
                cmd.Parameters.AddWithValue("@Mobile_Number", obj.Mobile_Number);
                cmd.Parameters.AddWithValue("@Email", obj.Email);
                cmd.Parameters.AddWithValue("@Aboutus", obj.Aboutus);
                cmd.Parameters.AddWithValue("@Description", obj.Description);
                cmd.Parameters.AddWithValue("@Image", obj.Image);
                cmd.Parameters.AddWithValue("@Image2", obj.Image2);
                cmd.Parameters.AddWithValue("@Image3", obj.Image3);

                cmd.Parameters.AddWithValue("@ArtistName", obj.ArtistName);
                cmd.Parameters.AddWithValue("@ArtworkName", obj.ArtworkName);

                if (obj.YearofPurchaseModern != null)
                {
                    cmd.Parameters.AddWithValue("@YearofPurchase", obj.YearofPurchaseModern);
                }
                else if (obj.YearofPurchaseBook != null)
                {
                    cmd.Parameters.AddWithValue("@YearofPurchase", obj.YearofPurchaseBook);
                }
                else if (obj.YearofPurchaseJewellery != null)
                {
                    cmd.Parameters.AddWithValue("@YearofPurchase", obj.YearofPurchaseJewellery);
                }
                else if (obj.YearofPurchaseTimepieces != null)
                {
                    cmd.Parameters.AddWithValue("@YearofPurchase", obj.YearofPurchaseTimepieces);
                }


                cmd.Parameters.AddWithValue("@Purchasefrom", obj.Purchasefrom);
                cmd.Parameters.AddWithValue("@AuthorPublisherName", obj.AuthorPublisherName);
                cmd.Parameters.AddWithValue("@BookPublishedYear", obj.BookPublishedYear);

                cmd.Parameters.AddWithValue("@BookCondition", obj.BookCondition);
                cmd.Parameters.AddWithValue("@ChoosAuctionOption", obj.ChoosAuctionOption);
                cmd.Parameters.AddWithValue("@Automaker", obj.Automaker);
                cmd.Parameters.AddWithValue("@Model", obj.Model);
                cmd.Parameters.AddWithValue("@Engine", obj.Engine);
                cmd.Parameters.AddWithValue("@Chassis", obj.Chassis);
                cmd.Parameters.AddWithValue("@Registration", obj.Registration);

                if (obj.BillOfPurchaseJewellery != null)
                {
                    cmd.Parameters.AddWithValue("@BillOfPurchase", obj.BillOfPurchaseJewellery);
                }
                else if (obj.BillOfPurchaseTimepieces != null)
                {
                    cmd.Parameters.AddWithValue("@BillOfPurchase", obj.BillOfPurchaseTimepieces);
                }

                cmd.Parameters.AddWithValue("@Weight", obj.Weight);
                cmd.Parameters.AddWithValue("@Brand", obj.Brand);


                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;

        }

        // Consignment
        public ActionResult consign()
        {
            consign obj = new consign();
            obj.CategoryList = BindcategoryforConsign(obj);
            return View("~/Views/User/consign.cshtml", obj);
        }

        public List<SelectListItem> BindcategoryforConsign(consign obj)
        {
            DataTable dt = new DataTable();
            dt = util.Display("Execute Proc_video 'Getcategoryforvideo'");

            List<SelectListItem> objList = new List<SelectListItem>();
            if (dt.Rows.Count > 0)
            {

                foreach (DataRow dr in dt.Rows)
                {
                    SelectListItem list = new SelectListItem();
                    list.Text = dr["category"].ToString();
                    list.Value = dr["categoryid"].ToString();

                    if (obj.categoryid == int.Parse(dr["categoryid"].ToString()))
                    {
                        list.Selected = true;
                    }

                    objList.Add(list);
                }

            }
            return objList.ToList();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult consign(consign obj, HttpPostedFileBase[] UploadPhotos, FormCollection fobj)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    obj.category = fobj["hdncategory"].ToString();

                    string test = fobj["hdnuserid"].ToString();
                    obj.Userid= int.Parse(fobj["hdnuserid"].ToString());

                    //string VirtualFile = "~/Content/Consign/";

                    //string UploadImages = string.Empty;

                    //if (UploadPhotos != null)
                    //{
                    //    foreach (HttpPostedFileBase file in UploadPhotos)
                    //    {
                    //        //string MainFile = string.Empty;
                    //        //Checking file is available to save.  
                    //        if (file != null)
                    //        {
                    //            string ext = Path.GetExtension(file.FileName).ToLower();
                    //            if (!util.IsValidImageFileExtension(ext))
                    //            {
                    //                ViewBag.Message = "warning";
                    //                ViewBag.Message1 = "Only Pdf files are allowed!";

                    //                TempData["Message"] = ViewBag.Message;
                    //                TempData["Message1"] = ViewBag.Message1;
                    //                return RedirectToAction("consign");
                    //            }
                    //            else
                    //            {
                    //                //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                    //                //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                    //                var fileName = "Img-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                    //                file.SaveAs(Server.MapPath(VirtualFile + fileName));
                    //                UploadImages += fileName + ",";
                    //            }
                    //        }
                    //    }

                    //    if (UploadImages != "")
                    //    {
                    //        obj.UploadPhotos = UploadImages.Substring(0, UploadImages.Length - 1);
                    //    }
                    //}



                    if (SaveConsign(obj))
                    {
                        ModelState.Clear();

                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record added successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        obj.CategoryList = BindcategoryforConsign(obj);

                        //return View("~/Views/User/consign.cshtml", obj);
                        return RedirectToAction("consign");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not added successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("consign");
                    }


                }
                return View("~/Views/User/consign.cshtml", obj);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("consign");
                //return RedirectToAction("~/Views/User/consign.cshtml", obj);
            }

        }

        public bool SaveConsign(consign objconsign)
        {
            bool response;


            using (SqlCommand cmd = new SqlCommand("Proc_Consignment"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Artist_Designer", objconsign.Artist);
                cmd.Parameters.AddWithValue("@Title", objconsign.Title);
                cmd.Parameters.AddWithValue("@category", objconsign.category);
                cmd.Parameters.AddWithValue("@Year", objconsign.Year);
                cmd.Parameters.AddWithValue("@Medium", objconsign.Medium);
                cmd.Parameters.AddWithValue("@Height", objconsign.Height);
                cmd.Parameters.AddWithValue("@Width", objconsign.Width);
                cmd.Parameters.AddWithValue("@Depth", objconsign.Depth);
                cmd.Parameters.AddWithValue("@Units", objconsign.Units);
                cmd.Parameters.AddWithValue("@EditionNumaber", objconsign.EditionNumaber);
                cmd.Parameters.AddWithValue("@EditionSize", objconsign.EditionSize);
                cmd.Parameters.AddWithValue("@WorkSign", objconsign.WorkSign);
                cmd.Parameters.AddWithValue("@CertificateOfAuthencity", objconsign.CertificateOfAuthencity);
                cmd.Parameters.AddWithValue("@AquireWork", objconsign.AquireWork);
                cmd.Parameters.AddWithValue("@CityLocated", objconsign.CityLocated);
                cmd.Parameters.AddWithValue("@Pricemind", objconsign.Pricemind);
                cmd.Parameters.AddWithValue("@PriceCurrency", objconsign.PriceCurrency);
                cmd.Parameters.AddWithValue("@Price", objconsign.Price);
                cmd.Parameters.AddWithValue("@MobileNumber", objconsign.MobileNumber);
                //cmd.Parameters.AddWithValue("@UploadPhotos", objconsign.UploadPhotos);
                cmd.Parameters.AddWithValue("@MoreInformation", objconsign.MoreInformation);
                cmd.Parameters.AddWithValue("@Userid", objconsign.Userid);
        
                cmd.Parameters.Add("@Consignmentid", SqlDbType.Int).Direction = ParameterDirection.Output;

                if (util.Execute(cmd))
                {
                    string id = cmd.Parameters["@Consignmentid"].Value.ToString();
                    objconsign.Consignmentid = int.Parse(id);

                    if (TempData["Mutlti"] != null)
                    {
                        List<string> objImgList = TempData["Mutlti"] as List<string>;

                        foreach (var list in objImgList)
                        {
                            using (SqlCommand cmd1 = new SqlCommand("Proc_ConsignMultipleImage"))
                            {
                                cmd1.CommandType = CommandType.StoredProcedure;
                                cmd1.Parameters.AddWithValue("@Para", "Add");
                                cmd1.Parameters.AddWithValue("@Consignmentid", objconsign.Consignmentid);
                                cmd1.Parameters.AddWithValue("@ImageName", list);
                                if (util.Execute(cmd1))
                                {
                                    response = true;
                                }
                            }
                        }

                    }


                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        // landing page for consign
        public ActionResult Consignwithus()
        {

            return View("~/Views/User/Consignwithus.cshtml");

        }


        public JsonResult InsertMultipleImages(consign objconsign)
        {
            int id = objconsign.Consignmentid;
            TempData["Mutlti"] = objconsign.MultiImage;
          

            return Json(true, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Marketing()
        {

            return View("~/Views/User/Marketing.cshtml");

        }

        [HttpPost]
        public ActionResult Marketing(Marketing obj)
        {
            try
            {
               
                if (ModelState.IsValid)
                {
                 
                    if (obj.Id == 0)
                    {
                        if (SaveMarketing(obj))
                        {
                            ModelState.Clear();

                            TempData["Message"] = "Success";
                            TempData["Message1"] = "Record added successfully.";

                            return RedirectToAction("Marketing");
                        }
                        else
                        {
                            return RedirectToAction("Marketing");
                        }
                    }

                }
                return View("~/Views/User/Marketing.cshtml", obj);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);


                return null;
            }

        }

        public bool SaveMarketing(Marketing obj)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_Marketing"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Add");
                cmd.Parameters.AddWithValue("@Name", obj.Name);
                cmd.Parameters.AddWithValue("@Email", obj.Email);
                cmd.Parameters.AddWithValue("@Country", obj.Country);
                cmd.Parameters.AddWithValue("@City", obj.City);
                cmd.Parameters.AddWithValue("@Interested_In", obj.Interested_In);
                cmd.Parameters.AddWithValue("@About_Us", obj.About_Us);
                cmd.Parameters.AddWithValue("@Mobile", obj.Mobile);


                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;

        }

        // Custom Error
        public ActionResult Error()
        {
            return View();
        }

    }

}