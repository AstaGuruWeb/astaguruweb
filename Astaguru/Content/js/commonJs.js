﻿$('.addToCart').click(function () {
    $("span.clsClearGallary").text('');
    var baseUrlPath = $('#hiddenbaseUrlPath').val();
    var userId = $('#hiddenUserId').val();
    var productId = $(this).attr("data-productid");

    $("#divLoader").show();

    if (userId > 0) {

        var addGallary = {};
        addGallary.userid = userId;
        addGallary.productid = productId;

        $.ajax({
            url: "/Bid/addToGallary",
            type: 'POST',
            dataType: 'json',
            data: addGallary,
            traditional: true,
            success: function (ReturnData) {
                if (ReturnData.status == 'Success') {
                    $("#divLoader").hide();
                    document.getElementById('Success_' + productId).innerHTML = 'The Lot has been added to your auction gallery.'
                    document.getElementById('Err_' + productId).innerHTML = '';
                    setTimeout(function () {
                        document.getElementById('Success_' + productId).innerHTML = ''
                        document.getElementById('Err_' + productId).innerHTML = '';
                    }, 3000);
                }
                else if (ReturnData.status == 'Error') {
                    $("#divLoader").hide();
                    document.getElementById('Success_' + productId).innerHTML = ''
                    document.getElementById('Err_' + productId).innerHTML = 'This Lot already exist in your auction gallery.';
                    setTimeout(function () {
                        document.getElementById('Success_' + productId).innerHTML = ''
                        document.getElementById('Err_' + productId).innerHTML = '';
                    }, 3000);
                }
            },
        });
    }
    else {
        $("#divLoader").hide();
        $('#LoginForm').modal('toggle');
    }

});




$('#tabAuctionGallary').click(function () {
    var userId = $('#hiddenUserId').val();
    var baseUrlPath = $('#hiddenbaseUrlPath').val();
    if (userId > 0) {
        window.location = baseUrlPath + '/Home/MyAuctionGallary'
    }
    else {
        $('#LoginForm').modal('toggle');
    }


});

$('#ddlCurrency').change(function () {
    $("#divLoader").show();

    var baseUrlPath = $('#hiddenbaseUrlPath').val();
    var setCurrency = $('#ddlCurrency').val();

    var currency = {};
    currency.currency = setCurrency;

    $.ajax({
        url: "/Common/setCurrency",
        type: 'POST',
        dataType: 'json',
        data: currency,
        traditional: true,
        success: function (ReturnData) {
            if (ReturnData.Success) {
                $("#divLoader").hide();
                window.location.reload();
            }
        },
    });

});




$('.removeCart').click(function () {

    $("span.clsClearGallary").text('');
    var baseUrlPath = $('#hiddenbaseUrlPath').val();
    var userId = $('#hiddenUserId').val();
    var productId = $(this).attr("data-productid");

    $("#divLoaderMaster").show();

    if (userId > 0) {

        var removeGallary = {};
        removeGallary.userid = userId;
        removeGallary.productid = productId;

        $.ajax({
            url: "/Bid/DeleteFromGallary",
            type: 'POST',
            dataType: 'json',
            data: removeGallary,
            traditional: true,
            success: function (ReturnData) {
                if (ReturnData.status == 'Success') {
                    $("#divLoaderMaster").hide();
                    window.location.reload();
                    
                }
                else if (ReturnData.status == 'Error') {
                    $("#divLoaderMaster").hide();
                    window.location.reload();
                }
            },
        });
    }
    else {
        $("#divLoader").hide();
        $('#LoginForm').modal('toggle');
    }

});
