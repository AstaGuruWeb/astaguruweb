﻿using Astaguru.Models;
using Dapper;
using Repository.Lib;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Astaguru.Services.UserService
{
    public class ArtistService
    {
        Log log = new Log();
        public Auction getArtistProfile(int artistid)
        {
            
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "getArtistProfile");
                    para.Add("@artistid", artistid);
                    return con.Query<Auction>("CRUDArtist", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }
              
            }
        }

        public List<Auction> getArtistCurrentAuction(Auction AUC)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "getArtistCurrentAuction");
                    para.Add("@artistid", AUC.artistid);
                    para.Add("@AuctionId", AUC.Auctionid);
                    return con.Query<Auction>("CRUDArtist", para, null, true, 0, CommandType.StoredProcedure).ToList();
                }

                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public List<Auction> getArtistPastAuction(Auction AUC)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "getArtistPastAuction");
                    para.Add("@artistid", AUC.artistid);
                    para.Add("@AuctionId", AUC.Auctionid);
                    return con.Query<Auction>("CRUDArtist", para, null, true, 0, CommandType.StoredProcedure).ToList();
                }         

                  catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public List<Auction> getArtistList()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "getArtistList");
                    return con.Query<Auction>("CRUDArtist", para, null, true, 0, CommandType.StoredProcedure).ToList();

                }

                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public List<Auction> getArtistResult(int artistid,string Searchtype)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    if (Searchtype == "All" || Searchtype == null || Searchtype == "")
                    {
                        var para = new DynamicParameters();
                        para.Add("@Mode", "getArtistResult");
                        para.Add("@artistid", artistid);
                        return con.Query<Auction>("CRUDArtist", para, null, true, 0, CommandType.StoredProcedure).ToList();
                    }

                    else if (Searchtype.Trim() == "Art")
                    {
                        var para = new DynamicParameters();
                        para.Add("@Mode", "getArtistResultByAuctionType");
                        para.Add("@artistid", artistid);
                        para.Add("@AuctionType", 1);
                        return con.Query<Auction>("CRUDArtist", para, null, true, 0, CommandType.StoredProcedure).ToList();
                    }

                    else if (Searchtype.Trim() == "Jewelry, Antiques & Collectibles")
                    {
                        var para = new DynamicParameters();
                        para.Add("@Mode", "getArtistResultByAuctionType");
                        para.Add("@artistid", artistid);
                        para.Add("@AuctionType", 2);
                        return con.Query<Auction>("CRUDArtist", para, null, true, 0, CommandType.StoredProcedure).ToList();
                    }
                }
             

                  catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }

            }
            return null;
        }


        //public int getArtistid(string firstname,string lastname)
        //{
        //    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
        //    {
        //        try
        //        {
        //            var para = new DynamicParameters();
        //            para.Add("@Mode", "getArtistId");
        //            para.Add("@FirstName", "firstname");
        //            para.Add("@LastName", "lastname");
        //            return con.Query<int>("CRUDArtist", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();

        //        }

        //        catch (Exception ex)
        //        {
        //            log.logErrorMessage(ex.Message);
        //            log.logErrorMessage(ex.StackTrace);
        //            return 0;
        //        }
        //        finally
        //        {
        //            con.Close();
        //        }
        //    }
           
        //}


    }
}