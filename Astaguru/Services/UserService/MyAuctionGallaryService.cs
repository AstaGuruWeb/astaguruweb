﻿using Astaguru.Models;
using Dapper;
using Repository.Lib;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Astaguru.Services.UserService
{
    public class MyAuctionGallaryService
    {
        Log log = new Log();
        public List<Auction> GetMyAuctionGallaryList(Auction AUC)
        {
            Utility util = new Utility();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Para", "getMyGallery");
                    para.Add("@userid", AUC.userid);
                    return con.Query<Auction>("Proc_getMyGallery", para, null, true, 0, CommandType.StoredProcedure).ToList();
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }

            }

        }

        public double GetMyAuctionGallaryCount(Auction AUC)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    var Listscheme = con.Query<double>("select COUNT(v2_getMyGallery.productid) from v2_getMyGallery WHERE userid= " + AUC.userid + "", null, null, true, 0, CommandType.Text).SingleOrDefault();
                    return Listscheme;
                }
              
                  catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
                finally
                {
                    con.Close();
                }
            }

        }

    }
}