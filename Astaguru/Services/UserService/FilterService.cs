﻿using Astaguru.Models;
using Dapper;
using Repository.Lib;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Astaguru.Services.UserService
{
    public class FilterService
    {
        Log log = new Log();
        public List<Auction> GetFilterArtist(int Auctionid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetFilterArtist");
                    para.Add("@Auctionid", Auctionid);
                    return con.Query<Auction>("CRUDFilter", para, null, true, 0, CommandType.StoredProcedure).ToList();
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }

            }
        }

        public List<Auction> GetFilterDepartment(int Auctionid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetFilterDepartment");
                    para.Add("@Auctionid", Auctionid);
                    return con.Query<Auction>("CRUDFilter", para, null, true, 0, CommandType.StoredProcedure).ToList();
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }

            }
        }

        public List<Auction> GetFilterMedium(int Auctionid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetFilterMedium");
                    para.Add("@Auctionid", Auctionid);
                    return con.Query<Auction>("CRUDFilter", para, null, true, 0, CommandType.StoredProcedure).ToList();
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }


            }
        }
    }
}