﻿using Astaguru.Models;
using Dapper;
using Repository.Lib;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Astaguru.Services.UserService
{
    public class VideoService
    {
        Log log = new Log();
        public double GetVideoCount(string Subquery, string categoryid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    if (categoryid == "0")
                    {
                        var Listscheme = con.Query<double>("select COUNT(Id) from Video where IsActive=1", null, null, true, 0, CommandType.Text).SingleOrDefault();
                        return Listscheme;
                    }
                    else
                    {
                        var Listscheme = con.Query<double>("select COUNT(Id) from Video where IsActive=1 and Categoryid=" + categoryid + "", null, null, true, 0, CommandType.Text).SingleOrDefault();
                        return Listscheme;
                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
                finally
                {
                    con.Close();
                }

            }
        }


        public List<Video> GetVideoList(string Subquery, int Page, string categoryid)
        {

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    if (categoryid == "0")
                    {

                        var Listscheme = con.Query<Video>("select Id,Title,VideoUrl,CONVERT(varchar,PostDate,106) as PostDate, IsActive,Categoryid from Video where IsActive=1  ORDER BY Id desc OFFSET " + Page + " ROWS FETCH NEXT 15 ROWS ONLY", null, null, true, 0, CommandType.Text).ToList();
                        return Listscheme;
                    }
                    else
                    {
                        var Listscheme = con.Query<Video>("select Id,Title,VideoUrl,CONVERT(varchar,PostDate,106) as PostDate, IsActive,Categoryid from Video where IsActive=1 and Categoryid=" + categoryid + " ORDER BY Id desc OFFSET " + Page + " ROWS FETCH NEXT 15 ROWS ONLY", null, null, true, 0, CommandType.Text).ToList();
                        return Listscheme;
                    }
                }              
                   catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }
            }
        }

    }
}