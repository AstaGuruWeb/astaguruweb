﻿using Astaguru.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Astaguru.Services.UserService
{
    public class DepartmentService
    {
        public List<Auction> getPastAuctionByDepartment(string categoryid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    var Listscheme = con.Query<Auction>("select TOP 4 acution.*, artist.firstname, artist.lastname, medium.medium, category.category,style.style,AuctionList.Auctionname,AuctionList.Date, AuctionList.AuctionId as closingtime from acution, artist, medium, category, style, AuctionList where acution.artistid = artist.artistid and acution.mediumid = medium.mediumid and acution.categoryid = category.categoryid and AuctionList.AuctionId = acution.Online and acution.Online <> 13   and acution.styleid = style.styleid and acution.categoryid in(" + categoryid + ") and AuctionList.status = 'past' order by auctionlist.auctionid  desc", null, null, true, 0, CommandType.Text).ToList();
                    return Listscheme;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    con.Close();
                }

            }


        }


        public List<Auction> getCurrentAuctionByDepartment(string categoryid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    var Listscheme = con.Query<Auction>("select TOP 4 acution.*, artist.firstname, artist.lastname, medium.medium, category.category,style.style,AuctionList.Auctionname,AuctionList.Date, AuctionList.AuctionId as closingtime from acution, artist, medium, category, style, AuctionList where acution.artistid = artist.artistid and acution.mediumid = medium.mediumid and acution.categoryid = category.categoryid and AuctionList.AuctionId = acution.Online and acution.Online <> 13   and acution.styleid = style.styleid and acution.categoryid in(" + categoryid + ") and AuctionList.status = 'Current' order by acution.productid asc", null, null, true, 0, CommandType.Text).ToList();
                    return Listscheme;

                }

                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    con.Close();
                }

            }


        }



        public double getAllProductByDepartmentCount(string categoryid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();

                    var Listscheme = con.Query<double>("select COUNT(acution.productid)  from acution  with (NOLOCK) where categoryid in(" + categoryid + ")", null, null, true, 0, CommandType.Text).SingleOrDefault();
                    return Listscheme;
                }
                 catch (Exception ex)
                {
                    return 0;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public List<Auction> getAllProductByDepartment(string categoryid,int Page)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {

                try
                {
                    var para = new DynamicParameters();
                    var Listscheme = con.Query<Auction>("select acution.*, artist.firstname, artist.lastname, medium.medium, category.category,style.style,AuctionList.Auctionname,AuctionList.Date,AuctionList.AuctionId, AuctionList.DollarRate, AuctionList.status, AuctionList.auctionType from acution, artist, medium, category, style, AuctionList where acution.artistid = artist.artistid and acution.mediumid = medium.mediumid and acution.categoryid = category.categoryid and AuctionList.AuctionId = acution.Online and acution.styleid = style.styleid and  acution.categoryid in(" + categoryid + ")  order by cast(online as integer) desc  OFFSET " + Page + " ROWS FETCH NEXT 200 ROWS ONLY", null, null, true, 0, CommandType.Text).ToList();
                    return Listscheme;
                }

                 catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    con.Close();
                }
            }
        }



    }
}