﻿using Astaguru.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Repository.Lib;

namespace Astaguru.Services.UserService
{
    public class CurrentAuctionService
    {
        Utility util = new Utility();
        Log log = new Log();
    
        public List<Auction> GetCurrentAuctionList(string Subquery, int Page)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    if (Subquery.Length > 0 && Subquery != null)
                    {

                        //para.Add("@Mode", "GetCurrentAuctionList");
                        //para.Add("@Page", Page);
                        //var Listscheme = con.Query<Auction>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).ToList();
                        //return Listscheme;

                        var Listscheme = con.Query<Auction>("select * from v2_defaultlots with (NOLOCK) where " + Subquery + " ORDER BY cast(v2_defaultlots.reference as int) OFFSET " + Page + " ROWS FETCH NEXT 20 ROWS ONLY", null, null, true, 0, CommandType.Text).ToList();
                        con.Close();
                        return Listscheme;

                    }
                    else
                    {

                        para.Add("@Mode", "GetCurrentAuctionList");
                        para.Add("@Page", Page);
                        var Listscheme = con.Query<Auction>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).ToList();
                        con.Close();
                        return Listscheme;

                    }
                }
              

                       catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;

                }
            }
        }

        public List<Auction> GetLatestBid()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetLatestBid");
                    var Listscheme = con.Query<Auction>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).ToList();
                    con.Close();
                    return Listscheme;
                }
               


                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;

                }
            }
        }


        public double GetCurrentAuctionCount(string Subquery)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {

                    var para = new DynamicParameters();
                    if (Subquery.Length > 0)
                    {
                        //para.Add("@Mode", "GetCurrentAuctionCount");
                        //var Listscheme = con.Query<double>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                        //return Listscheme;
                        var Listscheme = con.Query<double>("select COUNT(v2_defaultlots.productid)  from v2_defaultlots  with (NOLOCK) where " + Subquery + "", null, null, true, 0, CommandType.Text).SingleOrDefault();
                        con.Close();
                        return Listscheme;
                    }
                    else
                    {
                        para.Add("@Mode", "GetCurrentAuctionCount");
                        var Listscheme = con.Query<double>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                        con.Close();
                        return Listscheme;
                    }

                }


                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;

                }

            }
        }


        public Auction GetCurrentAuctionDetail(int productid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetCurrentAuctionDetail");
                    para.Add("@productid", productid);
                    var val = con.Query<Auction>("CRUDCurrentAuction", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                    return val;
                }

                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;

                }
            }
        }

        public Auction GetAuctionDetail(int productid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetAuctionDetail");
                    para.Add("@productid", productid);
                   var val=  con.Query<Auction>("CRUDCurrentAuction", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                    return val;
                }


                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;

                }

            }
        }

        public int CheckArtistAssginedbidUserlist(Auction AUC)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "CheckArtistAssginedbidUserlist");
                    para.Add("@userid", AUC.userid);
                    para.Add("@artistid", AUC.productid);
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                }
                catch (Exception ex)
                {
                  
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
                finally
                {
                    con.Close();
                }
      
            }
            return value;
        }

        public int AddArtisttoBidUserList(Auction AUC)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "AddArtisttoBidUserList");
                    para.Add("@userid", AUC.userid);
                    para.Add("@artistid", AUC.productid);
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
                finally
                {
                    con.Close();
                }
                
            }
            return value;
        }

        public int InsertBidRecord(Auction AUC)
        {
           

            var value = (dynamic)null;
  
                CurrentAuctionService CAS = new CurrentAuctionService();
                Auction objproductdetail = CAS.GetCurrentAuctionDetail(AUC.productid);

                objproductdetail.nextValidBidRs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(objproductdetail.pricers, 1).ToString());

            if (AUC.nextValidBidRs > objproductdetail.pricers)
            {
                using (SqlConnection con1 = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
                    {
                    try
                    {
                        var para = new DynamicParameters();
                        para.Add("@Mode", "InsertBidRecord");
                        para.Add("@Firstname", AUC.firstname);
                        para.Add("@Lastname", AUC.lastname);
                        para.Add("@Thumbnail", AUC.thumbnail);
                        para.Add("@Bidpricers", AUC.nextValidBidRs);
                        para.Add("@Bidpriceus", AUC.nextValidBidUs);
                        para.Add("@Reference", AUC.reference);
                        para.Add("@anoname", AUC.nickname);
                        para.Add("@validbidpricers", AUC.pricers);
                        para.Add("@validbidpriceus", AUC.priceus);
                        para.Add("@currentbid", AUC.currentbid);
                        para.Add("@recentbid", AUC.recentbid);
                        para.Add("@UserId", AUC.userid);
                        para.Add("@Username", AUC.username);
                        para.Add("@productid", AUC.productid);
                        para.Add("@Auctionid", AUC.Online);
                        para.Add("@proxy", AUC.proxy);

                        para.Add("@browserName", AUC.browserName);
                        para.Add("@latitude", AUC.latitude);
                        para.Add("@longitude", AUC.longitude);
                        para.Add("@ipAddress", AUC.ipAddress);
                        para.Add("@userLocation", AUC.userLocation);
                        para.Add("@fullAddress", AUC.fullAddress);


                        value = con1.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                        con1.Close();
                    }
                     

                     catch (Exception ex)
                    {
                        con1.Close();
                        log.logErrorMessage(ex.Message);
                        log.logErrorMessage(ex.StackTrace);

                    }
                }
            }
            //}


            return value;


        }

        public int InsertProxyBidRecord(Auction AUC)
        {
            var value = (dynamic)null;
            var checkduplicatebid = 0;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "checkduplicateProxyAmountUser");
                    para.Add("@productid", AUC.productid);
                    para.Add("@ProxyAmt", AUC.ProxyAmt);
                    para.Add("@userid", AUC.userid);
                    checkduplicatebid = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                }

                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);

                }
            }

            if (checkduplicatebid == 0)
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
                {
                    try
                    {
                        var para = new DynamicParameters();
                        para.Add("@Mode", "InsertProxyBidRecord");
                        para.Add("@UserId", AUC.userid);
                        para.Add("@productid", AUC.productid);
                        para.Add("@ProxyAmt", AUC.ProxyAmt);
                        para.Add("@ProxyAmtus", AUC.ProxyAmtus);
                        para.Add("@Createdby", AUC.username);
                        para.Add("@Auctionid", AUC.Online);
                        para.Add("@BidBy", 0);

                        // 22_12_2020
                        para.Add("@latitude", AUC.latitude);
                        para.Add("@longitude", AUC.longitude);
                        para.Add("@ipAddress", AUC.ipAddress);
                        para.Add("@userLocation", AUC.userLocation);
                        para.Add("@fullAddress", AUC.fullAddress);
                        //

                        value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                        con.Close();

                    }


                    catch (Exception ex)
                    {
                        con.Close();
                        log.logErrorMessage(ex.Message);
                        log.logErrorMessage(ex.StackTrace);

                    }
                }
            }
         
          
            return value;
        }
        public int UpdateAcutionPrice(Auction AUC)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "UpdateAcutionPrice");
                    para.Add("@pricers", AUC.nextValidBidRs);
                    para.Add("@priceus", AUC.nextValidBidUs);
                    para.Add("@productid", AUC.productid);
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                }

                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);

                    return 0;
                }
            }
            return value;
        }

        public int UpdateBidClosingTime(int productid)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "UpdateBidClosingTime");
                    para.Add("@productid", productid);
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();

                }

                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);

                    return 0;
                }
            }
            return value;
        }

        public int GetUserBiderOwner(string reference)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetUserBiderOwner");
                    para.Add("@Reference", reference);
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                }
 

                    catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);

                    return 0;
                }
            }
            return value;
        }

        public List<Auction> GetProxyInfo(Auction AUC)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    //AUC.nextValidBidRs;
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetProxyInfo");
                    para.Add("@Auctionid", AUC.Online);
                    para.Add("@productid", AUC.productid);
                    //para.Add("@price", AUC.nextValidBidRs);
                    para.Add("@price", AUC.pricers);
                    para.Add("@UserId", AUC.userid);
                    var val= con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).ToList();
                    con.Close();
                    return val;
                }
                


                   catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);

                    return null;
                }
            }
        }

        public List<Auction> GetUpdatedProxyInfo(Auction AUC) 
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetUpdatedProxyInfo");
                    para.Add("@Auctionid", AUC.Online);
                    para.Add("@productid", AUC.productid);
                    para.Add("@price", AUC.curprice);
                    var val= con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).ToList();
                    con.Close();
                    return val;
                }
              

                   catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);

                    return null;
                }
            }
        }


        public List<Auction> GetAcutionData(Auction AUC)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetAcutionData");
                    para.Add("@Auctionid", AUC.Online);
                    para.Add("@productid", AUC.productid);
                    var val = con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).ToList();
                    con.Close();
                    return val;
                }
                

                  catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);

                    return null;
                }
            }
        }

        public int UpdateRecentCurrentBid(Auction AUC)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "UpdateRecentCurrentBid");
                    para.Add("@currentbid", AUC.currentbid);
                    para.Add("@recentbid", AUC.recentbid);
                    para.Add("@Bidrecordid", AUC.Bidrecordid);
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                }

                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                 

                }
            }
            return value;
        }

        public List<Auction> getBidUserList(Auction AUC)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "getBidUserList");
                    para.Add("@productid", AUC.productid);
                    para.Add("@userid", AUC.userid);
                    var val= con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).ToList();
                    con.Close();
                    return val;
                }
               
                       catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;

                }
            }
        }

        public List<Auction> getBidRecordList(Auction AUC)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "getBidRecordList");
                    para.Add("@productid", AUC.productid);
                   var val=  con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).ToList();
                    con.Close();
                    return val;
                }
                


                 catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;

                }
            }
        }

        public int updateBid(Auction AUC)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "updateBid");
                    para.Add("@currentbid", AUC.currentbid);
                    para.Add("@recentbid", AUC.recentbid);
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                }
                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);

                }


            }
            return value;
        }

        public int updateRecentBid(Auction AUC)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "updateRecentBid");
                    para.Add("@recentbid", AUC.recentbid);
                    para.Add("@Bidrecordid", AUC.Bidrecordid);
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                }


                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    
                }
            }
            return value;
        }


        public List<Auction> GetProxyDetails(int productid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetProxyDetails");
                    para.Add("@productid", productid);
                    var val= con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).ToList();
                    con.Close();
                    return val;
                }
               

                       catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
            }
        }

        public List<Auction> GetBidHistory(int productid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@Mode", "GetBidHistory");
                para.Add("@productid", productid);
                return con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).ToList();
            }
        }

        public Auction GetAdditionalCharges(int productid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@Mode", "GetAdditionalCharges");
                para.Add("@productid", productid);
                return con.Query<Auction>("CRUDCurrentAuction", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
            }
        }

        public List<Auction> GetHighValueBid()

        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetHighValueBid");
                    var Listscheme = con.Query<Auction>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).ToList();
                    con.Close();
                    return Listscheme;
                }
              

                    catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
            }
        }


        public List<Auction> GetMostPopularLots()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetMostPopularLots");
                    var Listscheme = con.Query<Auction>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).ToList();
                    con.Close();
                    return Listscheme;
                }
               
                  catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
            }
        }


        public List<Auction> GetClosingLots()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetClosingLots");
                    var Listscheme = con.Query<Auction>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).ToList();
                    con.Close();
                    return Listscheme;
                }
           catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }

            }
        }

        public int DeleteArtisttoBidUserList(Auction AUC)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "DeleteArtisttoBidUserList");
                    para.Add("@userid", AUC.userid);
                    para.Add("@artistid", AUC.productid);
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                    return value;
                }
                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
            }
          
        }

        public List<Auction> GetMyPurchase(int userid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetMyPurchase");
                    para.Add("@UserId", userid);
                    con.Close();
                    return con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).ToList();
                }
                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
            }
        }

        public Auction Getproxyinfosame(int AuctionId)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "Getproxyinfosame");
                    para.Add("@AuctionId", AuctionId);
                  var val=con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).FirstOrDefault();
                    con.Close();
                    return val;
                }
                

                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }

            }
        }

        //new created on 20-1-2020
        #region
        public int Getcountbidproduct(int prooductid)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetcountMostPopularCount");
                    para.Add("@productid", prooductid);
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();

                }
                catch(Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                }

            }
            return value;
        }

        #endregion


        //created on 26-1-20
        public int Gethighestproxybytime(int productid)
        {
            int value = 0;
            DataTable dt = new DataTable();
            dt = util.Display("exec CRUDBid 'Gethighestproxybytime',0,0,'','','',0,0,'','',0,0,0,0,''," + productid + "");
            if (dt.Rows.Count > 0)
            {
                int bidpricevalue = int.Parse(dt.Rows[0]["Bidpricers"].ToString());
                DataTable dt1 = new DataTable();
                 dt1 = util.Display("exec CRUDBid 'CheckmultipleGethighestproxybytime',0, 0,'','','',"+ bidpricevalue + ", 0,'','',0,0,0,0,'',"+ productid + "");
                if (dt1.Rows.Count == 2)
                {
                    value = int.Parse(dt.Rows[0]["UserId"].ToString());

                }
            }

            return value;
        }

        //created on 29_06_2020
        #region

        public Auction Getcurrentleadinguser(int productid)                             //get leading user Bidpricers
        {
  
            Auction objAuction = new Auction();
            Utility util = new Utility();

            DataTable dt = new DataTable();
             dt = util.Display("exec CRUDBid 'Getcurrentleadinguser',0,0,'','','',0,0,'','',0,0,0,0,'',"+ productid + "");
           
            if (dt.Rows.Count > 0)
            {
                objAuction.userid = int.Parse(dt.Rows[0]["UserId"].ToString());
                objAuction.amountlimt = int.Parse(dt.Rows[0]["amountlimt"].ToString());
                objAuction.Bidpricers = int.Parse(dt.Rows[0]["bidpricers"].ToString());
            }
            return objAuction;
        }


        //created on 29_06_2020
        public Auction Getcurrentpriceuser(int productid, int userid)                               //get outbid user Bidpricers
        {
            Auction objAuction = new Auction();
            DataTable dt = new DataTable();
            dt = util.Display("exec CRUDBid 'Getcurrentpriceuser'," + userid + ",0,'','','',0,0,'','',0,0,0,0,''," + productid + "");
            if (dt.Rows.Count > 0)
            {

                objAuction.Bidpricers = int.Parse(dt.Rows[0]["Bidpricers"].ToString());
                objAuction.userid = int.Parse(dt.Rows[0]["userid"].ToString());
                objAuction.amountlimt = int.Parse(dt.Rows[0]["amountlimt"].ToString());
            }
            return objAuction;
        }


        //created on 29_06_2020

        public Auction Getcurrentleadinguserforbidclose(int productid)                                //get leadinguser after bid close  
        {

            Auction objAuction = new Auction();
            DataTable dt = new DataTable();
           dt = util.Display("exec CRUDBid 'Getcurrentleadinguserforbidclose',0,0,'','','',0,0,'','',0,0,0,0,''," + productid + "");
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["amountlimt"].ToString() == "")
                {
                    objAuction.amountlimt = 0;
                }
                else
                {
                    objAuction.amountlimt = int.Parse(dt.Rows[0]["amountlimt"].ToString());
                }

                objAuction.Bidpricers = int.Parse(dt.Rows[0]["Bidpricers"].ToString());
                objAuction.userid = int.Parse(dt.Rows[0]["userid"].ToString());
                objAuction.checkbidlimit = int.Parse(dt.Rows[0]["checkbidlimit"].ToString());
                objAuction.Bidrecordid = int.Parse(dt.Rows[0]["Bidrecordid"].ToString());
            }
            return objAuction;
        }


        //created on 29-1-20

        public int updatechecklimit(int userid, int Bidrecordid)                   //set flag for leading uuser after bid closed
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "Updatechecklimit");
                    para.Add("@userid", userid);
                    para.Add("@Bidrecordid", Bidrecordid);
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                }

                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);

                }
            }
            return value;
        }
        #endregion


        //created on 29_06_2020
        public Auction GetcurrentpriceuserAfterproxycheck(int productid, int userid)                         //get proxyuser data
        {
          
            Auction objAuction = new Auction();
            DataTable dt = new DataTable();
            dt = util.Display("exec CRUDBid 'GetcurrentpriceuserAfterproxycheck'," + userid + ",0,'','','',0,0,'','',0,0,0,0,''," + productid + "");
            if (dt.Rows.Count > 0)
            {
                objAuction.ProxyAmt = int.Parse(dt.Rows[0]["ProxyAmt"].ToString());
                objAuction.Bidpricers = int.Parse(dt.Rows[0]["Bidpricers"].ToString());
                objAuction.userid = int.Parse(dt.Rows[0]["userid"].ToString());
                objAuction.amountlimt = int.Parse(dt.Rows[0]["amountlimt"].ToString());
                objAuction.islessproxy = int.Parse(dt.Rows[0]["islessproxy"].ToString());
            }
            return objAuction;
        }

// create 29_06_2020

        public int Getuserlimit(int userid)                 
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "Getuserlimit");
                    para.Add("@userid", userid);
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                }

                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);

                }

            }
            return value;
        }

        //created on 6_2_2020
        public void Updateislesssproxy(int userid, int productid,int islessproxy)                 //set flag for proxy user
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "Updateislesssproxy");
                    para.Add("@userid", userid);
                    para.Add("@productid", productid);
                    para.Add("@islessproxy", islessproxy);
                    con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                }


                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    
                }

            }
        }



        // create 29_06_2020

        #region


        public void Isleading(int productid,int userid,int bidpricers,int islead)                        //Update leading user
        {            
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "Isleading");
                    para.Add("@userid", userid);
                    para.Add("@productid", productid);
                    para.Add("@Bidpricers", bidpricers);
                    para.Add("@islead", islead);
                    con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                }

                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    
                }
            }
        }

        // create 29_06_2020
        public int getoutbidamount(int productid, int userid)                      //get outbid user
        {

            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "getoutbidamount");
                    para.Add("@userid", userid);
                    para.Add("@productid", productid);
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                }

                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
            }
            return value;
       
        }

        #endregion


        //created on 17_2_2020
        public List<Auction> Getsameproxy(Auction AUC)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "Getsameproxy");
                    para.Add("@Auctionid", AUC.Online);
                    para.Add("@productid", AUC.productid);
                    para.Add("@price", AUC.nextValidBidRs);
                   var val=  con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).ToList();
                    con.Close();
                    return val;
                }
             

                  catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
            }
        }


        public List<Auction> GetproxyUser(Auction AUC)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetproxyUser");
                    para.Add("@Auctionid", AUC.Online);
                    para.Add("@productid", AUC.productid);
                    para.Add("@price", AUC.nextValidBidRs);

                    //para.Add("@price", AUC.pricers);          
                    var val=  con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).ToList();
                    con.Close();
                    return val;
                }
              

                  catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
            }
        }

        public List<Auction> GetUpdatedProxyuser(Auction AUC)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                
                var para = new DynamicParameters();
                para.Add("@Mode", "GetUpdatedProxyuser");
                para.Add("@Auctionid", AUC.Online);
                para.Add("@productid", AUC.productid);
                para.Add("@price", AUC.curprice);
                para.Add("@nextvalid", AUC.nextValidBidRs);
               var val=  con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).ToList();
                    con.Close();
                    return val;
                }
                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }

            }
        }

        // create 29_06_2020
        public void Isleadingproxy(int productid, int userid, int ProxyAmt, int islead)                        //Update leading user
        {

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "Isleadingproxy");
                    para.Add("@userid", userid);
                    para.Add("@productid", productid);
                    para.Add("@islead", islead);
                    para.Add("@ProxyAmt", ProxyAmt);
                    con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                }
               
                 catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                
                }
            }

        }

        public int checkforproxyuser(int productid,int userid)
        {

            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "checkforproxyuser");
                    para.Add("@userid", userid);
                    para.Add("@productid", productid);
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();

                }

                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
            }
            return value;
        
        }

        public int getoutbidproxyamount(int productid, int userid)                      //get outbid user
        {

            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                { 
                var para = new DynamicParameters();
                para.Add("@Mode", "getoutbidproxyamount");
                para.Add("@userid", userid);
                para.Add("@productid", productid);
                value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                con.Close();
                }

                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }

            }
            return value;         
        }


        public Auction GetCurrentLeadingProxyuser(int productid)                             //get leading user proxy
        {
            Auction objAuction = new Auction();
            DataTable dt = new DataTable();
            dt= util.Display("exec CRUDBid 'GetCurrentLeadingProxyuser',0,0,'','','',0,0,'','',0,0,0,0,''," + productid + "");
     
            if (dt.Rows.Count > 0)
            {
                objAuction.userid = int.Parse(dt.Rows[0]["UserId"].ToString());
                objAuction.amountlimt = int.Parse(dt.Rows[0]["amountlimt"].ToString());
                objAuction.ProxyAmt = int.Parse(dt.Rows[0]["ProxyAmt"].ToString());
            }
            return objAuction;

             
        }



        // created on 2_2_2020

        public Auction Getcurrentoutbiduser(int productid, int userid)                               //get outbid user Bidpricers
        {
      
            Auction objAuction = new Auction();
                    
            DataTable dt = new DataTable();
            dt = util.Display("exec CRUDBid 'Getcurrentoutbiduser',"+ userid + ",0,'','','',0,0,'','',0,0,0,0,'',"+ productid + "");
          
            if (dt.Rows.Count > 0)
            {

                objAuction.Bidpricers = int.Parse(dt.Rows[0]["Bidpricers"].ToString());
                objAuction.userid = int.Parse(dt.Rows[0]["userid"].ToString());
                objAuction.amountlimt = int.Parse(dt.Rows[0]["amountlimt"].ToString());
                objAuction.email = dt.Rows[0]["email"].ToString();
                objAuction.username = dt.Rows[0]["username"].ToString();
                objAuction.mobile = dt.Rows[0]["mobile"].ToString();
                objAuction.name = dt.Rows[0]["name"].ToString();
            }
            return objAuction;
        }

        public int checkforproxyuserfroutbidmessage (int productid, int userid)
        {
                
            int proxyuser = 0;
            DataTable dt = new DataTable();
            dt = util.Display("exec CRUDBid 'checkforproxyuserfroutbidmessage'," + userid + ",0,'','','',0,0,'','',0,0,0,0,''," + productid + "");   
            if (dt.Rows.Count > 0)
            {
                proxyuser = dt.Rows.Count;
            }
            return proxyuser;
        }



        public int GetCurrentAuctionId()
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetCurrentAuctionId");
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                }

                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);

                }

            }
            return value;
        }



        //created on 10_07_2020

        public int InsertBidRecordForAdminStartAuctoion(Auction AUC)
        {


            var value = (dynamic)null;

            using (SqlConnection con1 = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {

                try

                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "InsertBidRecord_startauction");
                    para.Add("@Firstname", AUC.firstname);
                    para.Add("@Lastname", AUC.lastname);
                    para.Add("@Thumbnail", AUC.thumbnail);
                    para.Add("@Bidpricers", AUC.nextValidBidRs);
                    para.Add("@Bidpriceus", AUC.nextValidBidUs);
                    para.Add("@Reference", AUC.reference);
                    para.Add("@anoname", AUC.nickname);
                    para.Add("@validbidpricers", AUC.pricers);
                    para.Add("@validbidpriceus", AUC.priceus);
                    para.Add("@currentbid", AUC.currentbid);
                    para.Add("@recentbid", AUC.recentbid);
                    para.Add("@UserId", AUC.userid);
                    para.Add("@Username", AUC.username);
                    para.Add("@productid", AUC.productid);
                    para.Add("@Auctionid", AUC.Online);
                    para.Add("@proxy", AUC.proxy);

                    para.Add("@browserName", AUC.browserName);

                    // 22_12_2020
                    para.Add("@latitude", AUC.latitude);
                    para.Add("@longitude", AUC.longitude);
                    para.Add("@ipAddress", AUC.ipAddress);
                    para.Add("@fullAddress", AUC.fullAddress);
                    //

                    value = con1.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con1.Close();
                }
                catch (Exception ex)
                {
                    con1.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);

                }

            }



            return value;


        }


        // created on 7_01_2020
        //Get leading user for addition billing charges
        public int GetLeadinguseridByProduct(int productid)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetLeadinguseridByProduct");
                    para.Add("@productid", productid);
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                }

                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);

                }

            }
            return value;
        }


        //added on 20_10_2021
        //Get ConfirmedBid and isOldUser value during Bidding
        public Auction getConfirmedBidIsOlduserValue(int userid)
        {
            Auction objauc = new Auction();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    //var para = new DynamicParameters();
                    //para.Add("@Mode", "GetUserConfirmedValue");
                    //value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();

                    //return value;

                    DataTable dt = new DataTable();
                    dt = util.Display("exec CRUDBid 'GetConfirmedBidIsOlduserValue'," + userid + "");

                    if (dt.Rows.Count > 0)
                    {
                        objauc.confirmbid = int.Parse(dt.Rows[0]["confirmbid"].ToString());
                        objauc.isOldUser = int.Parse(dt.Rows[0]["isOldUser"].ToString());

                    }
                    return objauc;

                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    con.Close();
                }
            }


        }
    }
}
