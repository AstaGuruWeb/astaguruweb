﻿using Astaguru.Models;
using Dapper;
using Repository.Lib;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Astaguru.Services.UserService
{
    public class BlogService
    {
        Log log = new Log();
        public double GetBlogCount(string Subquery, string categoryid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    if (categoryid == "0")
                    {
                        var Listscheme = con.Query<double>("select COUNT(BlogId) from Blog where IsActive=1", null, null, true, 0, CommandType.Text).SingleOrDefault();
                        return Listscheme;
                    }
                    else
                    {
                        var Listscheme = con.Query<double>("select COUNT(BlogId) from Blog where IsActive=1 and Categoryid=" + categoryid + "", null, null, true, 0, CommandType.Text).SingleOrDefault();
                        return Listscheme;
                    }
                }

                 catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
                finally
                {
                    con.Close();
                }
            }
        }


        public List<Blog> GetBlogList(string Subquery,int Page, string categoryid)
        {
   
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    if (categoryid == "0")
                    {

                        var Listscheme = con.Query<Blog>("select BlogId,Title,Image,CONVERT(varchar,PostDate,106) as PostDate,LEFT(Description, 250) + '...' as Description, IsActive, Slug, CreatedBy,Categoryid from Blog where IsActive=1  ORDER BY Blogid desc OFFSET " + Page + " ROWS FETCH NEXT 6 ROWS ONLY", null, null, true, 0, CommandType.Text).ToList();
                        return Listscheme;
                    }
                    else
                    {
                        var Listscheme = con.Query<Blog>("select BlogId,Title,Image,CONVERT(varchar,PostDate,106) as PostDate,LEFT(Description, 250) + '...' as Description, IsActive, Slug,CreatedBy, Categoryid from Blog where IsActive=1 and Categoryid=" + categoryid + " ORDER BY Blogid desc OFFSET " + Page + " ROWS FETCH NEXT 6 ROWS ONLY", null, null, true, 0, CommandType.Text).ToList();
                        return Listscheme;
                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }
            }
        }


        //created on 28_12_2020
        public List<Blog> GetHomeBlog()
        {
            Utility util = new Utility();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    List<Blog> objAuctionlist = new List<Blog>();
                    DataTable dt = new DataTable();
                    dt = util.Display("Execute Proc_Blog 'HomeBlog'");
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            Blog obj = new Blog();
                            obj.Slug = dr["Slug"].ToString();
                            obj.BlogId = int.Parse(dr["BlogId"].ToString());
                            obj.Image = dr["Image"].ToString();
                            obj.Title = dr["Title"].ToString();
                            obj.PostDate = dr["PostDate"].ToString();
                            obj.Description = dr["Description"].ToString();
                            obj.CreatedBy = dr["CreatedBy"].ToString();


                            objAuctionlist.Add(obj);
                        }
                    }
                    return objAuctionlist;
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }
            }
        }
    }
}