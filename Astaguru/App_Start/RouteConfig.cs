﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Astaguru
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
         name: "SuperAdmin",
         url: "SuperAdmin",
         defaults: new { controller = "Login", action = "Index", Usertype = "SuperAdmin" }
         );


            routes.MapRoute(
           name: "Index",
           url: "",
           defaults: new { controller = "Home", action = "Home" }

          );



            routes.MapRoute(
           name: "LiveAuction",
           url: "LiveAuction",
           defaults: new { controller = "Home", action = "CurrentAuction" }

           );


            routes.MapRoute(
         name: "ListLiveAuction",
         url: "ListLiveAuction",
         defaults: new { controller = "Home", action = "ListCurrentAuction" }

         );



            routes.MapRoute(
           name: "UpcomingAuctions",
           url: "UpcomingAuctions",
           defaults: new { controller = "Upcoming", action = "UpcomingAuctions" }

           );



            routes.MapRoute(
          name: "UpcomingPreview",
          url: "Upcoming/UpcomingPreview/{strurl}",
          defaults: new { controller = "Upcoming", action = "UpcomingPreview", id = UrlParameter.Optional }

          );



            routes.MapRoute(
   name: "UpcomingPreviewList",
   url: "Upcoming/UpcomingPreviewList/{strurl}",
   defaults: new { controller = "Upcoming", action = "UpcomingPreviewList", id = UrlParameter.Optional }

   );


            routes.MapRoute(
        name: "PastAuctions",
        url: "PastAuctions",
        defaults: new { controller = "Past", action = "PastAuctions" }

        );

            routes.MapRoute(
        name: "PastAuctionResult",
        url: "Past/PastAuctionResult/{strurl}",
        defaults: new { controller = "Past", action = "PastAuctionResult", id = UrlParameter.Optional }

        );
            routes.MapRoute(
  name: "listPastAuctionResult",
  url: "Past/listPastAuctionResult/{strurl}",
  defaults: new { controller = "Past", action = "listPastAuctionResult", id = UrlParameter.Optional }

  );

            routes.MapRoute(
                name: "AboutUs",
                url: "AboutUs",
                defaults: new { controller = "Home", action = "AboutUs" }

                );

            routes.MapRoute(
            name: "Services",
            url: "Service",
            defaults: new { controller = "Home", action = "Services" }

            );

            routes.MapRoute(
            name: "TermsAndConditions",
            url: "TermsAndConditions",
            defaults: new { controller = "Home", action = "TermsAndConditions" }

            );


            routes.MapRoute(
            name: "Careers",
            url: "Careers",
            defaults: new { controller = "Home", action = "Careers" }

            );


            routes.MapRoute(
        name: "Departments",
        url: "Departments",
        defaults: new { controller = "Home", action = "Departments" }

        );



            routes.MapRoute(
        name: "EstateSales",
        url: "Departments/EstateSales",
        defaults: new { controller = "Home", action = "deptEstate" }

        );

            routes.MapRoute(
  name: "FineWritingInstruments",
  url: "Departments/FineWritingInstruments",
  defaults: new { controller = "Home", action = "deptFine" }

  );

            routes.MapRoute(
name: "FurnitureAndDecorativeArt",
url: "Departments/FurnitureAndDecorativeArt",
defaults: new { controller = "Home", action = "deptFurniture" }

);


            routes.MapRoute(
name: "IndianAntiques",
url: "Departments/IndianAntiques",
defaults: new { controller = "Home", action = "deptIndian" }

);


            routes.MapRoute(
name: "IndianModernArt",
url: "Departments/IndianModernArt",
defaults: new { controller = "Home", action = "deptModernArt" }

);


            routes.MapRoute(
name: "JewelleryAndSilver",
url: "Departments/JewelleryAndSilver",
defaults: new { controller = "Home", action = "deptJewellery" }

);

            routes.MapRoute(
name: "Memorabilia",
url: "Departments/Memorabilia",
defaults: new { controller = "Home", action = "deptMemorabilia" }

);

            routes.MapRoute(
name: "NumismaticAndPhilatelic",
url: "Departments/NumismaticAndPhilatelic",
defaults: new { controller = "Home", action = "deptNumismatic" }

);

            routes.MapRoute(
name: "RareBooksAndManuscripts",
url: "Departments/RareBooksAndManuscripts",
defaults: new { controller = "Home", action = "deptBooks" }

);

            routes.MapRoute(
name: "ScienceAndNaturalHistory",
url: "Departments/ScienceAndNaturalHistory",
defaults: new { controller = "Home", action = "deptScience" }

);

            routes.MapRoute(
name: "SouthEastAsianContemporaryArt",
url: "Departments/SouthEastAsianContemporaryArt",
defaults: new { controller = "Home", action = "deptSoutheast" }

);

            routes.MapRoute(
name: "Textiles",
url: "Departments/Textiles",
defaults: new { controller = "Home", action = "deptTexilesView1" }

);


            routes.MapRoute(
name: "Timepieces",
url: "Departments/Timepieces",
defaults: new { controller = "Home", action = "Timepieces" }

);

            routes.MapRoute(
name: "Vintage & Classic Cars",
url: "Departments/VintageClassicCars",
defaults: new { controller = "Home", action = "deptClassic" }

);


            routes.MapRoute(
name: "Valuation",
url: "Valuation",
defaults: new { controller = "Home", action = "valuation" }

);

            routes.MapRoute(
name: "howtobuy",
url: "howtobuy",
defaults: new { controller = "Home", action = "howtobuy" }

);


            routes.MapRoute(
name: "HowToSell",
url: "HowToSell",
defaults: new { controller = "Home", action = "howToSell" }

);

            routes.MapRoute(
name: "ContactUs",
url: "ContactUs",
defaults: new { controller = "Home", action = "contactUs" }

);


            routes.MapRoute(
name: "RecordPriceArtwork",
url: "RecordPriceArtwork",
defaults: new { controller = "Home", action = "recordPriceArtwork" }

);

            routes.MapRoute(
name: "Videos",
url: "Videos",
defaults: new { controller = "Home", action = "video" }

);

            routes.MapRoute(
name: "PrivacyPolicy",
url: "PrivacyPolicy",
defaults: new { controller = "Home", action = "Policy" }

);

            routes.MapRoute(
name: "media",
url: "media",
defaults: new { controller = "Home", action = "media" }

);


            routes.MapRoute(
name: "mediadetail",
url: "mediadetail/{strNews}",
defaults: new { controller = "Home", action = "mediadetail", id = UrlParameter.Optional }

);

            routes.MapRoute(
name: "blog",
url: "blog",
defaults: new { controller = "Home", action = "blog" }

);


            routes.MapRoute(
name: "blogdetailpage",
url: "blogdetailpage/{strblog}",
defaults: new { controller = "Home", action = "blogdetailpage", id = UrlParameter.Optional }

);



            routes.MapRoute(
name: "faq",
url: "faq",
defaults: new { controller = "Home", action = "faq" }

);




            //use for webview call only

            routes.MapRoute(
 name: "mediadetailMob",
 url: "mediadetailMob/{strNews}",
 defaults: new { controller = "MobileApp", action = "mediadetailMob", id = UrlParameter.Optional }

 );



            routes.MapRoute(
name: "blogdetailpageMob",
url: "blogdetailpageMob/{strblog}",
defaults: new { controller = "MobileApp", action = "blogdetailpageMob", id = UrlParameter.Optional }

);


            // VintageAndClassicCar form
            routes.MapRoute(
name: "VintageAndClassicCar",
url: "VintageAndClassicCar",
defaults: new { controller = "Home", action = "VintageAndClassicCar" }

);


            routes.MapRoute(
         name: "truefittandhill",
         url: "MaryCohr",
         defaults: new { controller = "Home", action = "truefittandhill" }

         );


            // Promotion form
            routes.MapRoute(
            name: "Promotion",
            url: "Promotion",
            defaults: new { controller = "Home", action = "Promotion" }

            );

            routes.MapRoute(
         name: "consign",
         url: "SellNow",
         defaults: new { controller = "Home", action = "consign" }

         );

            routes.MapRoute(
    name: "consignwithus",
    url: "Sell",
    defaults: new { controller = "Home", action = "consignwithus" }

    );


            routes.MapRoute(
                  name: "Marketing",
                  url: "pre-Registration",
                  defaults: new { controller = "Home", action = "Marketing", }
              );



            routes.MapRoute(
                    name: "Default",
                    url: "{controller}/{action}/{id}",
                    defaults: new { controller = "Home", action = "Home", id = UrlParameter.Optional }
                );





        }
    }
}
